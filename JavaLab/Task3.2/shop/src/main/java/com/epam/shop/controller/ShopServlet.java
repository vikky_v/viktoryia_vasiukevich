package com.epam.shop.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class ShopServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryName = request.getParameter("categoryName");
		String subcategoryName = request.getParameter("subcategoryName");
		
		try {
			getGoods(response, categoryName, subcategoryName);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");

		String URI = request.getRequestURI();
		System.out.println(URI);
		System.out.println(request.getServletPath());
		if (URI.equals("/shop/")) {
			try {
				getCategories(response);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (URI.equals("/shop/category")) {
			String categoryName = request.getParameter("categoryName");
			try {
				getSubcategories(response, categoryName);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		if (URI.equals("/shop/subcategory")) {
			String subcategoryName = request.getParameter("subcategoryName");
			URL url = new URL(request.getHeader("Referer"));
			String categoryName = url.getQuery().split("=")[1];
			try {
				getGoods(response, categoryName, subcategoryName);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (URI.equals("/shop/add")) {
			String categoryName = request.getParameter("categoryName");
			String subcategoryName = request.getParameter("subcategoryName");
			try {
				getAddingPage(response, categoryName, subcategoryName);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private void getAddingPage(HttpServletResponse response, String categoryName, String subcategoryName) throws IOException, TransformerException {
		
		PrintWriter out = response.getWriter();

		// get the xsl stored in this project
		ServletContext context = getServletContext();

		Source xmlDoc = new StreamSource((InputStream) (context.getResourceAsStream("form.xml")));
		Source xslDoc = new StreamSource((InputStream) (context.getResourceAsStream("formStylesheet.xsl")));
		StringWriter writer = new StringWriter();

		// The transformed document is returned to the browser.
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer trans = factory.newTransformer(xslDoc);
		trans.setParameter("categoryName", categoryName);
		trans.setParameter("subcategoryName", subcategoryName);
		trans.transform(xmlDoc, new StreamResult(writer));

		out.println(writer.toString());
		
	}

	private void getCategories(HttpServletResponse response) throws IOException, TransformerException {

		PrintWriter out = response.getWriter();

		// get the xsl stored in this project
		ServletContext context = getServletContext();

		Source xmlDoc = new StreamSource((InputStream) (context.getResourceAsStream("products.xml")));
		Source xslDoc = new StreamSource((InputStream) (context.getResourceAsStream("listOfCategories.xsl")));
		StringWriter writer = new StringWriter();

		// The transformed document is returned to the browser.
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer trans = factory.newTransformer(xslDoc);
		trans.setParameter("categoryName", "null");
		trans.setParameter("subcategoryName", "null");
		trans.transform(xmlDoc, new StreamResult(writer));

		out.println(writer.toString());

	}

	private void getSubcategories(HttpServletResponse response, String categoryName)
			throws IOException, TransformerException {

		PrintWriter out = response.getWriter();

		// get the xsl stored in this project
		ServletContext context = getServletContext();

		Source xmlDoc = new StreamSource((InputStream) (context.getResourceAsStream("products.xml")));
		Source xslDoc = new StreamSource((InputStream) (context.getResourceAsStream("listOfCategories.xsl")));
		StringWriter writer = new StringWriter();

		// The transformed document is returned to the browser.
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer trans = factory.newTransformer(xslDoc);

		trans.setParameter("categoryName", categoryName);
		trans.setParameter("subcategoryName", "null");

		trans.transform(xmlDoc, new StreamResult(writer));

		out.println(writer.toString());

	}

	private void getGoods(HttpServletResponse response,String categoryName, String subcategoryName)
			throws IOException, TransformerException {

		PrintWriter out = response.getWriter();

		// get the xsl stored in this project
		ServletContext context = getServletContext();

		Source xmlDoc = new StreamSource((InputStream) (context.getResourceAsStream("products.xml")));
		Source xslDoc = new StreamSource((InputStream) (context.getResourceAsStream("listOfCategories.xsl")));
		StringWriter writer = new StringWriter();

		// The transformed document is returned to the browser.
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer trans = factory.newTransformer(xslDoc);

		trans.setParameter("categoryName", categoryName);
		trans.setParameter("subcategoryName", subcategoryName);

		trans.transform(xmlDoc, new StreamResult(writer));

		out.println(writer.toString());

	}
}
