<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="categoryName" />
	<xsl:param name="subcategoryName" />
	
	<xsl:template match="/">
		
		<html>
			<body>
			<form action="shopServlet" method="POST">
			<input type="hidden" name="categoryName" value="{$categoryName}"/>
			<input type="hidden" name="subcategoryName" value="{$subcategoryName}"/>
			<p/><label>Name</label>           <input type="text" name="name" required="required" style="position: absolute; right: 1000px;"/>
			<p/><label>Producer</label>       <input type="text" name="producer" required="required" style="position: absolute; right: 1000px;"/>
			<p/><label>Model</label>          <input type="text" name="model" required="required" pattern="[A-Z]{2}[0-9]{3}" placeholder="AA000" style="position: absolute; right: 1000px;"/>
			<p/><label>Date Of Issue</label>  <input type="text" name="dateOfIssue" required="required" pattern="([0][1-9]|[12][0-9]|[3][01])-([0][1-9]|[1][0-2])-([0-9]{4})" placeholder="31-01-2001" style="position: absolute; right: 1000px;"/>
			<p/><label>Color</label>          <input type="text" name="color" required="required" style="position: absolute; right: 1000px;"/>			
			<p/><label>Price</label>          <input type="text" name="price"  style="position: absolute; right: 1000px;"/>
			<p/><input type="submit" value="Save" />
			</form>			
			</body>			
		</html>
		
	</xsl:template>
</xsl:stylesheet>