<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" omit-xml-declaration="yes"/>

	<xsl:param name="categoryName" />
	<xsl:param name="subcategoryName" />
	<xsl:template match="/">

		<html>
			<body>

				<xsl:choose>
					<xsl:when test="$subcategoryName = 'null'">
						<xsl:choose>
							<xsl:when test="$categoryName = 'null'">
								<xsl:apply-templates select="products" />
							</xsl:when>

							<xsl:when test="$categoryName != 'null'">
								<xsl:apply-templates select="products/category" />
								<hr /><a href="/shop">back</a>
							</xsl:when>
						</xsl:choose>
					</xsl:when>

					<xsl:when test="$subcategoryName != 'null'">
						<xsl:apply-templates select="products/category/subcategory" />
						<hr /><a href="category?categoryName={$categoryName}">back</a>
						
						<a href="add?categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}">Add</a>
					</xsl:when>

				</xsl:choose>

			</body>
		</html>

	</xsl:template>

	<xsl:template match="products">
		<xsl:for-each select="category">
			<h2>
			<a href="category?categoryName={@name}"><xsl:value-of select="@name" /></a>(<xsl:value-of select="count(subcategory/good)" />)</h2>
		</xsl:for-each>

	</xsl:template>

	<xsl:template match="category">
		<div>
			<xsl:if test="@name = $categoryName">
				<xsl:for-each select="subcategory">
					<h2><a href="subcategory?subcategoryName={@name}"><xsl:value-of select="@name" /></a>(<xsl:value-of select="count(good)" />)</h2>
				</xsl:for-each>
			</xsl:if>			
		</div>
	</xsl:template>

	<xsl:template match="subcategory">
		<div>
			<xsl:if test="@name = $subcategoryName">
				<xsl:for-each select="good">
					<table>
						<tr>
							<td>producer</td>
							<td>
								<xsl:value-of select="producer" />
							</td>
						</tr>
						<tr>
							<td>model</td>
							<td>
								<xsl:value-of select="model" />
							</td>
						</tr>
						<tr>
							<td>dateOfIssue</td>
							<td>
								<xsl:value-of select="dateOfIssue" />
							</td>
						</tr>
						<tr>
							<td>color</td>
							<td>
								<xsl:value-of select="color" />
							</td>
						</tr>
						<tr>
							<td>price</td>
							<td>
								<xsl:if test="not(price)">
									<xsl:text>out of stock</xsl:text>
								</xsl:if>
								<xsl:if test="price">
									<xsl:value-of select="price" />
								</xsl:if>
							</td>
						</tr>
					</table>
				</xsl:for-each>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>