package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.NewsVO;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;

@Controller
@SessionAttributes("filter")
public class ViewNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@RequestMapping(value = "news",method = RequestMethod.GET, params = "newsId")
	public String viewNews(
			@RequestParam(value = "newsId", required = true) String newsId,
			Model model, @ModelAttribute("filter") Filter filter)
			throws ServiceException {

		Comment comment = new Comment();
		model.addAttribute("comment", comment);

		NewsVO news = newsManagementService.getNews(Integer.parseInt(newsId));

		model.addAttribute("news", news);

		List<Long> navigationList = newsManagementService.getPrevNextNewsId(filter,
				Long.parseLong(newsId));
		long previous = navigationList.get(0);
		long next = navigationList.get(1);

		model.addAttribute("previous", previous);
		model.addAttribute("next", next);

		return "viewNews";
	}
	
	@RequestMapping(value = "news/comment/add", method = RequestMethod.POST)
	public String addComment(Model model, @ModelAttribute("comment") Comment comment) throws ServiceException {

		newsManagementService.addComment(comment);

		return "redirect:/news?newsId=" + comment.getNewsId();

	}

}
