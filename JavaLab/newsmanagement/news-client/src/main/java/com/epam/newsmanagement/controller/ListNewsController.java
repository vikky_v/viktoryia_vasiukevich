package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.NewsVO;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@Controller
@SessionAttributes("filter")
public class ListNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@Autowired
	private PagedView pagedView;
	
	@RequestMapping(value = "/")
	public String index(Model model) {

			 Filter filter = new Filter();
			 filter.setAuthor(new Author());
			 filter.setTags(Collections.<Tag>emptyList());
			 model.addAttribute("filter", filter);
	
			 return "redirect:news/list?page=0";
	}

	@RequestMapping(value = "news/list", method = RequestMethod.POST, params = "submit")
	public String submitFilter(
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException{

		
		List<Tag> filterTags = new ArrayList<Tag>();
		if (tagsId != null) {

			for (Long tagId : tagsId) {
				filterTags.add(newsManagementService.getTagById(tagId));
			}

		}
		filter.setTags(filterTags);

		return "redirect:list?page=0";
	}

	@RequestMapping(value = "news/list", method = RequestMethod.POST, params = "reset")
	public String resetFilter(
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException{

		filter.setAuthor(new Author());
		filter.setTags(Collections.<Tag>emptyList());
		 
		return "redirect:list?page=0";
	}
	
	
	@RequestMapping(value = "news/list", method = RequestMethod.GET)
	public String newsList(
			@RequestParam(value = "page", required = false, defaultValue = "0") String page,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException{

		int numberOfNews = newsManagementService.countNews(filter);

		pagedView.setRowCount(numberOfNews);
		pagedView.setCurrentPage(Integer.parseInt(page));

		List<NewsVO> news = newsManagementService.getListNews(pagedView, filter);

		model.addAttribute("view", pagedView);
		model.addAttribute("news", news);

		List<Author> authors = newsManagementService.getAllAuthors();
		model.addAttribute("authors", authors);

		List<Tag> tags = newsManagementService.getAllTags();
		model.addAttribute("tags", tags);

		return "listNews";

	}

	

	

}
