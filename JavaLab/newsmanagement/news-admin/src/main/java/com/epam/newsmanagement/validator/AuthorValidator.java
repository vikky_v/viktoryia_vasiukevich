package com.epam.newsmanagement.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;

public class AuthorValidator implements Validator {

	@Autowired
	INewsManagementService newsManagementService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		
		return Author.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors){

			
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorName",
				"required.authorName", "Field name is required.");
		
		Author author = (Author) target;
		try {
			if(newsManagementService.isAuthorExists(author)){
				errors.rejectValue("authorName","required.authorNameExist");
			}
		} catch (ServiceException e) {
			
		}
	}

}
