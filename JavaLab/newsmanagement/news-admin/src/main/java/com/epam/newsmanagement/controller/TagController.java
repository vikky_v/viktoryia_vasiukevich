package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.validator.TagValidator;

@Controller
public class TagController {

	@Autowired
	private INewsManagementService newsManagementService;

	@Autowired
	private TagValidator tagValidator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(tagValidator);
	}

	@RequestMapping(value = "tags" , method = RequestMethod.GET)
	public String tags(Model model) throws ServiceException {

		setTagsContent(new Tag(), model);

		return "tags";
	}

	@RequestMapping(value = "tags-delete", method = RequestMethod.POST)
	public String expireTag(			
			@ModelAttribute("tag") Tag tag) throws ServiceException {
		
			newsManagementService.deleteTag(tag.getTagId());
			
			return "redirect:/tags";
		
	}
	
	@RequestMapping(value = "tags-update", method = RequestMethod.POST)
	public String updateTag(@ModelAttribute("tag") @Validated Tag tag,
			BindingResult result, Model model) throws ServiceException {
		
		if (result.hasErrors()) {
			setTagsContent(tag, model);
			return "tags";
		} else {
			newsManagementService.updateTag(tag);
			return "redirect:/tags";
		}
		
	}


	@RequestMapping(value = "tags-save", method = RequestMethod.POST)
	public String addTags(@ModelAttribute("tag") @Validated Tag tag, BindingResult result, Model model)
			throws ServiceException {

		if (result.hasErrors()) {

			setTagsContent(tag, model);

			return "tags";
		} else {

		newsManagementService.addTag(tag);

		return "redirect:/tags";
		}
	}
	
	private void setTagsContent(Tag tag,Model model) throws ServiceException {

		List<Tag> listOfTags = newsManagementService.getAllTags();
		model.addAttribute("tags", listOfTags);
		
		model.addAttribute("tag", tag);
	
	}

}
