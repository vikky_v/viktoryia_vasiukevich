package com.epam.newsmanagement.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.dto.NewsVO;

public class NewsValidator implements Validator {

	
	@Override
	public boolean supports(Class<?> clazz) {
		return NewsVO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.title",
				"required.title", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.modificationDate",
				"required.modificationDate", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.shortText",
				"required.shortText", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.fullText",
				"required.fullText", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "author.authorId",
				"required.author", "Field name is required.");

	}

}
