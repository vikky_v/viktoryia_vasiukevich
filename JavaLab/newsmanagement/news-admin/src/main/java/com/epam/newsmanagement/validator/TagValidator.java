package com.epam.newsmanagement.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;

public class TagValidator implements Validator {

	@Autowired
	INewsManagementService newsManagementService;

	@Override
	public boolean supports(Class<?> clazz) {

		return Tag.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

	

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tagName",
				"required.tagName", "Field name is required.");

		Tag tag = (Tag) target;
		try {
			if (newsManagementService.isTagExists(tag)) {
				errors.rejectValue("tagName", "required.tagNameExist");
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}

}
