package com.epam.newsmanagement.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.NewsVO;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.ControllerUtils;
import com.epam.newsmanagement.validator.NewsValidator;

@Controller
public class AddUpdateNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@Autowired
	private NewsValidator newsValidator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(newsValidator);
	}

	@RequestMapping(value = "/addnews", method = RequestMethod.GET)
	public String addNews(
			@RequestParam(value = "newsId", required = false, defaultValue = "0") String newsId,
			Model model) throws ServiceException {

		long newsVOId = Long.parseLong(newsId);
		NewsVO newsVO = null;

		if (newsVOId != 0) {
			newsVO = newsManagementService.getNews(newsVOId);
		} else {
			newsVO = new NewsVO();

		}

		setFilterContent(model);

		model.addAttribute("NewsVO", newsVO);

		return "addUpdateNews";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(
			@RequestParam(value = "pattern", required = true) String pattern,
			@RequestParam(value = "modificDate", required = true) String modificationDate,
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("NewsVO") NewsVO news, BindingResult result,
			Model model) throws ServiceException,
			ParseException {

		Date date = null;
		if (ControllerUtils.isDateValid(modificationDate, pattern)) {
			date = ControllerUtils.parseDate(modificationDate, pattern);
			news.getNews().setModificationDate(date);
		}

		newsValidator.validate(news, result);

		if (result.hasErrors()) {

			setFilterContent(model);

			model.addAttribute("NewsVO", news);

			return "addUpdateNews";
		} else {

			long createdNewsId = newsManagementService.saveNews(news.getNews(),
					news.getAuthor().getAuthorId(), tagsId, date);
			model.addAttribute("menu", "news");
			return "redirect:news?newsId=" + createdNewsId;
		}

	}

	private void setFilterContent(Model model) throws ServiceException {

		List<Author> authors = newsManagementService.getAllAuthors();
		model.addAttribute("authors", authors);

		List<Tag> tags = newsManagementService.getAllTags();
		model.addAttribute("tags", tags);
	}

}
