package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.validator.AuthorValidator;

@Controller
public class AuthorController {

	@Autowired
	private INewsManagementService newsManagementService;

	@Autowired
	private AuthorValidator authorValidator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(authorValidator);
	}

	@RequestMapping(value ="/authors", method = RequestMethod.GET)
	public String authors(Model model) throws ServiceException {

		setAuthorsContent(new Author(), model);
		return "authors";
	}
	
	@RequestMapping(value =  "authors-expire" , method = RequestMethod.POST)
	public String expireAuthor(			
			@ModelAttribute("author") Author author) throws ServiceException {
		
			newsManagementService.deleteAuthor(author.getAuthorId());
			
			return "redirect:/authors";
		
	}
	
	@RequestMapping(value =  "authors-update", method = RequestMethod.POST)
	public String updateAuthor(@ModelAttribute("author") @Validated Author author,
			BindingResult result, Model model) throws ServiceException {
		
		if (result.hasErrors()) {
			setAuthorsContent(author, model);
			return "authors";
		} else {
			newsManagementService.updateAuthor(author);
			return "redirect:/authors";
		}
		
	}


	@RequestMapping(value = "/authors-save", method = RequestMethod.POST)
	public String addAuthors(
			@ModelAttribute("author") @Validated Author author,
			BindingResult result, Model model) throws ServiceException {

		if (result.hasErrors()) {

			setAuthorsContent(author, model);

			return "authors";
		} else {

			newsManagementService.addAuthor(author);

			return "redirect:/authors";
		}
	}

	private void setAuthorsContent(Author author, Model model)
			throws ServiceException {

		List<Author> listOfAuthors = newsManagementService.getAllAuthors();
		model.addAttribute("authors", listOfAuthors);

		model.addAttribute("author", author);

	}

}
