<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<head>
<title><spring:message code="menu.addauthors" /></title>

<spring:url value="/resources/js/jquery-1.11.3.js" var="jquery" />
<script type="text/javascript" src="${jquery}"></script>

<spring:url value="/resources/js/submenuAuthor.js" var="submenu" />
<script type="text/javascript" src="${submenu}"></script>
</head>
<div class="body">

	<c:choose>
		<c:when test="${empty authors}">
			<p>
				<spring:message code="message.noauthors" />
			</p>
		</c:when>
		<c:otherwise>

			<div class="items">
				<c:forEach items="${authors}" var="author">
					<div class="item">
						<form:form commandName="author" id="form${author.authorId}"
							method="POST" >
							<span class="itemName"><b><spring:message code="message.author" /></b></span>
							
							<form:input type="hidden" path="authorId"
									value="${author.authorId}" /> 
									
							<form:input type="text"
									id="author${author.authorId}" size="40" path="authorName"
									value="${author.authorName}" disabled="true" />
							
								<div id="edit${author.authorId}" class="edit">
									<a href="" id="${author.authorId}" class="edit"><spring:message
											code="button.edit" /></a>
								</div>

								<div id="menu${author.authorId}" class="submenu">
									<a href="" id="${author.authorId}" class="update"><spring:message
											code="button.update" /></a>&nbsp;&nbsp;&nbsp; <input
										type="hidden" name="message" id="message"
										value="<spring:message code="message.expire" />" /> <a
										href="" id="${author.authorId}" class="expire"><spring:message
											code="button.expire" /></a>&nbsp;&nbsp;&nbsp; <a href=""
										id="${author.authorId}" class="cancel"><spring:message
											code="button.cancel" /></a>
								</div> 
								
								
								<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						
						</form:form>

						</div>

				</c:forEach>
			
			<div class="addItem">
					<form:form commandName="author" id="addAuthor" method="POST"
						action="authors-save">
						<span class="itemName"><b><spring:message code="message.addauthor" /></b></span>
						<form:hidden path="authorId" value="0" /> <form:input
								type="text" id="authorName" size="40" path="authorName"
								maxlength="30" required="required" />
											
						<a href="" class="save"><spring:message
									code="button.save" /></a>
									
						<br><form:errors path="authorName" cssClass="error" />
					</form:form>

				</div>
	
						
				

			</div>
			<br>
			<br>

		</c:otherwise>

	</c:choose>

</div>