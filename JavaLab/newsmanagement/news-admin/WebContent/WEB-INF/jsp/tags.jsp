<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
<title><spring:message code="menu.addtags" /></title>

<spring:url value="/resources/js/jquery-1.11.3.js" var="jquery" />
<script type="text/javascript" src="${jquery}"></script>

<spring:url value="/resources/js/submenuTag.js" var="submenu" />
<script type="text/javascript" src="${submenu}"></script>
</head>

<div class="body">

	<c:choose>
		<c:when test="${empty tags}">
			<p>
				<spring:message code="message.notags" />
			</p>
		</c:when>
		<c:otherwise>

			<div class="items">
				<c:forEach items="${tags}" var="tag">
					<div class="item">
						<form:form commandName="tag" id="form${tag.tagId}" method="POST"
							>
							<span class="itemName"><b><spring:message code="message.tag" /></b></span>
							<form:input type="hidden" path="tagId"
									value="${tag.tagId}" /> 
									
									<form:input type="text"
									id="tag${tag.tagId}" size="40" path="tagName"
									value="${tag.tagName}" disabled="true" />
							
								<div id="edit${tag.tagId}" class="edit">
									<a href="" id="${tag.tagId}" class="edit"><spring:message
											code="button.edit" /></a>
								</div>
								<div id="menu${tag.tagId}" class="submenu">
									<a href="" id="${tag.tagId}" class="update"><spring:message
											code="button.update" /></a>&nbsp;&nbsp;&nbsp; <input
										type="hidden" name="message" id="message"
										value="<spring:message code="message.delete" />" /> <a
										href="" id="${tag.tagId}" class="delete"><spring:message
											code="button.delete2" /></a>&nbsp;&nbsp;&nbsp; <a href=""
										id="${tag.tagId}" class="cancel"><spring:message
											code="button.cancel" /></a>

								</div>

							
						</form:form>

					</div>
					
					
				</c:forEach>
				
				<div class="addItem">
					<form:form commandName="tag" id="addTag" method="POST"
						action="tags-save">
						<span class="itemName"><b><spring:message code="message.addtag" /></b></span>
						<form:input type="text" id="tagName" size="40"
								path="tagName" maxlength="30"  />
																
						<a href="" class="save"><spring:message
									code="button.save" /></a>
									
						<br><form:errors path="tagName" cssClass="error" />
						
					</form:form>
				</div>
	
								
				

			</div>

			<br>
			<br>
		</c:otherwise>

	</c:choose>

</div>