<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<head>
<title><spring:message code="menu.viewNews" /></title>

</head>

<div class="body">
	<a class="left" href="?page=${page}"><spring:message code="link.back" /></a>
	<p />
	<div class="news">
		<div class="title"><b>${news.news.title}</b> &nbsp; (by ${news.author.authorName})</div>
			
		<div class="date">
		<fmt:setBundle
					basename="message" var="bundle" /> <fmt:message bundle="${bundle}"
					key="date.pattern" var="pattern" /> <fmt:formatDate
					pattern="${pattern}" value="${news.news.modificationDate}" />
		
		</div>
		<div class="fullText">
			<p>${news.news.fullText}</p>
		</div>
	</div>

	<c:forEach items="${news.comments}" var="comments">
		<div class="comment">
			<div class="dateComment">
			<fmt:setBundle
						basename="message" var="bundle" /> <fmt:message
						bundle="${bundle}" key="date.pattern" var="pattern" /> <fmt:formatDate
						pattern="${pattern}" value="${comments.creationDate}" />
			</div>
			<div class="commentText">
			<form:form commandName="comment" method="POST"
						action="deletecomment">
						<form:hidden path="commentId" value="${comments.commentId}" />
						<form:hidden path="newsId" value="${news.news.newsId}" />
						<input type="submit" value="X" class="buttondelete">
					</form:form> <br> ${comments.commentText}
			</div>
		</div>
		<p>
	</c:forEach>
	<div class="comment">
		<form:form commandName="comment" method="POST" action="addcomment">

			<form:hidden path="newsId" value="${news.news.newsId}" /> <form:textarea
						path="commentText" cols="47"  maxlength="100" required="required"/>
			<form:errors path="commentText" cssClass="error" />
		
			<div class="buttonComment">
			<input type="submit"
					class="postCommentButton"
					value="<spring:message code="button.postcomment"/>" />
			</div>

		</form:form>
	</div>
	<br> <br>

	<div class="bottom">
		<div class="previous">
			<c:if test="${previous > 0}">
						<a href="?newsId=${previous}"> <spring:message
								code="link.previous" />
						</a>
					</c:if>
		</div>
		<div class="next">			
					<c:if test="${next > 0}">
						<a href="?newsId=${next}"> <spring:message code="link.next" />
						</a>
					</c:if>
		</div>
		
		<br/> <br/> <br/>
	</div>
	<br/> <br/> <br/>

</div>