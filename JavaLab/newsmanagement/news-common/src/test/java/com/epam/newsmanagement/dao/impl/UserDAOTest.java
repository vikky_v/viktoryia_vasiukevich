package com.epam.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.IUserDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class UserDAOTest {
	
	
	@Autowired
	private IUserDAO userDAO;

	@Test
	@DatabaseSetup("classpath:/resources/setup/UserDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/UserDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCreate() throws Exception {
	String name = null;
	name=userDAO.getNameByLogin("admin");
	assertEquals("Admin Admin",name);
		
	}

}
