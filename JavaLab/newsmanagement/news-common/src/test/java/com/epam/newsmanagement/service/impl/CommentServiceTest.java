package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock
	private ICommentDAO commentDAO;

	@InjectMocks
	private CommentServiceImpl commentService;

	private long id = 1L;
	private Comment comment = new Comment();
	private Comment commentNull = null;

	@Test
	public void createTest() throws Exception {

		when(commentDAO.create(comment)).thenReturn(id);

		commentService.create(comment);
		verify(commentDAO).create(comment);
		assertEquals(id, commentService.create(comment));

	}

	@Test(expected = ServiceException.class)
	public void createTestException() throws Exception {

		commentService.create(commentNull);

	}

	@Test
	public void deleteTest() throws Exception {

		commentService.delete(1L);
		verify(commentDAO).delete(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteTestException() throws Exception {

		commentService.delete(0L);

	}

	@Test
	public void deleteByNewsIDTest() throws Exception {

		commentService.deleteByNewsID(1L);
		verify(commentDAO).deleteByNewsID(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteByNewsIDTestException() throws Exception {

		commentService.deleteByNewsID(-1L);
	
	}


	@Test
	public void findByNewsIdTest() throws Exception {

		commentService.getByNewsID(1L);
		verify(commentDAO).findByNewsID(1L);

	}

	@Test(expected = ServiceException.class)
	public void findByNewsIdTestException() throws Exception {

		commentService.getByNewsID(-1L);

	}

}
