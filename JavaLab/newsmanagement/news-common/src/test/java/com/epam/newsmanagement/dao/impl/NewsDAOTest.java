package com.epam.newsmanagement.dao.impl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.connection.DBUtil;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class NewsDAOTest {

	@Autowired
	private INewsDAO newsDAO;

	@Autowired
	private DBUtil dbUtil;
	
	@Autowired
	private PagedView pagedView;

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE)
	public void testCreate() throws Exception {

		News news = new News();
		news.setTitle("aaa");
		news.setShortText("qwety");
		news.setFullText("qwetyruytuighhfj");
		news.setCreationDate(new Timestamp(new Date().getTime()));
		news.setModificationDate(new Timestamp(new Date().getTime()));
		long id = newsDAO.create(news);

		List<News> newsList = newsDAO.getAll();
		assertEquals(7, newsList.size());
		assertTrue(id > 0);
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {
		News news = new News(3, "Short", "qwetyruytuighhfj", "aaa",
				new Timestamp(new Date().getTime()), new Timestamp(
						new Date().getTime()));

		newsDAO.update(news);

		News news2 = newsDAO.getById(3);

		assertEquals(news.getNewsId(), news2.getNewsId());
		assertEquals(news.getShortText(), news2.getShortText());
		assertEquals(news.getFullText(), news2.getFullText());
		assertEquals(news.getTitle(), news2.getTitle());

		Calendar now = Calendar.getInstance();
		Long time = news.getModificationDate().getTime();
		now.setTimeInMillis(time);
		now.set(Calendar.AM_PM, 0);
		now.set(Calendar.HOUR, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		
		Calendar now2 = Calendar.getInstance();
		Long time2 = news2.getModificationDate().getTime();
		now2.setTimeInMillis(time2);
		now2.set(Calendar.AM_PM, 0);
		now2.set(Calendar.HOUR, 0);
		now2.set(Calendar.MINUTE, 0);
		now2.set(Calendar.SECOND, 0);
		now2.set(Calendar.MILLISECOND, 0);

		assertEquals(now, now2);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagauthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDelete() throws Exception {

		newsDAO.delete(6);
		List<News> newsList = newsDAO.getAll();

		assertEquals(5, newsList.size());
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testFindById() throws Exception {

		News news = new News(3, "1 Short Text here...",
				"1 Short Text here... And then full text...", "1st News",
				new Timestamp(new Date().getTime()), new Timestamp(
						new Date().getTime()));
		News news2 = newsDAO.getById(3);

		assertEquals(news2.getNewsId(), news2.getNewsId());
		assertEquals(news.getShortText(), news2.getShortText());
		assertEquals(news.getFullText(), news2.getFullText());
		assertEquals(news.getTitle(), news2.getTitle());

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetFilteredNews() throws Exception {
		
		
		pagedView.setCurrentPage(0);
		
		System.out.println(pagedView.getCurrentPage());
		System.out.println(pagedView.getItemPerPage());
		
		Author a = new Author(18, "Oleg", null);
		Tag t = new Tag(3,"news");
		List<Tag> listTag= new ArrayList<Tag>();
		listTag.add(t);
		Filter filter = new Filter();
		filter.setAuthor(a);
		filter.setTags(listTag);
		
		List<News> newsList = newsDAO.getNews(pagedView,filter);

		assertEquals(2, newsList.size());

	}
	

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCountNews() throws Exception {
		
		Filter filter = new Filter();
		
		int numberOfNews = newsDAO.countNews(filter);

		assertEquals(6, numberOfNews);

	}
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCountFilteredNews() throws Exception {
		
		Author a = new Author();
		a.setAuthorId(18);
		Filter filter = new Filter();
		filter.setAuthor(a);
	
		
		
		int numberOfNews = newsDAO.countNews(filter);
		assertEquals(2, numberOfNews);
		
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1,""));
		tags.add(new Tag(2,""));
		tags.add(new Tag(3,""));
		filter = new Filter();
		filter.setTags(tags);
		
		
		numberOfNews = newsDAO.countNews(filter);
		assertEquals(3, numberOfNews);

	}
	
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAllRows() throws Exception {
		List<News> newsList = newsDAO.getAll();

		assertEquals(6, newsList.size());

	}
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetPrevNextNewsId() throws Exception {
		
		Filter filter = new Filter();
			
		List<Long> number = newsDAO.getPrevNextNewsId(filter, 13);
		
		assertEquals(2,number.size());
		long prev = number.get(0);
		assertEquals(1L,prev);
		long next = number.get(1);
		assertEquals(5L,next);
			
	}
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetPrevNextNewsIdFilter() throws Exception {
	List<Tag> tags = new ArrayList<Tag>();
	tags.add(new Tag(1,""));
	tags.add(new Tag(2,""));
	tags.add(new Tag(3,""));
	
	Filter filter = new Filter();
	filter.setTags(tags);
	
	List<Long> number = newsDAO.getPrevNextNewsId(filter, 4);
	
	assertEquals(2,number.size());
	long prev = number.get(0);
	assertEquals(3L,prev);
	long next = number.get(1);
	assertEquals(5L,next);
	
	Author a = new Author();
	filter = new Filter();
	filter.setAuthor(a);
	
	a.setAuthorId(18L);
	number = newsDAO.getPrevNextNewsId(filter, 4);
	assertEquals(2,number.size());
	prev = number.get(0);
	assertEquals(5L,prev);
	next = number.get(1);
	assertEquals(0L,next);
	}
	

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAuthorByNews() throws Exception {

		long authorId = newsDAO.getAuthorByNews(3).getAuthorId();

		assertEquals(20, authorId);
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetNewsByAuthor() throws Exception {

		List<News> news = newsDAO.getNewsByAuthor(18);

		assertEquals(2, news.size());
		
		assertTrue(news.get(0).getNewsId()==4L | news.get(0).getNewsId()==5L);
		if(news.get(0).getNewsId()==4L){
			assertTrue(news.get(1).getNewsId()==5L);
		}
		else{
			assertTrue(news.get(1).getNewsId()==4L);
		}
			
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCreateNewsAuthor() throws Exception {

		newsDAO.createNewsAuthor(6, 19);

		Map<Long, Long> newsAuthor = getAllRowsNewsAuthor();

		assertEquals(4, newsAuthor.size());

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAllRowsNewsAuthor() throws Exception {

		Map<Long, Long> newsAuthor = getAllRowsNewsAuthor();

		assertEquals(3, newsAuthor.size());
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdateNewsAuthor() throws Exception {

		newsDAO.updateNewsAuthor(5, 19);

		long authorId = newsDAO.getAuthorByNews(5).getAuthorId();

		assertEquals(19, authorId);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetTagsByNews() throws Exception {

		List<Tag> tags = newsDAO.getTagsByNews(4);

		assertEquals(2, tags.size());
		
		assertTrue(tags.get(0).getTagId()==2L | tags.get(0).getTagId()==3L);
		if(tags.get(0).getTagId()==2L){
			assertTrue(tags.get(1).getTagId()==3L);
		}
		else{
			assertTrue(tags.get(1).getTagId()==2L);
		}
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAllRowsNewsTag() throws Exception {

		Map<Long, ArrayList<Long>> newsTag = getAllRowsNewsTag();

		assertEquals(4, newsTag.size());
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCreateNewsTag() throws Exception {

		newsDAO.createNewsTag(4, 2);

		Map<Long, ArrayList<Long>> newsAuthor = getAllRowsNewsTag();

		assertEquals(5, newsAuthor.size());

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDeleteNewsTag() throws Exception {

		newsDAO.deleteNewsTag(5, 3);

		Map<Long, ArrayList<Long>> newsTag = getAllRowsNewsTag();

		assertEquals(3, newsTag.size());
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetNewsByTag() throws Exception {

		List<News> news = newsDAO.getNewsByTag(3);
	
		assertEquals(2, news.size());
		
		assertTrue(news.get(0).getNewsId()==4L | news.get(0).getNewsId()==5L);
		if(news.get(0).getNewsId()==4L){
			assertTrue(news.get(1).getNewsId()==5L);
		}
		else{
			assertTrue(news.get(1).getNewsId()==4L);
		}
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDeleteNewsTagByNewsID() throws Exception {

		newsDAO.deleteNewsTagByNewsID(4);

		Map<Long, ArrayList<Long>> newsTag = getAllRowsNewsTag();

		assertEquals(2, newsTag.size());
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDeleteNewsAuthorByNewsID() throws Exception {

		newsDAO.deleteNewsAuthorByNewsID(4);

		Map<Long, Long> newsAuthor = getAllRowsNewsAuthor();

		assertEquals(2, newsAuthor.size());
	}

	private Map<Long, Long> getAllRowsNewsAuthor() throws Exception {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		Map<Long, Long> newsAuthor = new HashMap<Long, Long>();
		try {
			con = dbUtil.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQLQuery.GET_ALL_ROWS_NEWS_AUTHOR);
			while (rs.next()) {
				newsAuthor.put(rs.getLong(2), rs.getLong(3));
			}
		} finally {
			dbUtil.close(st);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return newsAuthor;
	}

	private Map<Long, ArrayList<Long>> getAllRowsNewsTag() throws Exception {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		Map<Long, ArrayList<Long>> newsTag = new HashMap<Long, ArrayList<Long>>();
		try {
			con = dbUtil.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQLQuery.GET_ALL_ROWS_NEWS_TAG);
			ArrayList<Long> id = new ArrayList<Long>();
			while (rs.next()) {
				id.add(rs.getLong(2));
				id.add(rs.getLong(3));
				newsTag.put(rs.getLong(1), id);
			}

		} finally {
			dbUtil.close(st);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return newsTag;
	}

}
