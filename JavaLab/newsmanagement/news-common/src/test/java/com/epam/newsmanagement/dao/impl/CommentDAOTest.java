package com.epam.newsmanagement.dao.impl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dto.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class CommentDAOTest {

	@Autowired
	private ICommentDAO commentDAO;

	@Test
	@DatabaseSetup("classpath:/resources/setup/CommentDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCreate() throws Exception {
		long id = 0;
		Comment c = new Comment();
		c.setCommentText("comment");
		c.setNewsId(2L);
		c.setCreationDate(new Date());
		
		id = commentDAO.create(c);


		assertTrue(id > 0);
	}


	@Test
	@DatabaseSetup("classpath:/resources/setup/CommentDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {

		Comment comment = new Comment(4, "4444", new Timestamp(
				new Date().getTime()), 13);

		commentDAO.update(comment);

		Comment newComment = commentDAO.findByNewsID(13L).get(0);
		assertEquals(comment.getCommentId(), newComment.getCommentId());
		assertEquals(comment.getCommentText(), newComment.getCommentText());
		assertEquals(comment.getNewsId(), newComment.getNewsId());
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/CommentDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDelete() throws Exception {

		commentDAO.delete(1L);
		int rows = commentDAO.findByNewsID(1L).size();

		assertEquals(2, rows);
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/CommentDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDeleteByNewsId() throws Exception {

		commentDAO.deleteByNewsID(1L);
		int rows = commentDAO.findByNewsID(1L).size();

		assertEquals(0, rows);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/CommentDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testFindByNewsId() throws Exception {

		List<Comment> rows = commentDAO.findByNewsID(1L);

		assertEquals(3, rows.size());

	}

}
