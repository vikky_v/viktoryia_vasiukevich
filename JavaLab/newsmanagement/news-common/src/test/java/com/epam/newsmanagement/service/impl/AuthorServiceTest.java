package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.service.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock
	private IAuthorDAO authorDAO;

	@InjectMocks
	private AuthorServiceImpl authorService;

	private long id = 1L;
	private long id2 = 2L;
	private Author author = new Author();
	private Author author2 = new Author(2, "222", null);
	private Author authorNull = null;

	@Test
	public void createTest() throws Exception {

		when(authorDAO.create(author)).thenReturn(id);
		authorService.create(author);
		verify(authorDAO).create(author);
		assertEquals(id, authorService.create(author));

	}

	@Test(expected = ServiceException.class)
	public void createTestException() throws Exception {

		authorService.create(authorNull);

	}

	@Test
	public void updateTest() throws Exception {

		authorService.update(author);
		verify(authorDAO).update(author);

	}

	@Test(expected = ServiceException.class)
	public void updateTestException() throws Exception {

		authorService.update(authorNull);

	}

	@Test
	public void deleteTest() throws Exception {

		authorService.delete(1L);
		verify(authorDAO).delete(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteTestException() throws Exception {

		authorService.delete(-1L);

	}

	@Test
	public void findByIdTest() throws Exception {

		when(authorDAO.getById(id)).thenReturn(author);
		when(authorDAO.getById(id2)).thenReturn(author2);

		authorService.findById(id);
		verify(authorDAO).getById(id);

		authorService.findById(id2);
		verify(authorDAO).getById(id2);

		assertFalse(authorService.findById(id).equals(
				authorService.findById(id2)));

	}

	@Test(expected = ServiceException.class)
	public void findByIdTestException() throws Exception {

		authorService.findById(-1L);
	}

	@Test
	public void getAuthorIdTest() throws Exception {

		when(authorDAO.getAuthorId(author)).thenReturn(id);
		authorService.getAuthorId(author);
		verify(authorDAO).getAuthorId(author);
		assertEquals(id, authorService.getAuthorId(author));
	}

	@Test(expected = ServiceException.class)
	public void getAuthorIdTestException() throws Exception {
		authorService.getAuthorId(authorNull);
	}

	@Test
	public void getAllRowsTest() throws Exception {
		authorService.getAll();
		verify(authorDAO).getAll();
}

}
