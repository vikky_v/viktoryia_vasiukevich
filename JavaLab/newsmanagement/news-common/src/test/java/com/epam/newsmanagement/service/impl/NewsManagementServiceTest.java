package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {

	@Mock
	private IAuthorService authorService;

	@Mock
	private ICommentService commentService;

	@Mock
	private INewsService newsService;

	@Mock
	private ITagService tagService;

	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;


	@Test
	public void showFileredNewsForPageTest() throws Exception {

		List<News> listOfNews = new ArrayList<News>();
		News news1 = new News();
		news1.setNewsId(1L);
		listOfNews.add(news1);
		News news2 = new News();
		news2.setNewsId(2L);
		listOfNews.add(news2);
	

		Author author = new Author(1L, "one", null);

		Tag tag = new Tag(5L, "t");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag);
		Filter filter = new Filter();
		filter.setAuthor(author);
		filter.setTags(tags);
		
		PagedView pW = new PagedView();
		pW.setCurrentPage(0);
		
		
		List<Comment> comments = new ArrayList<Comment>();
		comments.add(new Comment());
		
		
		when(newsService.getListNews(pW,filter)).thenReturn(listOfNews);
		when(newsService.getAuthorByNews(1L)).thenReturn(author);
		when(newsService.getTagsByNews(1L)).thenReturn(tags);
		when(commentService.getByNewsID(1L)).thenReturn(comments);
		when(newsService.getAuthorByNews(2L)).thenReturn(author);
		when(newsService.getTagsByNews(2L)).thenReturn(tags);
		when(commentService.getByNewsID(2L)).thenReturn(comments);
		
		newsManagementService.getListNews(pW,filter);
		
		verify(newsService).getListNews(pW,filter);
		verify(newsService).getAuthorByNews(1L);
		verify(newsService).getTagsByNews(1L);
		verify(commentService).getByNewsID(1L);
		verify(newsService).getAuthorByNews(2L);
		verify(newsService).getTagsByNews(2L);
		verify(commentService).getByNewsID(2L);
		
	}
	
	


	@Test
	public void addNewsTest() throws Exception {
		News news1 = new News();
		List<Long> tagsId = new ArrayList<Long>();
		tagsId.add(5L);
		tagsId.add(7L);

		when(newsService.create(news1)).thenReturn(10L);

		newsManagementService.addNews(news1, 1L, tagsId);

		verify(newsService).createNewsAuthor(10L, 1L);
		verify(newsService).createNewsTag(10L, 5L);
		verify(newsService).createNewsTag(10L, 7L);
	}

	@Test
	public void editNewsTest() throws Exception {
		News news1 = new News();
		news1.setNewsId(40L);
		Tag tag = new Tag(5L, "t");
		Tag tag2 = new Tag(7L, "ttt");
		List<Long> tagsIdBefore = new ArrayList<Long>();
		tagsIdBefore.add(5L);
		tagsIdBefore.add(7L);
		
		List<Tag> tagsBefore = new ArrayList<Tag>();
		tagsBefore.add(tag);
		tagsBefore.add(tag2);
		
		List<Long> tagsIdAfter = new ArrayList<Long>();
		tagsIdAfter.add(7L);
		tagsIdAfter.add(8L);
		

		when(newsService.getTagsByNews(40L)).thenReturn(tagsBefore);

		newsManagementService.editNews(news1,1L,tagsIdAfter);

		verify(newsService).update(news1);
		verify(newsService).updateNewsAuthor(40L, 1L);
		verify(newsService).deleteNewsTagByNewsID(40L);
		verify(newsService).createNewsTag(40L, 7L);
		verify(newsService).createNewsTag(40L, 8L);
		

	}

	@Test
	public void deleteNewsTOTest() throws Exception {

		newsManagementService.deleteNews(1L);
		verify(newsService).deleteNewsTagByNewsID(1L);
		verify(newsService).deleteNewsAuthorByNewsID(1L);
		verify(commentService).deleteByNewsID(1L);
		verify(newsService).delete(1L);
	}

	@Test
	public void showSingleNewsVOTest() throws Exception {

		News news = new News();
		news.setNewsId(1L);

		Author author = new Author(5L, "a", null);

		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(7L,"q"));
		tags.add(new Tag(8L,"q8"));

		List<Comment> comments = new ArrayList<Comment>();
		comments.add(new Comment());

		when(newsService.getById(1L)).thenReturn(news);
		when(newsService.getAuthorByNews(1L)).thenReturn(author);
		when(newsService.getTagsByNews(1L)).thenReturn(tags);
		
		newsManagementService.getNews(1L);
		verify(newsService).getById(1L);
		verify(newsService).getAuthorByNews(1L);
		verify(newsService).getTagsByNews(1L);
		verify(commentService).getByNewsID(1L);
	}

	@Test
	public void addAuthorTest() throws Exception {

		Author a1 = new Author(1L, "qwert", null);
		Author a2 = new Author(2L, "asdfg", null);

		when(authorService.getAuthorId(a1)).thenReturn(1L);
		try {
			newsManagementService.addAuthor(a1);
			fail("Exception is required");
		} catch (ServiceException e) {
			//catch exception
		}
		verify(authorService, never()).create(a1);

		when(authorService.getAuthorId(a1)).thenReturn(0L);
		when(authorService.create(a2)).thenReturn(2L);
		newsManagementService.addAuthor(a2);
		verify(authorService).create(a2);
		assertEquals(2L, newsManagementService.addAuthor(a2));
	}

	@Test
	public void searchByAuthorTest() throws Exception {
		Author a1 = new Author(1L, "qwert", null);
		News n1 = new News();
		n1.setNewsId(2L);
		News n2 = new News();
		n2.setNewsId(10L);
		List<News> news = new ArrayList<News>();
		news.add(n1);
		news.add(n2);
		
		when(authorService.getAuthorId(a1)).thenReturn(1L);
		when(newsService.getNewsByAuthor(1L)).thenReturn(news);
		
		newsManagementService.getByAuthor(a1);
		verify(authorService).getAuthorId(a1);
		verify(newsService).getNewsByAuthor(1L);
		
	}

	@Test
	public void addTagTest() throws Exception {

		Tag t1 = new Tag(1L, "get");
		Tag t2 = new Tag(2L, "post");

		when(tagService.getByName(t1.getTagName())).thenReturn(t1);
		try {
			newsManagementService.addTag(t1);
			fail("Exception is required");
		} catch (ServiceException e) {
			//catch exception
		}
		verify(tagService, never()).create(t1);

		when(tagService.getByName(t2.getTagName())).thenReturn(null);
		when(tagService.create(t2)).thenReturn(2L);
		newsManagementService.addTag(t2);
		verify(tagService).create(t2);
		assertEquals(2L, newsManagementService.addTag(t2));
	}

//	@Test
//	public void addTagsTest() throws Exception {
//
//		Tag t1 = new Tag(1L, "get");
//		Tag t2 = new Tag(2L, "post");
//		List<Tag> tags = new ArrayList<Tag>();
//		tags.add(t1);
//		tags.add(t2);
//
//		when(tagService.getByName(t1.getTagName())).thenReturn(null);
//		when(tagService.create(t1)).thenReturn(1L);
//		when(tagService.getByName(t2.getTagName())).thenReturn(t2);
//
//		try {
//			newsManagementService.addTags(tags);
//			fail("Exception is required");
//		} catch (ServiceException e) {
//			//catch exception
//		}
//
//		verify(tagService).create(t1);
//		verify(tagService, never()).create(t2);
//	}

	@Test
	public void searchByTagsTest() throws Exception {
		Tag t1 = new Tag(1L, "get");
		Tag t2 = new Tag(2L, "post");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(t1);
		tags.add(t2);

		News n1 = new News();
		n1.setNewsId(9L);
		News n2 = new News();
		n2.setNewsId(10L);
		List<News> news = new ArrayList<News>();
		news.add(n1);
		news.add(n2);

		when(tagService.getByName(t1.getTagName())).thenReturn(t1);
		when(tagService.getByName(t2.getTagName())).thenReturn(t2);
		when(newsService.getNewsByTag(1L)).thenReturn(news);
		when(newsService.getNewsByTag(2L)).thenReturn(new ArrayList<News>());
		
		newsManagementService.getByTags(tags);
	
		verify(newsService).getNewsByTag(1L);
		
	
		verify(newsService).getNewsByTag(2L);

	}

//	@Test
//	public void addCommentsTest() throws Exception {
//		Comment com1 = new Comment();
//		com1.setCommentId(1L);
//		Comment com2 = new Comment();
//		com2.setCommentId(2L);
//		List<Comment> coms = new ArrayList<Comment>();
//		coms.add(com1);
//		coms.add(com2);
//
//		newsManagementService.addComments(coms);
//		verify(commentService).create(com1);
//		verify(commentService).create(com2);
//	}


//	@Test
//	public void deleteCommentsTest() throws Exception {
//		List<Long> comId = new ArrayList<Long>();
//		comId.add(11L);
//		comId.add(12L);
//		newsManagementService.deleteComments(comId);
//		verify(commentService).delete(11L);
//		verify(commentService).delete(12L);
//	}


}
