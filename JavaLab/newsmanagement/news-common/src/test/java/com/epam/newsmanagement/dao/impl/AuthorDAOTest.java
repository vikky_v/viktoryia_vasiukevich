package com.epam.newsmanagement.dao.impl;


import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dto.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class AuthorDAOTest {

	@Autowired
	private IAuthorDAO authorDAO;

	@Test
	@DatabaseSetup("classpath:/resources/setup/AuthorDS.xml")
	@ExpectedDatabase(value = "classpath:/resources/setup/expectedData.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@DatabaseTearDown(value ="classpath:/resources/setup/AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCreate() throws Exception {
		long id = 0;
		Author a = new Author();
		a.setAuthorName("Vika");
		id = authorDAO.create(a);
		assertTrue(id > 0);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/AuthorDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAllRows() throws Exception {

		List<Author> authorList = authorDAO.getAll();

		assertEquals(3, authorList.size());
		assertTrue(authorList.contains((new Author(1, "Kate", null))));
		assertTrue(authorList.contains((new Author(2, "Olya", null))));
		assertTrue(authorList.contains((new Author(3, "Ivan", null))));

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/AuthorDS.xml")
	public void testFindById() throws Exception {

		Author author = authorDAO.getById(2);

		assertEquals(2, author.getAuthorId());
		assertEquals("Olya", author.getAuthorName());

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/AuthorDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {

		Author author = new Author(1, "Kate2", null);

		authorDAO.update(author);

		Author author2 = authorDAO.getById(1);

		assertEquals(author, author2);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/AuthorDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDelete() throws Exception {

		authorDAO.delete(3L);

		assertNotNull(authorDAO.getById(3).getExpired());

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/AuthorDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAuthorId() throws Exception {

		long id3 = authorDAO.getAuthorId(new Author(3, "Ivan", null));

		assertEquals(3, id3);

	}

}
