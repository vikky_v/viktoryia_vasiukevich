package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;


public class NewsServiceImpl implements INewsService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private INewsDAO newsDAO;

	public long create(News ob) throws ServiceException {
		long newsId = 0L;
		if (ob != null) {
			try {
				newsId = newsDAO.create(ob);
			} catch (DAOException e) {
				log.error("Can't create news", e);
				throw new ServiceException("Can't create news", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return newsId;
	}

	public void update(News ob) throws ServiceException {

		if (ob != null) {
			try {
				newsDAO.update(ob);
			} catch (DAOException e) {
				log.error("Can't update news", e);
				throw new ServiceException("Can't update news", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
	}

	public void delete(long id) throws ServiceException {

		if (id > 0) {
			try {
				newsDAO.delete(id);
			} catch (DAOException e) {
				log.error("Can't delete news", e);
				throw new ServiceException("Can't delete news", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}
	
	public News getById(long id) throws ServiceException {
		News news = null;
		if (id > 0) {
			try {
				news = newsDAO.getById(id);
			} catch (DAOException e) {
				log.error("Can't find news", e);
				throw new ServiceException("Can't find news", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return news;
	}

	/**
	 * Method that invokes INewsDAO getFilteredNews method
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return list of news that will be shown
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<News> getListNews(PagedView pagedView, Filter filter)
			throws ServiceException {
		List<News> news = null;

		try {
			news = newsDAO.getNews(pagedView, filter);
		} catch (DAOException e) {
			log.error("Can't get list of news", e);
			throw new ServiceException("Can't get list of news", e);
		}

		return news;
	}

	/**
	 * Method that invokes INewsDAO countFilteredNews method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return number of found news
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public int countNews(Filter filter) throws ServiceException {

		int numberOfNews = 0;
		try {
			numberOfNews = newsDAO.countNews(filter);
		} catch (DAOException e) {
			log.error("Can't get number of news", e);
			throw new ServiceException("Can't get number of news", e);
		}

		return numberOfNews;

	}

	/**
	 * Method that invokes INewsDAO getPrevNextNewsId method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @param newsId
	 *            ID of current news
	 * @return List with previous and next newsId
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws ServiceException {

		List<Long> number = null;

		try {
			number = newsDAO.getPrevNextNewsId(filter, newsId);
		} catch (DAOException e) {
			log.error("Can't get pevious and next id of news", e);
			throw new ServiceException("Can't get pevious and next id of news",
					e);
		}

		return number;
	}

	public Author getAuthorByNews(long newsId) throws ServiceException {
		Author author = null;
		if (newsId > 0) {
			try {
				author = newsDAO.getAuthorByNews(newsId);
			} catch (DAOException e) {
				log.error("Can't get author", e);
				throw new ServiceException("Can't get author", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return author;
	}

	public List<News> getNewsByAuthor(long authorId) throws ServiceException {
		List<News> news = null;
		if (authorId > 0) {
			try {
				news = newsDAO.getNewsByAuthor(authorId);
			} catch (DAOException e) {
				log.error("Can't get news", e);
				throw new ServiceException("Can't get news", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return news;
	}

	public void createNewsAuthor(long newsId, long authorId)
			throws ServiceException {
		if (newsId > 0 && authorId > 0) {

			try {
				newsDAO.createNewsAuthor(newsId, authorId);
			} catch (DAOException e) {
				log.error("Can't create news author row", e);
				throw new ServiceException("Can't create news author row", e);
			}
		} else {
			log.error("newsId <= 0 or authorId <= 0");
			throw new ServiceException("newsId <= 0 or authorId <= 0");
		}

	}

	public void updateNewsAuthor(long newsId, long authorId)
			throws ServiceException {
		if (newsId > 0 && authorId > 0) {
			try {
				newsDAO.updateNewsAuthor(newsId, authorId);
			} catch (DAOException e) {
				log.error("Can't get update news author row", e);
				throw new ServiceException("Can't get update news author row",
						e);
			}
		} else {
			log.error("newsId <= 0 or authorId <= 0");
			throw new ServiceException("newsId <= 0 or authorId <= 0");
		}
	}

	public List<Tag> getTagsByNews(long newsId) throws ServiceException {
		List<Tag> tags = null;
		if (newsId > 0) {
			try {
				tags = newsDAO.getTagsByNews(newsId);
			} catch (DAOException e) {
				log.error("Can't get tag id", e);
				throw new ServiceException("Can't get tag id", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return tags;
	}

	public void createNewsTag(long newsId, long tagId) throws ServiceException {
		if (newsId > 0 && tagId > 0) {
			try {
				newsDAO.createNewsTag(newsId, tagId);
			} catch (DAOException e) {
				log.error("Can't get create news tag row", e);
				throw new ServiceException("Can't get create news tag row", e);
			}
		} else {
			log.error("newsId <= 0 or tagId <= 0");
			throw new ServiceException("newsId <= 0 or tagId <= 0");
		}
	}

	public void deleteNewsTag(long newsId, long tagId) throws ServiceException {
		if (newsId > 0 && tagId > 0) {
			try {
				newsDAO.deleteNewsTag(newsId, tagId);
			} catch (DAOException e) {
				log.error("Can't delete news tag row", e);
				throw new ServiceException("Can't delete news tag row", e);
			}
		} else {
			log.error("newsId <= 0 or tagId <= 0");
			throw new ServiceException("newsId <= 0 or tagId <= 0");
		}
	}

	public List<News> getNewsByTag(long tagId) throws ServiceException {
		List<News> news = null;
		if (tagId > 0) {
			try {
				news = newsDAO.getNewsByTag(tagId);
			} catch (DAOException e) {
				log.error("Can't get news id", e);
				throw new ServiceException("Can't get news id", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return news;
	}

	public void deleteNewsTagByNewsID(long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				newsDAO.deleteNewsTagByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't delete news tag row", e);
				throw new ServiceException("Can't delete news tag row", e);
			}
		} else {
			log.error("newsId <= 0");
			throw new ServiceException("newsId <= 0");
		}

	}

	public void deleteNewsAuthorByNewsID(long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				newsDAO.deleteNewsAuthorByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't delete news author row", e);
				throw new ServiceException("Can't delete news author row", e);
			}
		} else {
			log.error("newsId <= 0");
			throw new ServiceException("newsId <= 0");
		}

	}

}
