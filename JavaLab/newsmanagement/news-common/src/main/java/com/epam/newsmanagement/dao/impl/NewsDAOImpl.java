package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.connection.DBUtil;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.utils.QueryBilderUtil;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

public class NewsDAOImpl implements INewsDAO {

	@Autowired
	private DBUtil dbUtil;

	@Autowired
	private AuthorDAOImpl authorDAO;

	@Autowired
	private TagDAOImpl tagDAO;

	public long create(News ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long newsId = 0L;
		String generatedColumns[] = { "news_id" };
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_NEWS, generatedColumns);
			ps.setString(1, ob.getShortText());
			ps.setString(2, ob.getFullText());
			ps.setString(3, ob.getTitle());
			ps.setTimestamp(4, new Timestamp(ob.getCreationDate().getTime()));
			ps.setTimestamp(5,
					new Timestamp(ob.getModificationDate().getTime()));
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while (rs.next()) {
				newsId = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}
		return newsId;
	}

	public void update(News ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.UPDATE_NEWS);
			ps.setString(1, ob.getShortText());
			ps.setString(2, ob.getFullText());
			ps.setString(3, ob.getTitle());
			ps.setTimestamp(4,
					new Timestamp(ob.getModificationDate().getTime()));
			ps.setLong(5, ob.getNewsId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}
	}

	public void delete(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS);
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}

	}

	public News getById(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		News news = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.FIND_NEWS);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				Timestamp tmstmpCreation = rs.getTimestamp(5);
				Date dateCreation = new Date(tmstmpCreation.getTime());
				Timestamp tmstmpModification = rs.getTimestamp(6);
				Date dateModification = new Date(tmstmpModification.getTime());

				news = new News(rs.getLong(1), rs.getString(2),
						rs.getString(3), rs.getString(4), dateCreation,
						dateModification);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}
		return news;
	}

	/**
	 * Method that get news applying filter by author and tags
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param author
	 *            author of news that must be shown
	 * @param tags
	 *            list of tags depending on which news must be shown
	 * @return list of news that will be shown
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNews(PagedView pagedView, Filter filter)
			throws DAOException {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<News> news = new ArrayList<News>();
		try {
			con = dbUtil.getConnection();
			ps = QueryBilderUtil.getQueryNewsList(con, ps, pagedView, filter);
			rs = ps.executeQuery();
			while (rs.next()) {
				Timestamp tmstmpCreation = rs.getTimestamp(5);
				Date dateCreation = new Date(tmstmpCreation.getTime());
				Timestamp tmstmpModification = rs.getTimestamp(6);
				Date dateModification = new Date(tmstmpModification.getTime());

				news.add(new News(rs.getLong(1), rs.getString(2), rs
						.getString(3), rs.getString(4), dateCreation,
						dateModification));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}

		return news;
	}

	/**
	 * Method that returns number of found news
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return number of found news
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public int countNews(Filter filter) throws DAOException {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		int numberOfNews = 0;

		try {
			con = dbUtil.getConnection();
			ps = QueryBilderUtil.getQueryCountNews(con, ps, filter);
			rs = ps.executeQuery();
			while (rs.next()) {
				numberOfNews = rs.getInt(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}

		return numberOfNews;
	}

	/**
	 * Method that returns List with previous newsId on 0 position and next
	 * newsId on 1st position
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @param newsId
	 *            ID of current news
	 * @return List with previous and next newsId
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws DAOException {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<Long> number = new ArrayList<Long>();

		try {
			con = dbUtil.getConnection();
			ps = QueryBilderUtil.getQueryPrevNextNews(con, ps, filter, newsId);
			rs = ps.executeQuery();
			int indexLast = 0;
			if (!rs.isBeforeFirst()) {
				number.add(0L);
				number.add(0L);
			} else {
				while (rs.next()) {

					if (rs.isLast()) {
						if (indexLast == 0) {
							if (rs.getInt(2) < rs.getInt(4)) {
								number.add(rs.getLong(1));
								number.add(0L);
							} else {
								number.add(0L);
								number.add(rs.getLong(1));
							}
						} else {
							number.add(rs.getLong(1));
							indexLast = 1;
						}
					} else {
						number.add(rs.getLong(1));
						indexLast = 1;
					}
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}

		return number;
	}

	public List<News> getAll() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		List<News> news = new ArrayList<News>();
		try {
			con = dbUtil.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQLQuery.SELECT_ALL_NEWS);
			while (rs.next()) {
				Timestamp tmstmpCreation = rs.getTimestamp(5);
				Date dateCreation = new Date(tmstmpCreation.getTime());
				Timestamp tmstmpModification = rs.getTimestamp(6);
				Date dateModification = new Date(tmstmpModification.getTime());

				news.add(new News(rs.getLong(1), rs.getString(2), rs
						.getString(3), rs.getString(4), dateCreation,
						dateModification));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, st, rs);
		}

		return news;
	}

	public Author getAuthorByNews(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long authorId = 0L;
		Author author = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_AUTHOR_ID_FROM_NEWSAUTHOR);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				authorId = rs.getLong(1);
			}

			if (authorId != 0) {
				author = authorDAO.getById(authorId);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}

		return author;
	}

	public List<News> getNewsByAuthor(long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// List<Long> newsId = new ArrayList<Long>();
		List<News> newsList = new ArrayList<News>();
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_NEWS_ID_FROM_NEWSAUTHOR);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			long newsId = 0L;
			News news = null;
			while (rs.next()) {
				newsId = rs.getLong(1);
				news = getById(newsId);
				newsList.add(news);
				// newsId.add(rs.getLong(1));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}
		// return newsId;
		return newsList;
	}

	public void createNewsAuthor(long newsId, long authorId)
			throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_NEWS_AUTHOR);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}
	}

	public void updateNewsAuthor(long newsId, long authorId)
			throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.UPDATE_NEWS_AUTHOR);
			ps.setLong(2, newsId);
			ps.setLong(1, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}
	}

	public List<Tag> getTagsByNews(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// List<Long> tagId = new ArrayList<Long>();
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_TAG_ID_FROM_NEWSTAG);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			long tagId = 0L;
			Tag tag = null;
			while (rs.next()) {
				tagId = rs.getLong(1);
				tag = tagDAO.getById(tagId);
				tagList.add(tag);

			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}

		return tagList;
	}

	public void createNewsTag(long newsId, long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_NEWS_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}
	}

	public void deleteNewsTag(long newsId, long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}
	}

	public List<News> getNewsByTag(long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<News> newsList = new ArrayList<News>();
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_NEWS_ID_FROM_NEWSTAG);
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			long newsId = 0L;
			News news = null;
			while (rs.next()) {
				newsId = rs.getLong(1);
				news = getById(newsId);
				newsList.add(news);

			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}
		// return newsId;
		return newsList;
	}

	public void deleteNewsTagByNewsID(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS_TAG_BY_NEWSID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}

	}

	public void deleteNewsAuthorByNewsID(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS_AUTHOR_BY_NEWSID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}

	}

}
