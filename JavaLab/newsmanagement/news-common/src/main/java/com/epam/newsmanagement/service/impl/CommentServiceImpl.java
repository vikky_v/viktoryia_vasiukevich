package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.exception.ServiceException;


public class CommentServiceImpl implements ICommentService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ICommentDAO commentDAO;

	public long create(Comment ob) throws ServiceException {
		long commentId = 0L;
		if (ob != null) {
			try {
				commentId = commentDAO.create(ob);
			} catch (DAOException e) {
				log.error("Can't create comment", e);
				throw new ServiceException("Can't create comment", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return commentId;
	}

	public void delete(long id) throws ServiceException {
		if (id > 0) {
			try {
				commentDAO.delete(id);
			} catch (DAOException e) {
				log.error("Can't delete comment", e);
				throw new ServiceException("Can't delete comment", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public void deleteByNewsID(long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				commentDAO.deleteByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't delete comments", e);
				throw new ServiceException("Can't delete comments", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public List<Comment> getByNewsID(long newsId) throws ServiceException {
		List<Comment> listComment = null;
		if (newsId > 0) {
			try {
				listComment = commentDAO.findByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't get list of comments", e);
				throw new ServiceException("Can't get list of comments", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return listComment;
	}

}
