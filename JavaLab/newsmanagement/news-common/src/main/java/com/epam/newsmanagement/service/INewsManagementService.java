package com.epam.newsmanagement.service;

import java.util.Date;
import java.util.List;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.NewsVO;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

/**
 * Interface for main service layer
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface INewsManagementService {

	
	
	/**
	 * Invoke addNews or editNews method depending on news Id
	 * 
	 * @param news News that should be saved
	 * @param authorId Author Id of this News
	 * @param tagsId list of Tags Id for this News
	 * @return ID of saved News
	 * @throws ServiceException
	 */
	public long saveNews(News news, Long authorId, List<Long> tagsId, Date date)
			throws ServiceException;
	
	/**
	 * Add News with Author and Tags
	 * 
	 * @param news
	 *            News that should be added
	 * @param author
	 *            Author of this News
	 * @param tag
	 *            list of Tags for this News
	 * @return ID of added News
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} error occurs
	 */
	public long addNews(News news, Long authorId, List<Long> tagsId)
			throws ServiceException;

	/**
	 * Edit News with Author and Tags
	 * 
	 * @param news
	 *            News that should be edited
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} error occurs
	 */
	public void editNews(News news, Long authorId, List<Long> tagsId)
			throws ServiceException;

	/**
	 * Delete News with Author, Tags and Comments
	 * 
	 * @param newsId
	 *            ID of News that should be deleted
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             CommentServiceException.class} error occurs
	 */
	public void deleteNews(long newsId) throws ServiceException;

	/**
	 * Return single NewsVO (News with Author and Tags)
	 * 
	 * @param newsId
	 *            ID of News
	 * @return NewsVO (News with Author and Tags)
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             AuthorServiceException.class} or {@link
	 *             TagServiceException.class} or {@link
	 *             CommentServiceException.class} error occurs
	 */
	public NewsVO getNews(long newsId) throws ServiceException;

	/**
	 * Add Author if author with equals name doesn't exist
	 * 
	 * @param author
	 *            Author that should be added
	 * @return ID of added Author
	 * @throws ServiceException
	 *             when author with equals name exist or {@link
	 *             AuthorServiceException.class} error occurs
	 */
	public long addAuthor(Author author) throws ServiceException;

	/**
	 * Return list of News associated with specified Author
	 * 
	 * @param ob
	 *            Author which news should be found
	 * @return List of News
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             AuthorServiceException.class} error occurs
	 */
	public List<News> getByAuthor(Author ob) throws ServiceException;

	/**
	 * Add Tag
	 * 
	 * @param tag
	 *            Tag that should be added
	 * @return ID of added Tag
	 * @throws ServiceException
	 *             when {@link TagServiceException.class} error occurs
	 */
	public long addTag(Tag tag) throws ServiceException;

	/**
	 * Add several Tags
	 * 
	 * @param tags
	 *            List of Tags that should be added
	 * @throws ServiceException
	 *             when {@link TagServiceException.class} error occurs
	 */
//	public void addTags(List<Tag> tags) throws ServiceException;

	/**
	 * Return list of News associated at least with one of specified Tags
	 * 
	 * @param tags
	 *            list of Tags for which News should be found
	 * @return List of News
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             TagServiceException.class} error occurs
	 */
	public List<News> getByTags(List<Tag> tags) throws ServiceException;

	/**
	 * Add several Comments
	 * 
	 * @param comments
	 *            list of Comments that should be added
	 * @throws ServiceException
	 *             when {@link CommentServiceException.class} error occurs
	 */
//	public void addComments(List<Comment> comments) throws ServiceException;

	/**
	 * Delete several Comments
	 * 
	 * @param commentIds
	 *            List of Comment ID's that should be deleted
	 * @throws ServiceException
	 *             when {@link CommentServiceException.class} error occurs
	 */
//	public void deleteComments(List<Long> commentIds) throws ServiceException;

	/**
	 * Method that get List of News for current page
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return List of News with Author, Tags, Coments
	 * @throws ServiceException
	 */
	public List<NewsVO> getListNews(PagedView pagedView,
			Filter filter) throws ServiceException;

	/**
	 * Invoke ITagService getAllRows method
	 * 
	 * @return List of Tag objects
	 * @throws ServiceException
	 * 
	 */
	public List<Tag> getAllTags() throws ServiceException;

	/**
	 * Invoke ITagService findById(id) method
	 * 
	 * @param id
	 *            ID of Tag that should be found
	 * @return Tag object
	 * @throws ServiceException
	 * 
	 */
	public Tag getTagById(long id) throws ServiceException;

	/**
	 * Invoke IAuthorService getAllRows method
	 * 
	 * @return List of Author objects
	 * @throws ServiceException
	 * 
	 */
	public List<Author> getAllAuthors() throws ServiceException;

	/**
	 * Invoke IAuthorService update(Author) method
	 * 
	 * @param ob
	 *            Author object that should be updated
	 * @throws ServiceException
	 * 
	 */
	public void updateAuthor(Author ob) throws ServiceException;

	/**
	 * Invoke IAuthorService delete(id) method
	 * 
	 * @param id
	 *            ID of Author that should be deleted
	 * @throws ServiceException
	 * 
	 */
	public void deleteAuthor(long id) throws ServiceException;

	/**
	 * Invokes INewsService countFilteredNews method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return number of found news
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public int countNews(Filter filter) throws ServiceException;

	/**
	 * Invoke ITagService update(Tag) method
	 * 
	 * @param ob
	 *            Tag object that should be created
	 * @throws ServiceException
	 *             when Tag ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void updateTag(Tag ob) throws ServiceException;

	/**
	 * Invoke ITagService delete(id) method
	 * 
	 * @param id
	 *            ID of Tag that should be deleted
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void deleteTag(long id) throws ServiceException;

	/**
	 * Invokes INewsService getPrevNextNewsId method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @param newsId
	 *            ID of current news
	 * @return List with previous and next newsId
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws ServiceException;

	public boolean isAuthorExists(Author author) throws ServiceException;
	public boolean isTagExists(Tag tag) throws ServiceException;

	public long addComment(Comment comment) throws ServiceException;
	public void deleteComment(Long id) throws ServiceException;
	
	
	

}
