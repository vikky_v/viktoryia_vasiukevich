package com.epam.newsmanagement.dao.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.epam.newsmanagement.dao.impl.SQLQuery;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

public final class QueryBilderUtil {

	private QueryBilderUtil() {

	}

	/**
	 * @param con
	 *            Connection
	 * @param ps
	 *            PreparedStatement
	 * @param pagedView
	 *            Defines number of page and News per page
	 * @param filter
	 *            Defines Author and List of Tags for filtering news
	 * @return PreparedStatement for List of News
	 * @throws SQLException
	 */
	public static PreparedStatement getQueryNewsList(Connection con,
			PreparedStatement ps, PagedView pagedView, Filter filter)
			throws SQLException {

		String sql = null;

		Author author = filter.getAuthor();
		List<Tag> tags = filter.getTags();
		int currentPage = pagedView.getCurrentPage();
		int itemPerPage = pagedView.getItemPerPage();

		boolean appendAuthor = false;
		boolean appendTags = false;

		if (author != null) {
			if (author.getAuthorId() != 0) {
				appendAuthor = true;
			}
		}
		if (tags != null) {
			if (!tags.isEmpty()) {
				appendTags = true;
			}
		}

		sql = SQLQuery.SELECT_NEWS;
		if (appendTags) {
			String sign = "?";
			// depending on number of tags we need different number of
			// parameters
			for (int i = 1; i < tags.size(); i++) {
				sign = sign + ",?";
			}
			sql = sql + SQLQuery.APPEND_TAG_START + sign
					+ SQLQuery.APPEND_TAG_END;
		}
		if (appendAuthor) {
			sql = sql + " " + SQLQuery.APPEND_AUTHOR;
		}
		sql = sql + " " + SQLQuery.APPEND_END_NEWS;

		ps = con.prepareStatement(sql);

		// define number of positions where to set ID of tags, author
		// number of current page and item per page
		int paramIndex = 1;
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				ps.setLong(paramIndex++, tags.get(i - 1).getTagId());
			}

		}

		if (appendAuthor) {
			ps.setLong(paramIndex++, author.getAuthorId());
		}

		ps.setInt(paramIndex++, currentPage * itemPerPage + 1);
		ps.setInt(paramIndex++, currentPage * itemPerPage + itemPerPage);

		return ps;
	}

	/**
	 * @param con
	 *            Connection
	 * @param ps
	 *            PreparedStatement
	 * @param filter
	 *            Defines Author and List of Tags for filtering news
	 * @return PreparedStatement for count all news
	 * @throws SQLException
	 */
	public static PreparedStatement getQueryCountNews(Connection con,
			PreparedStatement ps, Filter filter) throws SQLException {

		Author author = filter.getAuthor();
		List<Tag> tags = filter.getTags();

		boolean appendAuthor = false;
		boolean appendTags = false;

		if (author != null) {
			if (author.getAuthorId() != 0) {
				appendAuthor = true;
			}
		}
		if (tags != null) {
			if (!tags.isEmpty()) {
				appendTags = true;
			}
		}
		String sql = SQLQuery.SELECT_COUNT_NEWS;
		String sign = "?";
		if (appendTags) {

			// depending on number of tags we need different number of
			// parameters
			for (int i = 1; i < tags.size(); i++) {
				sign = sign + ",?";
			}
			sql = sql + SQLQuery.APPEND_TAG_START + sign
					+ SQLQuery.APPEND_TAG_END;
		}
		if (appendAuthor) {
			sql = sql + " " + SQLQuery.APPEND_AUTHOR;
		}

		ps = con.prepareStatement(sql);

		// define number of positions where to set ID of tags, author
		// number of current page and item per page
		int paramIndex = 1;
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				ps.setLong(paramIndex++, tags.get(i - 1).getTagId());
			}

		}

		if (appendAuthor) {
			ps.setLong(paramIndex++, author.getAuthorId());
		}

		return ps;
	}

	/**
	 * @param con
	 *            Connection
	 * @param ps
	 *            PreparedStatement
	 * @param filter
	 *            Defines Author and List of Tags for filtering news
	 * @param newsId
	 *            Id of News
	 * @return PreparedStatement to get previous and next news
	 * @throws SQLException
	 */
	public static PreparedStatement getQueryPrevNextNews(Connection con,
			PreparedStatement ps, Filter filter, long newsId)
			throws SQLException {

		Author author = filter.getAuthor();
		List<Tag> tags = filter.getTags();

		boolean appendAuthor = false;
		boolean appendTags = false;

		if (author != null) {
			if (author.getAuthorId() != 0) {
				appendAuthor = true;
			}
		}
		if (tags != null) {
			if (!tags.isEmpty()) {
				appendTags = true;
			}
		}

		String sql = SQLQuery.SELECT_PREV_NEXT_START;
		String sign = "?";
		if (appendTags) {

			// depending on number of tags we need different number of
			// parameters
			for (int i = 1; i < tags.size(); i++) {
				sign = sign + ",?";
			}
			sql = sql + SQLQuery.APPEND_TAG_START + sign
					+ SQLQuery.APPEND_TAG_END;
		}
		if (appendAuthor) {
			sql = sql + " " + SQLQuery.APPEND_AUTHOR;
		}
		sql = sql + " " + SQLQuery.SELECT_PREV_NEXT_MIDDLE;
		if (appendTags) {
			sql = sql + SQLQuery.APPEND_TAG_START + sign
					+ SQLQuery.APPEND_TAG_END;
		}
		if (appendAuthor) {
			sql = sql + " " + SQLQuery.APPEND_AUTHOR;
		}
		sql = sql + " " + SQLQuery.SELECT_PREV_NEXT_END;

		ps = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_UPDATABLE);

		// define number of positions where to set ID of tags, author
		// number of current page and item per page
		int paramIndex = 1;
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				ps.setLong(paramIndex++, tags.get(i - 1).getTagId());
			}
		}
		if (appendAuthor) {
			ps.setLong(paramIndex++, author.getAuthorId());
		}
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				ps.setLong(paramIndex++, tags.get(i - 1).getTagId());
			}
		}
		if (appendAuthor) {
			ps.setLong(paramIndex++, author.getAuthorId());
		}
		ps.setLong(paramIndex++, newsId);

		return ps;
	}
}
