package com.epam.newsmanagement.service;

import com.epam.newsmanagement.service.exception.ServiceException;

public interface IUserService {

	/**
	 * @param login
	 *            login of user
	 * @return name of user
	 * @throws ServiceException
	 */
	public String getNameByLogin(String login) throws ServiceException;
}
