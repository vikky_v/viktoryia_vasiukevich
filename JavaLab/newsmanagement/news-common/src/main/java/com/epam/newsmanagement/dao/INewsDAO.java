package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

/**
 * Extends {@link IDAO<News>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */

public interface INewsDAO extends IDAO<News> {

	/**
	 * Returns Author associated with given News ID
	 * 
	 * @param newsId
	 *            ID of News for which Author ID should be found
	 * @return Author
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public Author getAuthorByNews(long newsId) throws DAOException; // ->getAuthorByNews

	/**
	 * Returns list of News ID associated with given Authors ID
	 * 
	 * @param authorId
	 *            Author ID for which list of News ID should be found
	 * @return List of News
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNewsByAuthor(long authorId) throws DAOException; // ->getNewsByAuthor

	/**
	 * Creates relation between News ID and Author ID
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Author ID
	 * @param authorId
	 *            Author ID that should be connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void createNewsAuthor(long newsId, long authorId)
			throws DAOException;

	/**
	 * Updates relation between News ID and Author ID. Connect with specified
	 * News ID new Author ID
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Author ID
	 * @param authorId
	 *            Author ID that should be connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void updateNewsAuthor(long newsId, long authorId)
			throws DAOException;

	/**
	 * Returns list of Tags associated with given News ID
	 * 
	 * @param newsId
	 *            News ID for which list of Tag ID should be found
	 * @return List of Tag
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<Tag> getTagsByNews(long newsId) throws DAOException; // getTagsByNews

	/**
	 * Creates relation between News ID and Tag ID
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Tag ID
	 * @param tagId
	 *            Tag ID that should be connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void createNewsTag(long newsId, long tagId) throws DAOException;

	/**
	 * Deletes relation between News ID and Tag ID
	 * 
	 * @param newsId
	 *            News ID that is connected with given Tag ID
	 * @param tagId
	 *            Tag ID that is connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteNewsTag(long newsId, long tagId) throws DAOException;

	/**
	 * Returns list of News associated with given Tag ID
	 * 
	 * @param tagId
	 *            Tag ID for which list of News ID should be found
	 * @return List of News
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNewsByTag(long tagId) throws DAOException; 

	/**
	 * Deletes all relations between News ID and Tag ID by News ID
	 * 
	 * @param newsId
	 *            News ID for which should be deleted all relations
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteNewsTagByNewsID(long newsId) throws DAOException;

	/**
	 * Deletes all relations between News ID and Author ID by News ID
	 * 
	 * @param newsId
	 *            News ID for which should be deleted all relations
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteNewsAuthorByNewsID(long newsId) throws DAOException;

	/**
	 * Method that get news applying filter by author and tags
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return list of news that will be shown
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNews(PagedView pagedView,
			Filter filter) throws DAOException;

	/**
	 * Method that returns number of found news
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return number of found news
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public int countNews(Filter filter) throws DAOException;
	
	/**
	 * Method that returns List with previous newsId on 0 position and next
	 * newsId on 1st position
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @param newsId
	 *            ID of current news
	 * @return List with previous and next newsId
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws DAOException;
}
