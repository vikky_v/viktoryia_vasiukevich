package com.epam.newsmanagement.utils;


/**
 * Class for creating pagination on jsp pages
 *
 */
public class PagedView {

	public PagedView() {

	}

	/**
	 * Number of current page
	 */
	private int currentPage;

	/**
	 * Number of items that should be shown on one page
	 */
	private static int itemPerPage;

	/**
	 * Number of rows of the whole list
	 */
	private int rowCount;

	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * Method for setting number of current page. If parameter currentPage less
	 * then 0 it set to 0 If currentPage more than number of pages it set to
	 * last page number
	 * 
	 * @param currentPage
	 *            number of current page
	 */
	public void setCurrentPage(int currentPage) {

		if (currentPage < 0)
			this.currentPage = 0;
		else if (currentPage > (getPageCount() - 1)) {
			this.currentPage = getPageCount() - 1;
			if (this.currentPage < 0)
				this.currentPage = 0;
		} else
			this.currentPage = currentPage;
	}

	public static void setItemPerPage(int itemPerPage) {
		PagedView.itemPerPage = itemPerPage;
	}

	public int getItemPerPage() {
		return itemPerPage;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return number of pages
	 */
	public int getPageCount() {
		return (int) Math.ceil((double) rowCount / itemPerPage);
	}

	/**
	 * @return number of previous page or 0 if current page number is 0
	 */
	public int getPrevIndex() {
		int prev = currentPage - 1;
		return prev < 0 ? 0 : prev;
	}

	/**
	 * @return number of next page or last number if current page is last
	 */
	public int getNextIndex() {
		int lastIndex = getPageCount() - 1;
		int next = currentPage + 1;
		return next > lastIndex ? lastIndex : next;
	}

	/**
	 * @return true if current page is first, false in other case
	 */
	public boolean isFirstPage() {
		return 0 == currentPage;
	}

	/**
	 * @return true if current page is last, false in other case
	 */
	public boolean isLastPage() {
		return (getPageCount() - 1) == currentPage;
	}

	/**
	 * Method for getting array of page numbers
	 * 
	 * @return array of page numbers
	 */
	public int[] getIndexList() {

		int numberOfPages = getPageCount();
		int[] indexList = new int[numberOfPages];
		for (int i = 0; i < numberOfPages; i++) {
			indexList[i] = i + 1;
		}
		return indexList;
	}

}
