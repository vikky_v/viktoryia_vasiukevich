package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;

/**
 * Interface performing CRUD operations and getAllRows operation
 * 
 * @author Viktoryia Vasiukevich
 * 
 * @param <T>
 *            the type of entity, can be {@link com.epam.news.dto.Author},
 *            {@link com.epam.news.dto.Comment}, {@link com.epam.news.dto.News},
 *            {@link com.epam.news.dto.Tag}
 */
public interface IDAO<T> {

	/**
	 * @param ob
	 *            entity that should be created
	 * @return ID of created entity
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	long create(T ob) throws DAOException;

	/**
	 * @param ob
	 *            entity that should be updated
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	void update(T ob) throws DAOException;

	/**
	 * @param id
	 *            ID of entity that should be deleted
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	void delete(long id) throws DAOException;

	/**
	 * @param id
	 *            ID of entity that should be found
	 * @return entity
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	T getById(long id) throws DAOException;

	/**
	 * @return List of entities
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	List<T> getAll() throws DAOException;

}
