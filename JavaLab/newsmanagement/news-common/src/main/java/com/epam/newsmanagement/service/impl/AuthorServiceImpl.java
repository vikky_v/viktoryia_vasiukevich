package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;


public class AuthorServiceImpl implements IAuthorService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private IAuthorDAO authorDAO;

	public long create(Author ob) throws ServiceException {

		long authorId = 0;
		if (ob != null) {

			try {
				authorId = authorDAO.create(ob);
			} catch (DAOException e) {
				log.error("Can't create author", e);
				throw new ServiceException("Can't create author", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}

		return authorId;
	}

	public void update(Author ob) throws ServiceException {

		if (ob != null) {
			try {
				authorDAO.update(ob);
			} catch (DAOException e) {
				log.error("Can't update author", e);
				throw new ServiceException("Can't update author", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
	}

	public void delete(long id) throws ServiceException {

		if (id > 0) {
			try {
				authorDAO.delete(id);
			} catch (DAOException e) {
				log.error("Can't delete author", e);
				throw new ServiceException("Can't delete author", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public Author findById(long id) throws ServiceException {
		Author author = null;
		if (id > 0) {
			try {
				author = authorDAO.getById(id);
			} catch (DAOException e) {
				log.error("Can't find author", e);
				throw new ServiceException("Can't find author", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}

		return author;
	}

	public long getAuthorId(Author ob) throws ServiceException {
		long id = 0L;
		if (ob != null) {
			try {
				id = authorDAO.getAuthorId(ob);
			} catch (DAOException e) {
				log.error("Can't get id", e);
				throw new ServiceException("Can't get id", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return id;
	}

	public List<Author> getAll() throws ServiceException {
		List<Author> listAuthor = null;
		try {
			listAuthor = authorDAO.getAll();
		} catch (DAOException e) {
			log.error("Can't get list of Authors", e);
			throw new ServiceException("Can't get list of Authors", e);
		}
		return listAuthor;
	}

}
