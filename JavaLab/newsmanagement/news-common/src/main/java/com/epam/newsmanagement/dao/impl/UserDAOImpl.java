package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.connection.DBUtil;
import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.dao.exception.DAOException;

public class UserDAOImpl implements IUserDAO {

	@Autowired
	private DBUtil dbUtil;

	public String getNameByLogin(String login) throws DAOException {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String userName = null;

		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_USER_NAME_BY_LOGIN);
			ps.setString(1, login);
			rs = ps.executeQuery();

			while (rs.next()) {
				userName = rs.getString(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}

		return userName;
	}

}
