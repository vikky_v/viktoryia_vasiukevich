package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.NewsVO;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;


public class NewsManagementServiceImpl implements INewsManagementService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private ICommentService commentService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private ITagService tagService;


	/**
	 * Method that get List of News for current page
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return List of News with Author, Tags, Coments
	 * @throws ServiceException
	 */
	@Transactional(rollbackFor = Exception.class)
	public List<NewsVO> getListNews(PagedView pagedView,
			Filter filter) throws ServiceException {

		List<NewsVO> listOfNewsTO = new ArrayList<NewsVO>();
		try {

			// get list of News without tags, author, comments

			List<News> listOfNews = newsService.getListNews(
					pagedView, filter);
			
			
			Iterator<News> it = listOfNews.iterator();
			while (it.hasNext()) {

				// for each News get Author
				News news = it.next();
				long newsId = news.getNewsId();

				Author author = newsService.getAuthorByNews(newsId);

				// get list of Tags by news id
				List<Tag> listOfTags = newsService.getTagsByNews(newsId);

				// get list of comments by news id
				List<Comment> listOfComments = commentService
						.getByNewsID(newsId);

				// create NewsVO (News with Author, Comments, Tags)
				NewsVO newsTO = new NewsVO(news, author, listOfTags,
						listOfComments);

				listOfNewsTO.add(newsTO);

			}

		} catch (ServiceException e) {
			log.error("Exception - showAllNews", e);
			throw new ServiceException("Can't show all news", e);
		}

		return listOfNewsTO;
	}


	@Transactional(rollbackFor = Exception.class)
	public long addNews(News news, Long authorId, List<Long> tagsId)
			throws ServiceException {

		long newsId = 0L;
		try {

			if (authorId != null) {

				// get news id

				
				newsId = newsService.create(news);

				// create news_author row
				newsService.createNewsAuthor(newsId, authorId);

				if (tagsId != null) {

					// for each Tag get tag id and
					// create news_tag row
					Iterator<Long> it = tagsId.iterator();
					while (it.hasNext()) {
						
						long tagId = it.next();
						newsService.createNewsTag(newsId, tagId);
					}
				}
			}

		} catch (ServiceException e) {
			log.error("ServiceException - addNews", e);
			throw new ServiceException("News wasn't added", e);
		}
		return newsId;
	}

	@Transactional(rollbackFor = Exception.class)
	public void editNews(News news, Long authorId, List<Long> tagsId)
			throws ServiceException {

		try {
			newsService.update(news);
			long newsId = news.getNewsId();
			
			newsService.updateNewsAuthor(newsId, authorId);

			// delete previous tags
			newsService.deleteNewsTagByNewsID(newsId);

			// insert new tags
			
			for(Long tagId: tagsId){
				
				newsService.createNewsTag(newsId, tagId);
			}

		} catch (ServiceException e) {
			log.error("ServiceException - editNews", e);
			throw new ServiceException("News wasn't updated", e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void deleteNews(long newsId) throws ServiceException {

		try {
			newsService.deleteNewsTagByNewsID(newsId);
			newsService.deleteNewsAuthorByNewsID(newsId);
			commentService.deleteByNewsID(newsId);
			newsService.delete(newsId);
		} catch (ServiceException e) {
			log.error("Exception - deleteNewsTO", e);
			throw new ServiceException("News wasn't deleted", e);
		}

	}

	@Transactional(rollbackFor = Exception.class)
	public NewsVO getNews(long newsId) throws ServiceException {
		NewsVO singleNewsVO = null;
		try {
			News singleNews = newsService.getById(newsId);

			Author author = newsService.getAuthorByNews(newsId);

			List<Tag> tags = newsService.getTagsByNews(newsId);

			List<Comment> comments = commentService.getByNewsID(newsId);

			singleNewsVO = new NewsVO(singleNews, author, tags, comments);

		} catch (ServiceException e) {
			log.error("Exception - showSingleNewsTO", e);
			throw new ServiceException("Can't show news", e);
		}
		return singleNewsVO;
	}

	public long addAuthor(Author author) throws ServiceException {
		long authorId = 0L;
		try {
			// if author with equals name doesn't exist
			if (!isAuthorExists(author)) {
				authorId = authorService.create(author);
			} else {
				throw new ServiceException(
						"Can't add Author. Author has already exist");
			}
		} catch (ServiceException e) {
			log.error("ServiceException - addAuthor", e);
			throw new ServiceException("Can't add author", e);
		}
		return authorId;
	}

	@Transactional(rollbackFor = Exception.class)
	public List<News> getByAuthor(Author ob) throws ServiceException {
		List<News> authorNews = new ArrayList<News>();
		try {
			// get author id by name
			long authorId = authorService.getAuthorId(ob);

			if (authorId != 0) {
				authorNews = newsService.getNewsByAuthor(authorId);
			}
		} catch (ServiceException e) {
			log.error("Exception -  searchByAuthor ", e);
			throw new ServiceException("Can't search by author", e);
		}
		return authorNews;

	}

	public long addTag(Tag tag) throws ServiceException {
		long tagId = 0L;
		try {
			if (!isTagExists(tag)) {
				tagId = tagService.create(tag);
			} else {
				throw new ServiceException(
						"Can't add Tag. Tag has already exist");
			}
		} catch (ServiceException e) {
			log.error("ServiceException -  addTag ", e);
			throw new ServiceException("Can't add tag", e);
		}
		return tagId;

	}

	@Transactional(rollbackFor = Exception.class)
	public List<News> getByTags(List<Tag> tags) throws ServiceException {
		// list of news which have at least one of such tags
		List<News> tagNews = new ArrayList<News>();
		try {

			// list of news which has at least one tag
			List<News> oneTagNews = new ArrayList<News>();

			// for each tag search it in DB and return its id
			Iterator<Tag> it = tags.iterator();
			while (it.hasNext()) {
				Tag current = it.next();
				long tagId = current.getTagId();

				// select news id's which have such tag
				oneTagNews = newsService.getNewsByTag(tagId);

				// if there are such news
				if (oneTagNews.size() != 0) {
					Iterator<News> itNews = oneTagNews.iterator();
					while (itNews.hasNext()) {

						News currentNews = itNews.next();

						// if list of news doesn't contain such news - add it
						if (!tagNews.contains(currentNews)) {
							tagNews.add(currentNews);
						}
					}
				}
			}
		} catch (ServiceException e) {
			log.error("Exception -  searchByTags ", e);
			throw new ServiceException("Can't search by tags", e);
		}
		return tagNews;

	}


	/**
	 * Check by name if specified Author exists
	 * 
	 * @param author
	 *            Author that should be checked
	 * @return true - if Author with equals name exists, false - in another case
	 * 
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public boolean isAuthorExists(Author author) throws ServiceException {
		long dbAuthorId = 0L;
		dbAuthorId = authorService.getAuthorId(author);

		return (dbAuthorId != 0);
	}

	/**
	 * Check by name if specified Tag exists
	 * 
	 * @param tag
	 *            Tag that should be checked
	 * @return true - if Tag with equals name exists, false - in another case
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public boolean isTagExists(Tag tag) throws ServiceException {
		String tagName = tag.getTagName();
		Tag dbTag = tagService.getByName(tagName);
		if (dbTag == null) {
			return false;
		} else
			return true;

	}

	public List<Tag> getAllTags() throws ServiceException {

		List<Tag> listTag = tagService.getAll();

		return listTag;
	}

	public Tag getTagById(long id) throws ServiceException {

		Tag tag = tagService.getById(id);

		return tag;
	}

	public List<Author> getAllAuthors() throws ServiceException {

		List<Author> listAuthor = authorService.getAll();

		return listAuthor;
	}

	public void updateAuthor(Author ob) throws ServiceException {

		authorService.update(ob);

	}

	public void deleteAuthor(long id) throws ServiceException {

		authorService.delete(id);

	}

	public int countNews(Filter filter) throws ServiceException {

		int numberNews = newsService.countNews(filter);

		return numberNews;
	}

	public void updateTag(Tag ob) throws ServiceException {

		tagService.update(ob);
	}

	public void deleteTag(long id) throws ServiceException {

		tagService.delete(id);
	}

	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws ServiceException {

		List<Long> listId = newsService.getPrevNextNewsId(filter, newsId);
		
		return listId;
	}


	public long addComment(Comment comment) throws ServiceException {
		
		return commentService.create(comment);
		
	}


	public long saveNews(News news, Long authorId, List<Long> tagsId,Date date)
			throws ServiceException {

		long newsId = news.getNewsId();
		if (news.getNewsId() == 0L) {
			news.setCreationDate(date);
			newsId = addNews(news, authorId, tagsId);
		}
		else{
			editNews(news,authorId,tagsId);
		}
		
		return newsId;
	}


	public void deleteComment(Long id) throws ServiceException {

		commentService.delete(id);
		
	}

}
