package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.connection.DBUtil;
import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Comment;

public class CommentDAOImpl implements ICommentDAO {

	@Autowired
	private DBUtil dbUtil;

	public long create(Comment ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long commentId = 0L;
		String generatedColumns[] = { "comment_id" };
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_COMMENT, generatedColumns);
			ps.setLong(1, ob.getNewsId());
			ps.setString(2, ob.getCommentText());
			ps.setTimestamp(3, new Timestamp(new Date().getTime()));
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while (rs.next()) {
				commentId = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}
		return commentId;
	}

	public void update(Comment ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.UPDATE_COMMENT);
			ps.setString(1, ob.getCommentText());
			ps.setLong(2, ob.getCommentId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}

	}

	public void delete(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_COMMENT);
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}

	}

	public void deleteByNewsID(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_COMMENT_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps);
		}

	}

	public List<Comment> findByNewsID(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<Comment> comments = new ArrayList<Comment>();
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.FIND_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Timestamp tmstmp = rs.getTimestamp(3);
				Date date = null;
				if (tmstmp != null) {
					date = new Date(tmstmp.getTime());
				}
				comments.add(new Comment(rs.getLong(1), rs.getString(2), date,
						rs.getLong(4)));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(con, ps, rs);
		}
		return comments;

	}

}
