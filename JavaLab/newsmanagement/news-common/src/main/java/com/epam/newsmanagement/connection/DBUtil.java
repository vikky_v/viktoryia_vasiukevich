package com.epam.newsmanagement.connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * DBUtil - class for manage connections, contains its own Connection Pool
 * 
 * @author Viktoryia Vasiukevich
 * @see java.sql.Connection
 */
public class DBUtil {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private DataSource dataSource;

	/**
	 * Get Connection from {@link #dataSource}
	 * 
	 * @return {@link Connection}
	 */
	public Connection getConnection() {
		Connection con = null;
		con = DataSourceUtils.getConnection(dataSource);
		return con;

	}

	/**
	 * Closes {@link ResultSet}
	 * 
	 * @param rs
	 *            ResultSet that should be closed
	 */
	public void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				log.error("Error while closing ResultSet", e);
			}
		}
	}

	/**
	 * Closes {@link Statement}
	 * 
	 * @param st
	 *            Statement that should be closed
	 */
	public void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				log.error("Error while closing Statement", e);
			}
		}
	}

	/**
	 * Closes {@link PreparedStatement}
	 * 
	 * @param ps
	 *            PreparedStatement that should be closed
	 */
	public void close(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				log.error("Error while closing PreparedStatement", e);
			}
		}
	}

	/**
	 * Closes connection
	 * 
	 * @param con
	 *            Connection that should be closed
	 */
	public void close(Connection con) {
		if (con != null) {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/**
	 * @param con
	 *            Connection that should be closed
	 * @param ps
	 *            PreparedStatement that should be closed
	 * @param rs
	 *            ResultSet that should be closed
	 */
	public void close(Connection con, PreparedStatement ps, ResultSet rs) {
		close(con);
		close(ps);
		close(rs);
	}

	/**
	 * @param con
	 *            Connection that should be closed
	 * @param ps
	 *            PreparedStatement that should be closed
	 */
	public void close(Connection con, PreparedStatement ps) {
		close(con);
		close(ps);
	}

	/**
	 * @param con
	 *            Connection that should be closed
	 * @param ps
	 *            Statement that should be closed
	 * @param rs
	 *            ResultSet that should be closed
	 */
	public void close(Connection con, Statement st, ResultSet rs) {
		close(con);
		close(st);
		close(rs);
	}

}
