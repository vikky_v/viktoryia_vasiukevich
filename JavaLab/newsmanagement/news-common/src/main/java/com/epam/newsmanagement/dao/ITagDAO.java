package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Tag;

/**
 * Extends {@link IDAO<Tag>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface ITagDAO extends IDAO<Tag> {

	/**
	 * Returns Tag object by specified name
	 * 
	 * @param name
	 *            Name of Tag that should be found
	 * @return Tag object with specified name
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public Tag getByName(String name) throws DAOException;

}
