package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Comment;

/**
 * Extends {@link IDAO<Comment>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface ICommentDAO  {

	/**
	 * @param ob
	 *            Comment that should be created
	 * @return ID of created entity
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	long create(Comment ob) throws DAOException;

	/**
	 * @param ob
	 *            Comment that should be updated
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	void update(Comment ob) throws DAOException;

	/**
	 * @param id
	 *            ID of Comment that should be deleted
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	void delete(long id) throws DAOException;
	
	/**
	 * Delete all Comments found by News' ID
	 * 
	 * @param newsId
	 *            ID of News which comments should be deleted
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteByNewsID(long newsId) throws DAOException;

	/**
	 * Search all Comments for specified News' ID
	 * 
	 * @param newsId
	 *            ID of News which comments should be found
	 * @return List of Comments
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<Comment> findByNewsID(long newsId) throws DAOException;

}
