package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.exception.ServiceException;


public class TagServiceImpl implements ITagService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ITagDAO tagDAO;

	public long create(Tag ob) throws ServiceException {
		long tagId = 0L;
		if (ob != null) {
			try {
				tagId = tagDAO.create(ob);
			} catch (DAOException e) {
				log.error("Can't create tag", e);
				throw new ServiceException("Can't create tag", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return tagId;
	}

	public void update(Tag ob) throws ServiceException {
		if (ob != null) {
			try {
				tagDAO.update(ob);
			} catch (DAOException e) {
				log.error("Can't update tag", e);
				throw new ServiceException("Can't update tag", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
	}

	public void delete(long id) throws ServiceException {
		if (id > 0) {
			try {
				tagDAO.delete(id);
			} catch (DAOException e) {
				log.error("Can't delete tag", e);
				throw new ServiceException("Can't delete tag", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public Tag getById(long id) throws ServiceException {
		Tag tag = null;
		if (id > 0) {
			try {
				tag = tagDAO.getById(id);
			} catch (DAOException e) {
				log.error("Can't find tag", e);
				throw new ServiceException("Can't find tag", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return tag;
	}

	public Tag getByName(String name) throws ServiceException {
		Tag tag = null;
		if (name != null) {
			try {
				tag = tagDAO.getByName(name);
			} catch (DAOException e) {
				log.error("Can't find tag", e);
				throw new ServiceException("Can't find tag", e);
			}
		} else {
			log.error("Name is null");
			throw new ServiceException("Name is null");
		}
		return tag;
	}

	public List<Tag> getAll() throws ServiceException {
		List<Tag> listTag = null;
		try {
			listTag = tagDAO.getAll();
		} catch (DAOException e) {
			log.error("Can't get list of tags", e);
			throw new ServiceException("Can't get list of tags", e);
		}

		return listTag;
	}
}
