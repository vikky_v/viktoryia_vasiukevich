package com.epam.newsmanagement.dao.impl;

public class SQLQuery {

	/* AuthorDAOImpl */
	public final static String INSERT_AUTHOR = "INSERT INTO author(author_id,author_name,expired) VALUES ((sequence_author.nextval),?,null)";
	public final static String UPDATE_AUTHOR = "UPDATE author SET author_name=? WHERE author_id=?";
	public final static String FIND_AUTHOR = "SELECT author_id, author_name, expired FROM author WHERE author_id=?";
	public final static String DELETE_AUTHOR = "UPDATE author SET expired=? WHERE author_id=?";
	public final static String SELECT_ALL_AUTHORS = "SELECT author_id, author_name, expired FROM author WHERE expired IS NULL ORDER BY author_id";
	public final static String FIND_AUTHOR_ID_BY_NAME = "SELECT author_id FROM author WHERE author_name=?";

	/* NewsDAOImpl */
	public final static String INSERT_NEWS = "INSERT INTO news(news_id, short_text, full_text, title, creation_date, modification_date) VALUES ((sequence_news.nextval),?,?,?,?,?)";
	public final static String UPDATE_NEWS = "UPDATE news SET short_text=?, full_text=?, title=?, modification_date=? WHERE news_id=?";
	public final static String FIND_NEWS = "SELECT news_id,short_text,full_text,title,creation_date,modification_date FROM news WHERE news_id=?";
	public final static String DELETE_NEWS = "DELETE FROM news WHERE news_id=?";
	public final static String SELECT_ALL_NEWS = "SELECT news.news_id, news.short_text, news.full_text, news.title, news.creation_date, news.modification_date FROM news LEFT JOIN (SELECT news_id,count(*) AS c  FROM comments GROUP BY news_id ORDER BY c DESC) s ON news.news_id=s.news_id ORDER BY COALESCE(s.c,0) DESC, news.modification_date DESC";
	
	public final static String SELECT_NEWS ="WITH newsm AS(SELECT news.news_id, news.short_text, news.full_text, news.title, news.creation_date, news.modification_date FROM news LEFT JOIN (SELECT news_id, count(*) AS c FROM comments GROUP BY news_id ORDER BY c DESC) s ON news.news_id=s.news_id ORDER BY COALESCE(s.c,0) DESC, news.modification_date DESC) SELECT * FROM (SELECT newsm.news_id, newsm.short_text, newsm.full_text, newsm.title, newsm.creation_date, newsm.modification_date, ROWNUM rnum FROM newsm";
	public final static String SELECT_COUNT_NEWS ="WITH newsm AS(SELECT news.news_id, news.short_text, news.full_text, news.title, news.creation_date,news.modification_date FROM news) SELECT COUNT(*) from newsm";
	public final static String APPEND_END_NEWS =")WHERE rnum BETWEEN ? AND ?";
	public final static String APPEND_END_ALL_NEWS =")";
	public final static String APPEND_AUTHOR =" JOIN news_author ON newsm.news_id=news_author.news_id WHERE author_id=?";
	public final static String APPEND_TAG_START =" JOIN (SELECT news_id, listagg(tag_id, '; ') WITHIN GROUP (ORDER BY news_tag_id) FROM news_tag WHERE tag_id IN (";
	public final static String APPEND_TAG_END =") GROUP BY news_id)k ON newsm.news_id=k.news_id";
	
	public final static String SELECT_PREV_NEXT_START = "WITH newsm AS(SELECT news.news_id, news.short_text, news.full_text, news.title, news.creation_date, news.modification_date FROM news LEFT JOIN (SELECT news_id, count(*) AS c FROM comments GROUP BY news_id ORDER BY c DESC) s ON news.news_id=s.news_id ORDER BY COALESCE(s.c,0) DESC, news.modification_date DESC) SELECT * FROM (SELECT newsm.news_id, ROWNUM rnum1 FROM newsm ";
	public final static String SELECT_PREV_NEXT_MIDDLE = ")q1 join (SELECT newsm.news_id, ROWNUM rnum2 FROM newsm ";
	public final static String SELECT_PREV_NEXT_END = ") q2 on q2.news_id=? where q1.rnum1 in (q2.rnum2-1, q2.rnum2+1) order by q1.rnum1";
	
	/* CommentDAOImpl */
	public final static String INSERT_COMMENT = "INSERT INTO comments(comment_id, news_id, comment_text, creation_date) VALUES ((sequence_comments.nextval),?,?,?)";
	public final static String UPDATE_COMMENT = "UPDATE comments SET comment_text=? WHERE comment_id=?";
	public final static String FIND_COMMENT = "SELECT comment_id,comment_text,creation_date,news_id FROM comments WHERE comment_id=?";
	public final static String DELETE_COMMENT = "DELETE FROM comments WHERE comment_id=?";
	public final static String SELECT_ALL_COMMENTS = "SELECT comment_id,comment_text,creation_date,news_id FROM comments";
	public final static String DELETE_COMMENT_BY_NEWS_ID = "DELETE FROM comments WHERE news_id=?";
	public final static String FIND_BY_NEWS_ID = "SELECT comment_id,comment_text,creation_date,news_id FROM comments WHERE news_id=? ORDER BY creation_date";

	/* TagDAOImpl */
	public final static String INSERT_TAG = "INSERT INTO tag(tag_id,tag_name) VALUES ((sequence_tag.nextval),?)";
	public final static String UPDATE_TAG = "UPDATE tag SET tag_name=? WHERE tag_id=?";
	public final static String FIND_TAG = "SELECT tag_id,tag_name FROM tag WHERE tag_id=?";
	public final static String DELETE_TAG = "DELETE FROM tag WHERE tag_id=?";
	public final static String SELECT_ALL_TAGS = "SELECT tag_id,tag_name FROM tag";
	public final static String FIND_TAG_BY_NAME = "SELECT tag_id,tag_name FROM tag WHERE tag_name=?";

	/* NewsDAOImpl for tables news_author and news_tag */
	public final static String GET_AUTHOR_ID_FROM_NEWSAUTHOR = "SELECT author_id FROM news_author WHERE news_id=?";
	public final static String GET_NEWS_ID_FROM_NEWSAUTHOR = "SELECT news_id FROM news_author WHERE author_id=?";
	public final static String GET_TAG_ID_FROM_NEWSTAG = "SELECT tag_id FROM news_tag WHERE news_id=?";
	public final static String GET_ALL_ROWS_NEWS_AUTHOR = "SELECT news_author_id,news_id,author_id FROM news_author";
	public final static String GET_ALL_ROWS_NEWS_TAG = "SELECT news_tag_id,news_id,tag_id FROM news_tag";
	public final static String INSERT_NEWS_AUTHOR = "INSERT INTO news_author(news_author_id,news_id, author_id) VALUES((sequence_news_author.nextval),?,?)";
	public final static String INSERT_NEWS_TAG = "INSERT INTO news_tag(news_tag_id,news_id, tag_id) VALUES((sequence_news_tag.nextval),?,?)";
	public final static String UPDATE_NEWS_AUTHOR = "UPDATE news_author SET author_id=? WHERE news_id=?";
	public final static String DELETE_NEWS_TAG = "DELETE FROM news_tag WHERE news_id=? AND tag_id=?";
	public final static String GET_NEWS_ID_FROM_NEWSTAG = "SELECT news_id FROM news_tag WHERE tag_id=?";
	public final static String DELETE_NEWS_TAG_BY_NEWSID = "DELETE FROM news_tag WHERE news_id=?";
	public final static String DELETE_NEWS_AUTHOR_BY_NEWSID = "DELETE FROM news_author WHERE news_id=?";

	/* UserDAOIMpl */
	public final static String GET_USER_NAME_BY_LOGIN = "SELECT user_name FROM user_table WHERE login = ?";

}
