package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

/**
 * Interface for service layer for {@link INewsDAO.class}
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface INewsService {

	/**
	 * Check if News object is null and then invoke INewsDAO create(News) method
	 * 
	 * @param ob
	 *            News object that should be created
	 * @return ID of created News
	 * @throws ServiceException
	 *             when News ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public long create(News ob) throws ServiceException;

	/**
	 * Check if News object is null and then invoke INewsDAO update(News) method
	 * 
	 * @param ob
	 *            News object that should be updated
	 * @throws ServiceException
	 *             when News ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void update(News ob) throws ServiceException;

	/**
	 * Check if id==0 and then invoke INewsDAO delete(id) method
	 * 
	 * @param id
	 *            ID of News that should be deleted
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void delete(long id) throws ServiceException;

	/**
	 * Check if id==0 and then invoke INewsDAO findById(id) method
	 * 
	 * @param id
	 *            ID of News that should be found
	 * @return News object
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public News getById(long id) throws ServiceException;

	/**
	 * Check if newsId==0 and then invoke INewsDAO getAuthorByNews(newsId)
	 * method
	 * 
	 * @param newsId
	 *            ID of News for which Author ID should be found
	 * @return Author
	 * @throws ServiceException
	 *             when newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public Author getAuthorByNews(long newsId) throws ServiceException;

	/**
	 * Check if authorId==0 and then invoke INewsDAO getNewsByAuthor(authorId)
	 * method
	 * 
	 * @param authorId
	 *            Author ID for which list of News ID should be found
	 * @return List of News
	 * @throws ServiceException
	 *             when authorId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public List<News> getNewsByAuthor(long authorId) throws ServiceException;

	/**
	 * Check if authorId==0 and newsId==0 and then invoke INewsDAO
	 * createNewsAuthor(newsId,authorId) method
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Author ID
	 * @param authorId
	 *            Author ID that should be connected with given News ID
	 * @throws ServiceException
	 *             when authorId=0 or newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void createNewsAuthor(long newsId, long authorId)
			throws ServiceException;

	/**
	 * Check if authorId==0 and newsId==0 and then invoke INewsDAO
	 * updateNewsAuthor(newsId,authorId) method
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Author ID
	 * @param authorId
	 *            Author ID that should be connected with given News ID
	 * @throws ServiceException
	 *             when authorId=0 or newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void updateNewsAuthor(long newsId, long authorId)
			throws ServiceException;

	/**
	 * Check if newsId==0 and then invoke INewsDAO getTagsByNews(newsId) method
	 * 
	 * @param newsId
	 *            News ID for which list of Tag ID should be found
	 * @return List of Tags
	 * @throws ServiceException
	 *             when newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public List<Tag> getTagsByNews(long newsId) throws ServiceException;

	/**
	 * Check if tagId==0 and newsId==0 and then invoke INewsDAO
	 * createNewsTag(newsId,tagId) method
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Tag ID
	 * @param tagId
	 *            Tag ID that should be connected with given News ID
	 * @throws ServiceException
	 *             when tagId=0 or newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void createNewsTag(long newsId, long tagId) throws ServiceException;

	/**
	 * Check if tagId==0 and newsId==0 and then invoke INewsDAO
	 * deleteNewsTag(newsId,tagId) method
	 * 
	 * @param newsId
	 *            News ID that is connected with given Tag ID
	 * @param tagId
	 *            Tag ID that is connected with given News ID
	 * @throws ServiceException
	 *             when tagId=0 or newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void deleteNewsTag(long newsId, long tagId) throws ServiceException;

	/**
	 * Check if tagId==0 and then invoke INewsDAO getNewsByTag(tagId) method
	 * 
	 * @param tagId
	 *            Tag ID for which list of News ID should be found
	 * @return List of News
	 * @throws ServiceException
	 *             when tagId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public List<News> getNewsByTag(long tagId) throws ServiceException;

	/**
	 * Check if newsId==0 and then invoke INewsDAO deleteNewsTagByNewsID(newsId)
	 * method
	 * 
	 * @param newsId
	 *            News ID for which should be deleted all relations
	 * @throws ServiceException
	 *             when newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void deleteNewsTagByNewsID(long newsId) throws ServiceException;

	/**
	 * Check if newsId==0 and then invoke INewsDAO
	 * deleteNewsAuthorByNewsID(newsId) method
	 * 
	 * @param newsId
	 *            News ID for which should be deleted all relations
	 * @throws ServiceException
	 *             when newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void deleteNewsAuthorByNewsID(long newsId) throws ServiceException;

	/**
	 * Method that invokes INewsDAO getPrevNextNewsId method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @param newsId
	 *            ID of current news
	 * @return List with previous and next newsId
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws ServiceException;

	/**
	 * Method that invokes INewsDAO countFilteredNews method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return number of found news
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public int countNews(Filter filter) throws ServiceException;

	/**
	 * Method that invokes INewsDAO getFilteredNews method
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return list of news that will be shown
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<News> getListNews(PagedView pagedView, Filter filter)
			throws ServiceException;

}
