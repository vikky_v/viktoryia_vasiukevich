<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="body">


	<div class="loginform">

		<c:if test="${not empty error}">
			<div class="error">
				<spring:message code="login.error.message" />
			</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">
				<spring:message code="logout.message" />
			</div>
		</c:if>



		<form name='loginForm'
			action="<c:url value='/j_spring_security_check' />" method='POST'>

			<div>
				<div class="field">
					<span class="loginForm"><spring:message code="message.login" /></span>
					<input type='text' name='username'>
				</div>
				<div class="field">
					<span class="loginForm"><spring:message
							code="message.password" /></span> <input type='password'
						name='password' />
				</div>
				<div class="login">
					<input name="submit" type="submit" class="submit"
						value="<spring:message code="button.login"/>" />
				</div>
			</div>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

		</form>

	</div>

</div>
