<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="menu">
	<div class="innermenu">
		&nbsp;<img src="<c:url value="/resources/img/arrow2.png" />"
			height="13" width="13" />&nbsp; <a href="news"
			class="${requestScope['javax.servlet.forward.servlet_path'] eq '/news' ? ' active' : ''}"><spring:message
				code="menu.list" /></a>

		<p>
			&nbsp;<img src="<c:url value="/resources/img/arrow2.png" />"
				height="13" width="13" />&nbsp; <a href="addnews"
				class="${requestScope['javax.servlet.forward.servlet_path'] eq '/addnews' ? ' active' : ''}"><spring:message
					code="menu.addnews" /></a>
		<p>
			&nbsp;<img src="<c:url value="/resources/img/arrow2.png" />"
				height="13" width="13" />&nbsp; <a href="authors"
				class="${requestScope['javax.servlet.forward.servlet_path'] eq '/authors' ? ' active' : ''}"><spring:message
					code="menu.addauthors" /></a>
		<p>
			&nbsp;<img src="<c:url value="/resources/img/arrow2.png" />"
				height="13" width="13" />&nbsp; <a href="tags"
				class="${requestScope['javax.servlet.forward.servlet_path'] eq '/tags' ? ' active' : ''}"><spring:message
					code="menu.addtags" /></a> <br>
	</div>
</div>