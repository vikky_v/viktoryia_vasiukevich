$(document).ready(
		function() {

			var authors = [];
			
			$('.edit').on('click', function(e) {
				
				var target = $(this).attr('id');
				
				authors[target] = $("#author"+target).val();
				
				$("#edit" + target).hide();
				$("#menu" + target).css('display', 'inline-block');
				$("#author" + target).attr("disabled", false);
				
				e.preventDefault();
			});

			$('.cancel').on('click', function(e) {
				
				e.preventDefault();
				
				var target = $(this).attr('id');
				
				$("#author"+target).val(authors[target]);
				$("#author"+target).removeClass("highlight");

				$("#edit" + target).show();
				$("#menu" + target).hide();
				$("#author" + target).attr("disabled", true);
			});

			$('.expire').on(
					'click',
					function(e) {

						var target = $(this).attr('id');
						e.preventDefault(); 
						
						var answer = confirm($('#message').val());

					    if (answer) {
						$('#form' + target).attr('action', 'authors-expire');
						
						$('#form' + target).submit();  
					
					    }
											
					
					});

			$('.update').on('click', function(e) {

				e.preventDefault(); 
				var target = $(this).attr('id');
				
				if ($.trim($("#author"+target).val()).length == 0) {
					$("#author"+target).addClass("highlight");

				} else {
					$("#author"+target).removeClass("highlight");
				
//				var input = $("<input>").attr("type", "hidden").attr(
//						"name", "name").val("update");
//				$('#form' + target).append($(input));
				$('#form' + target).attr('action', 'authors-update');	
				$("#form" + target).submit(); // trigget the submit handler
			
				}
			
			});
			
			$('.save').on('click', function(e) {
				
				e.preventDefault(); // prevent the link's default behaviour

				if ($.trim($("#authorName").val()).length == 0) {
					$("#authorName").addClass("highlight");

				} else {
					$("#authorName").removeClass("highlight");
					$("#addAuthor").submit();
				}
			});

		});