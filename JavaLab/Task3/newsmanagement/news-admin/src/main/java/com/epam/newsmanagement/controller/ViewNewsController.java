package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.validator.CommentValidator;

@Controller
@SessionAttributes("filter")
public class ViewNewsController {

	@Autowired
	private INewsManagementService newsManagementService;
	
	@Autowired
	private CommentValidator commentValidator;

	@InitBinder("comment")
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(commentValidator);
	}
	
	@RequestMapping(value = "/addcomment", method = RequestMethod.POST)
	public String add(Model model, @ModelAttribute("comment") @Validated Comment comment, BindingResult result)
			throws ServiceException {

		if (result.hasErrors()) {
						 
			return "redirect:news?newsId=" + comment.getNewsId();
		}
	 else {
		 newsManagementService.addComment(comment);

		return "redirect:news?newsId=" + comment.getNewsId();
	 }
	}

	@RequestMapping(value = "/deletecomment", method = RequestMethod.POST)
	public String deleteComment(Model model,
			@ModelAttribute("comment") Comment comment) throws ServiceException {

		newsManagementService.deleteComment(comment.getCommentId());

		return "redirect:news?newsId=" + comment.getNewsId();

	}


	@RequestMapping(value = { "/news" }, method = RequestMethod.GET, params = "newsId")
	public String singleNews(
			@RequestParam(value = "newsId", required = true) String newsId,
			Model model, @ModelAttribute("filter") Filter filter)
			throws ServiceException {

		Comment comment = new Comment();
		model.addAttribute("comment", comment);

		News news = newsManagementService.getNews(Long
				.parseLong(newsId));

		model.addAttribute("news", news);
		
		List<Long> number = newsManagementService.getPrevNextNewsId(filter,
				Long
				.parseLong(newsId));
		long previous = number.get(0);
		long next = number.get(1);

		model.addAttribute("previous", previous);
		model.addAttribute("next", next);
		
		return "viewNews";
	}
	
	

}
