package com.epam.newsmanagement.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;

public class NewsValidator implements Validator {

	@Autowired
	INewsManagementService newsManagementService;

	@Override
	public boolean supports(Class<?> clazz) {
		return News.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title",
				"required.title", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "modificationDate",
				"required.modificationDate", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortText",
				"required.shortText", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fullText",
				"required.fullText", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "author.authorId",
				"required.author", "Field name is required.");
		News news = (News) target;
		System.err.println("version of news: "+news.getVersion());
		
		if (news.getNewsId() > 0) {
			long latestVersion = 0;
			try {
				latestVersion = newsManagementService
						.getCurrentVersion(news);
				System.err.println("latest version of news: "+latestVersion);
			} catch (ServiceException e) {
				errors.rejectValue("version", "news.locked");
			}
			if (news.getVersion() != latestVersion) {
				errors.rejectValue("version", "news.locked");
			}

		}
		

	}

}
