package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@Controller
@SessionAttributes("filter")
public class ListNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@Autowired
	private PagedView pagedView;
	
	
	@RequestMapping(value = "/news", method = RequestMethod.POST, params = "submit")
	public String news(
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException{
		
		List<Tag> filterTags = getListOfTags(tagsId);
				
		filter.setTags(filterTags);
		
		System.err.println(filter);
		 
		return "redirect:news?page=0";
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String newsPage(@RequestParam(value = "page", required = false, defaultValue = "0") String page,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException {

		int numberOfNews = newsManagementService.countNews(filter).intValue();
		
		pagedView.setRowCount(numberOfNews);
		pagedView.setCurrentPage(Integer.parseInt(page));

		List<News> news = newsManagementService.getListNews(pagedView, filter);
		
		model.addAttribute("view", pagedView);
		model.addAttribute("news", news);
		
		setFilterContent(model);
		
		return "listNews";

	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(
			@RequestParam(value = "newsId", required = false) List<Long> newsId,
			@RequestParam(value = "page", required = true) String page) throws ServiceException {

		
		for (Long id : newsId) {
			newsManagementService.deleteNews(id);
		}

		return "redirect:news?page=" + page;
	}
	
	
	@RequestMapping(value = "/news", method = RequestMethod.POST, params = "reset")
	public String reset(
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException{

		filter.setAuthor(new Author());
		filter.setTags(Collections.<Tag>emptyList());
		 
		return "redirect:news?page=0";
	}
	
	
	private List<Tag> getListOfTags(List<Long> tagsId) throws ServiceException{
		
		List<Tag> filterTags = new ArrayList<Tag>();
		if (tagsId != null) {

			for (Long tagId : tagsId) {
				filterTags.add(newsManagementService.getTagById(tagId));
				
			}

		}
		
		return filterTags;
		
	}
	
	private void setFilterContent(Model model) throws ServiceException{
		
		List<Author> authors = newsManagementService.getAllAuthors();
		model.addAttribute("authors", authors);

		List<Tag> tags = newsManagementService.getAllTags();
		model.addAttribute("tags", tags);
	}

}
