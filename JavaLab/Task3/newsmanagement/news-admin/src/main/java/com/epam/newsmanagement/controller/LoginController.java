package com.epam.newsmanagement.controller;

import java.util.Collections;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.IUserService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;

@Controller
@SessionAttributes({"filter","userName"})

public class LoginController {
	
	@Autowired
	private IUserService userService;

	@RequestMapping(value = "/")
	public String toLoginPage(Model model) {

		return "redirect:login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			Model model) {
				
		if (error != null) {
			model.addAttribute("error", "true");
		}

		if (logout != null) {
			model.addAttribute("msg", "true");
		}

		return "login";

	}

	@RequestMapping(value = "/welcome")
	public String index(Model model, HttpSession session) throws ServiceException {

		Filter filter = new Filter();
		filter.setAuthor(new Author());
		filter.setTags(Collections.<Tag>emptyList());
		session.setAttribute("filter", filter);
		
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String login = user.getUsername();
	    	    
	    String userName = userService.getNameByLogin(login);
		
	    session.setAttribute("userName", userName);
	  
	    model.asMap().clear();
	    
		return "redirect:news?page=0";
	}


}
