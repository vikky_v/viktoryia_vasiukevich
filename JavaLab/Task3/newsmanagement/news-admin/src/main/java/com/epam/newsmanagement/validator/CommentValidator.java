package com.epam.newsmanagement.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.dto.Comment;

public class CommentValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		
		return Comment.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "commentText",
				"required.commentText", "Field name is required.");

	}

}
