package com.epam.newsmanagement.dao.impl.jpa;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Author;

@Transactional
public class AuthorDAOJPAImpl implements IAuthorDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public long create(Author author) throws DAOException {

		try {
			entityManager.persist(author);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

		return author.getAuthorId();
	}

	public void update(Author author) throws DAOException {

		try {
			entityManager.merge(author);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

	}

	public void delete(long id) throws DAOException {

		Author author = getById(id);
		author.setExpired(new Date());
		try {
			update(author);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

	}

	public Author getById(long id) throws DAOException {

		Author author = null;
		try {
			author = entityManager.find(Author.class, id);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return author;

	}

	@SuppressWarnings("unchecked")
	public List<Author> getAll() throws DAOException {

		List<Author> listAuthor = Collections.<Author> emptyList();

		try {
			listAuthor = (List<Author>) entityManager
					.createQuery(
							"select a from Author a where a.expired is null order by a.authorId")
					.getResultList();
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return listAuthor;
	}

	public long getAuthorId(Author author) throws DAOException {

		Query query = null;

		try {
			query = entityManager
					.createQuery("select a.authorId from Author a where a.authorName = ?1 ");
			query.setParameter(1, author.getAuthorName());
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		if (query.getResultList().size() != 0) {
			return (Long) query.getSingleResult();
		} else {
			return 0L;
		}

	}

}
