package com.epam.newsmanagement.dao.impl.jpa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.util.QueryBuilder;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@Transactional
public class NewsDAOJPAImpl implements INewsDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public long create(News news) throws DAOException {

		try {
			entityManager.persist(news);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return news.getNewsId();
	}

	public void update(News news) throws DAOException {

		try {
			entityManager.merge(news);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

	}

	public void delete(long id) throws DAOException {

		try {
			entityManager.remove(getById(id));
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

	}

	public News getById(long id) throws DAOException {

		News news = null;
		try {
			news = entityManager.find(News.class, id);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return news;

	}

	@SuppressWarnings("unchecked")
	public List<News> getAll() throws DAOException {

		List<News> listNews = Collections.<News> emptyList();
		try {
			listNews = (List<News>) entityManager.createQuery(
					"select n from News n").getResultList();
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return listNews;
	}

	@SuppressWarnings("unchecked")
	public List<News> getNewsByTag(long tagId) throws DAOException {

		List<News> listNews = Collections.<News> emptyList();
		try {
			Query query = entityManager
					.createQuery("select n from News n join n.tags t  WHERE t.tagId = ?1");
			query.setParameter(1, tagId);
			listNews = (List<News>) query.getResultList();
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return listNews;
	}

	// public List<News> getNews(PagedView pagedView, Filter filter)
	// throws DAOException {
	//
	// List<News> list = Collections.<News> emptyList();
	// int currentPage = pagedView.getCurrentPage();
	// int itemPerPage = pagedView.getItemPerPage();
	//
	// try {
	// CriteriaBuilder crBuilder = entityManager.getCriteriaBuilder();
	//
	// CriteriaQuery<News> crQuery = crBuilder.createQuery(News.class);
	//
	// Root<News> news = crQuery.from(News.class);
	// Join<News, Tag> tags = news.join("tags", JoinType.LEFT);
	//
	// crQuery.select(news).distinct(true);
	//
	// crQuery.orderBy(crBuilder.desc(crBuilder.size(news
	// .<Collection<Comment>> get("comments"))), crBuilder
	// .desc(news.get("modificationDate")));
	//
	// List<Predicate> predicates = new ArrayList<Predicate>();
	//
	// if (filter.getAuthor() != null
	// && filter.getAuthor().getAuthorId() != 0) {
	// long authorId = filter.getAuthor().getAuthorId();
	//
	// predicates.add(crBuilder.equal(
	// news.get("author").get("authorId"), authorId));
	//
	// }
	//
	// if (filter.getTags() != null && filter.getTags().size() != 0) {
	// List<Long> tagsId = new LinkedList<Long>();
	// for (int i = 0; i < filter.getTags().size(); i++) {
	//
	// tagsId.add(filter.getTags().get(i).getTagId());
	// }
	//
	// Expression<String> exp = tags.get("tagId");
	// Predicate predicate = exp.in(tagsId);
	// predicates.add(predicate);
	//
	// }
	//
	// crQuery.where(predicates.toArray(new Predicate[] {}));
	//
	// list = entityManager.createQuery(crQuery)
	// .setFirstResult(currentPage * itemPerPage)
	// .setMaxResults(itemPerPage).getResultList();
	// } catch (RuntimeException exc) {
	// throw new DAOException(exc);
	// }
	// return list;
	// }

	@SuppressWarnings("unchecked")
	public List<News> getNews(PagedView pagedView, Filter filter)
			throws DAOException {

		List<News> list = Collections.<News> emptyList();
		int currentPage = pagedView.getCurrentPage();
		int itemPerPage = pagedView.getItemPerPage();

		Query query = entityManager.createQuery(QueryBuilder.getNewsQL(filter));

		query.setFirstResult(currentPage * itemPerPage);
		query.setMaxResults(itemPerPage);

		if (filter.getTags() != null && filter.getTags().size() != 0) {
			List<Long> tagsId = new LinkedList<Long>();
			for (int i = 0; i < filter.getTags().size(); i++) {

				tagsId.add(filter.getTags().get(i).getTagId());
			}

			query.setParameter("tagsId", tagsId);

		}

		if (filter.getAuthor() != null && filter.getAuthor().getAuthorId() != 0) {
			query.setParameter("authorId", filter.getAuthor().getAuthorId());
		}

		list = query.getResultList();
		return list;
	}

	public Number countNews(Filter filter) throws DAOException {

		Number numberOfNews = 0;

		try {
			CriteriaBuilder crBuilder = entityManager.getCriteriaBuilder();

			CriteriaQuery<Long> crQuery = crBuilder.createQuery(Long.class);
			Root<News> news = crQuery.from(News.class);
			Join<News, Tag> tags = news.join("tags", JoinType.LEFT);

			crQuery.select(crBuilder.countDistinct(news));
			List<Predicate> predicates = new ArrayList<Predicate>();

			if (filter.getAuthor() != null
					&& filter.getAuthor().getAuthorId() != 0) {
				long authorId = filter.getAuthor().getAuthorId();

				predicates.add(crBuilder.equal(
						news.get("author").get("authorId"), authorId));

			}

			if (filter.getTags() != null && filter.getTags().size() != 0) {
				List<Long> tagsId = new LinkedList<Long>();
				for (int i = 0; i < filter.getTags().size(); i++) {

					tagsId.add(filter.getTags().get(i).getTagId());
				}

				Expression<String> exp = tags.get("tagId");
				Predicate predicate = exp.in(tagsId);
				predicates.add(predicate);

			}

			crQuery.where(predicates.toArray(new Predicate[] {}));
			numberOfNews = entityManager.createQuery(crQuery).getSingleResult();
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

		return numberOfNews;
	}

	// public List<Long> getPrevNextNewsId(Filter filter, long newsId)
	// throws DAOException {
	//
	// Set<Long> setOfNewsId = new LinkedHashSet<Long>();
	//
	// try {
	// CriteriaBuilder crBuilder = entityManager.getCriteriaBuilder();
	//
	// CriteriaQuery<Long> crQuery = crBuilder.createQuery(Long.class);
	//
	// Root<News> news = crQuery.from(News.class);
	// Join<News, Tag> tags = news.join("tags", JoinType.LEFT);
	//
	// crQuery.select(news.<Long> get("newsId"));
	// crQuery.orderBy(crBuilder.desc(crBuilder.size(news
	// .<Collection<Comment>> get("comments"))), crBuilder.desc(news
	// .get("modificationDate")));
	// List<Predicate> predicates = new ArrayList<Predicate>();
	//
	// if (filter.getAuthor() != null && filter.getAuthor().getAuthorId() != 0)
	// {
	// long authorId = filter.getAuthor().getAuthorId();
	//
	// predicates.add(crBuilder.equal(news.get("author").get("authorId"),
	// authorId));
	//
	// }
	//
	// if (filter.getTags() != null && filter.getTags().size() != 0) {
	// List<Long> tagsId = new LinkedList<Long>();
	// for (int i = 0; i < filter.getTags().size(); i++) {
	//
	// tagsId.add(filter.getTags().get(i).getTagId());
	// }
	//
	// Expression<String> exp = tags.get("tagId");
	// Predicate predicate = exp.in(tagsId);
	// predicates.add(predicate);
	//
	// }
	//
	// crQuery.where(predicates.toArray(new Predicate[] {}));
	//
	//
	// setOfNewsId.addAll(entityManager.createQuery(crQuery).getResultList());
	// } catch (RuntimeException exc) {
	// throw new DAOException(exc);
	// }
	//
	// List<Long> listId2 = new ArrayList<Long>(setOfNewsId);
	//
	// int index = listId2.indexOf(newsId);
	//
	// List<Long> listPrevNext = new ArrayList<Long>();
	// if (index == 0) {
	// listPrevNext.add(0L);
	// if (listId2.size() == 1) {
	// listPrevNext.add(0L);
	// } else {
	// listPrevNext.add(listId2.get(index + 1));
	// }
	// } else if (index == listId2.size() - 1) {
	// listPrevNext.add(listId2.get(index - 1));
	// listPrevNext.add(0L);
	// } else {
	// listPrevNext.add(listId2.get(index - 1));
	// listPrevNext.add(listId2.get(index + 1));
	// }
	//
	// return listPrevNext;
	// }

	@SuppressWarnings("unchecked")
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws DAOException {

		List<Long> listId = new ArrayList<Long>(2);

		Query query = QueryBuilder.getQueryPrevNextNews(entityManager, filter,
				newsId);
		List<Object[]> resultList = (List<Object[]>) query.getResultList();

		if (resultList.size() == 0) {
			listId.add(0L);
			listId.add(0L);
		} else if (resultList.size() == 1) {
			Object[] number = resultList.get(0);
			long id = ((Number) number[0]).longValue();
			long rownum = ((Number) number[1]).longValue();
			long curRownum = ((Number) number[3]).longValue();

			if (curRownum < rownum) {
				listId.add(0L);
				listId.add(id);
			} else {
				listId.add(id);
				listId.add(0L);
			}
		} else if (resultList.size() == 2) {
			Object[] number1 = resultList.get(0);
			long id1 = ((Number) number1[0]).longValue();
			long rownum1 = ((Number) number1[1]).longValue();

			Object[] number2 = resultList.get(1);
			long id2 = ((Number) number2[0]).longValue();
			long rownum2 = ((Number) number2[1]).longValue();

			if (rownum1 < rownum2) {
				listId.add(id1);
				listId.add(id2);
			} else {
				listId.add(id2);
				listId.add(id1);
			}
		}

		return listId;

	}

	public long getCurrentVersion(News news) throws DAOException {

		long version = 0;
		try {
			Query query = entityManager
					.createQuery("SELECT n.version FROM News n WHERE n.newsId = ?1");
			query.setParameter(1, news.getNewsId());
			version = (Long) query.getSingleResult();
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return version;
	}

}
