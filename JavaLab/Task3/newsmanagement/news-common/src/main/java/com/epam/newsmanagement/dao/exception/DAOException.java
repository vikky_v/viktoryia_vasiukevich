package com.epam.newsmanagement.dao.exception;

/**
 * Thrown when some error occurred in {@link AuthorDAOImpl.class}, {@link
 * CommentDAOImpl.class}, {@link NewsDAOImpl.class}, {@link TagDAOImpl.class},
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public class DAOException extends Exception {

	
	private static final long serialVersionUID = 1L;

	public DAOException() {

	}

	public DAOException(String arg0) {
		super(arg0);

	}

	public DAOException(Throwable arg0) {
		super(arg0);

	}

	public DAOException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

}
