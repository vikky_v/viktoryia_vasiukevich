package com.epam.newsmanagement.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

/**
 * Entity associated with table 'TAG'
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
@Entity
@Table(name="TAG")
public class Tag implements Serializable{

	
	private static final long serialVersionUID = 951724986404804074L;
	
	@Id
	@Column(name="TAG_ID")
	@GeneratedValue(generator="seq")
	@SequenceGenerator(name="seq",sequenceName="SEQUENCE_TAG", allocationSize=1) 
	private long tagId;
	
	@Column(name="TAG_NAME")
	private String tagName;

	public Tag() {

	}

	public Tag(long tag_id, String tag_name) {
		super();
		this.tagId = tag_id;
		this.tagName = tag_name;
	}

	public long getTagId() {
		return tagId;
	}

	public void setTagId(long tag_id) {
		this.tagId = tag_id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tag_name) {
		this.tagName = tag_name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nTag [tagId=");
		builder.append(tagId);
		builder.append(", tagName=");
		builder.append(tagName);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + tagId);
		result = prime * result
				+ ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Tag)) {
			return false;
		}
		Tag other = (Tag) obj;
		if (tagId != other.tagId) {
			return false;
		}
		if (tagName == null) {
			if (other.tagName != null) {
				return false;
			}
		} else if (!tagName.equals(other.tagName)) {
			return false;
		}
		return true;
	}

}
