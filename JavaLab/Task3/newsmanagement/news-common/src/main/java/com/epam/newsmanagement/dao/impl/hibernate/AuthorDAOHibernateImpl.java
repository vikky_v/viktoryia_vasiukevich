package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Author;

@Transactional
public class AuthorDAOHibernateImpl implements IAuthorDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public long create(Author author) throws DAOException {

		long authorsId = 0L;
		try {
			authorsId = (Long) sessionFactory.getCurrentSession().save(author);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

		return authorsId;
	}

	public void update(Author author) throws DAOException {

		try {
			sessionFactory.getCurrentSession().update(author);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

	}

	public void delete(long id) throws DAOException {

		try {
			Author author = getById(id);
			author.setExpired(new Date());
			sessionFactory.getCurrentSession().update(author);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

	}

	public Author getById(long id) throws DAOException {

		Author author = null;
		try {
			author = (Author) sessionFactory.getCurrentSession().get(
					Author.class, id);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

		return author;
	}

	@SuppressWarnings("unchecked")
	public List<Author> getAll() throws DAOException {
		List<Author> listAuthor = Collections.<Author>emptyList();
		try {
			listAuthor = (List<Author>) sessionFactory
					.getCurrentSession()
					.createQuery(
							"from Author a where a.expired is null order by a.authorId")
					.list();
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return listAuthor;

	}

	public long getAuthorId(Author author) throws DAOException {

		Query query;
		try {
			query = sessionFactory.getCurrentSession().createQuery(
					"select authorId from Author where authorName = :name ");
			query.setString("name", author.getAuthorName());
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		if (query.list().size() != 0) {
			return (Long) query.uniqueResult();
		} else {
			return 0L;
		}

	}

}
