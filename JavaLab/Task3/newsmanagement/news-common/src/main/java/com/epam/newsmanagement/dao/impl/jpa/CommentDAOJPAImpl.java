package com.epam.newsmanagement.dao.impl.jpa;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Comment;

@Transactional
public class CommentDAOJPAImpl implements ICommentDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public long create(Comment comment) throws DAOException {

		comment.setCreationDate(new Timestamp(new Date().getTime()));
		try {
			entityManager.persist(comment);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

		return comment.getCommentId();
	}

	public void delete(long id) throws DAOException {

		try {
			entityManager.remove(entityManager.find(Comment.class, id));
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

	}

}
