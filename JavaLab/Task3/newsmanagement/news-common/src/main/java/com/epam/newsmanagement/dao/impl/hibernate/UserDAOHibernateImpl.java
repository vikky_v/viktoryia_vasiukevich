package com.epam.newsmanagement.dao.impl.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.dao.exception.DAOException;

@Transactional
public class UserDAOHibernateImpl implements IUserDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public String getNameByLogin(String login) throws DAOException {
		
		String name = null;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT TO_CHAR(u.user_name)  FROM user_table u WHERE u.login = :login");
			query.setString("login", login);
			name = (String) query.uniqueResult();
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		
		return name;
	}

}
