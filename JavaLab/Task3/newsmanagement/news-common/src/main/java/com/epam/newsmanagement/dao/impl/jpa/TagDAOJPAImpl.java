package com.epam.newsmanagement.dao.impl.jpa;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Tag;

@Transactional
public class TagDAOJPAImpl implements ITagDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public long create(Tag tag) throws DAOException {

		try {
			entityManager.persist(tag);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

		return tag.getTagId();
	}

	public void update(Tag tag) throws DAOException {

		try {
			entityManager.merge(tag);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
	}

	public void delete(long id) throws DAOException {

		try {
			Query query = entityManager
					.createNativeQuery("delete from news_tag where news_tag.tag_id=?1");
			query.setParameter(1, id);
			query.executeUpdate();
			Tag tag = getById(id);
			entityManager.remove(tag);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}

	}

	public Tag getById(long id) throws DAOException {
		
		Tag tag = null;
		try {
			tag = entityManager.find(Tag.class, id);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return tag;
	}

	@SuppressWarnings("unchecked")
	public List<Tag> getAll() throws DAOException {
		
		List<Tag> listTag = Collections.<Tag>emptyList();
		try {
			listTag = (List<Tag>) entityManager.createQuery(
					"select t from Tag t order by t.tagId").getResultList();
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		return listTag;
	}

	public Tag getByName(String name) throws DAOException {
		
		Query query = null;
		try {
			query = entityManager
					.createQuery("select t from Tag t where t.tagName = ?1");
			query.setParameter(1, name);
		} catch (RuntimeException exc) {
			throw new DAOException(exc);
		}
		if (query.getResultList().size() != 0) {
			return (Tag) query.getSingleResult();
		} else {
			return null;
		}
	}

}
