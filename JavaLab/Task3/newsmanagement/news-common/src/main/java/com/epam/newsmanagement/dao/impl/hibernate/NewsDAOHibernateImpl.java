package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dao.impl.util.QueryBuilder;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@Transactional
public class NewsDAOHibernateImpl implements INewsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public long create(News news) throws DAOException {

		long newsId = 0L;
		try {
			newsId = (Long) sessionFactory.getCurrentSession().save(news);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return newsId;
	}

	public void update(News news) throws DAOException {

		try {
			sessionFactory.getCurrentSession().update(news);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

	}

	public void delete(long id) throws DAOException {

		try {
			News news = getById(id);
			sessionFactory.getCurrentSession().delete(news);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

	}

	public News getById(long id) throws DAOException {

		News news = null;
		try {
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(News.class, "news");
			criteria.add(Restrictions.eq("news.newsId", id));

			Criteria subCriteria = criteria.createCriteria("news.comments",
					JoinType.LEFT_OUTER_JOIN);
			subCriteria.addOrder(Order.asc("commentId"));
			news = (News) criteria.uniqueResult();
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return news;
	}

	@SuppressWarnings("unchecked")
	public List<News> getAll() throws DAOException {
		List<News> listNews = Collections.<News> emptyList();
		try {
			listNews = (List<News>) sessionFactory.getCurrentSession()
					.createQuery("from News").list();
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return listNews;
	}

	@SuppressWarnings("unchecked")
	public List<News> getNewsByTag(long tagId) throws DAOException {

		List<News> list = Collections.<News> emptyList();

		try {
			Query query = sessionFactory
					.getCurrentSession()
					.createQuery(
							"select n from News n join n.tags t  WHERE t.tagId = :tagId");
			query.setLong("tagId", tagId);

			list = (List<News>) query.list();
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

		return list;
	}

	// @SuppressWarnings("unchecked")
	// public List<News> getNews(PagedView pagedView, Filter filter)
	// throws DAOException {
	//
	// int currentPage = pagedView.getCurrentPage();
	// int itemPerPage = pagedView.getItemPerPage();
	// List<News> list = Collections.<News> emptyList();
	//
	// try {
	// Criteria criteria = sessionFactory.getCurrentSession()
	// .createCriteria(News.class, "news");
	//
	// criteria.setFirstResult(currentPage * itemPerPage);
	// criteria.setMaxResults(itemPerPage);
	//
	// criteria.createAlias("news.author", "author",
	// JoinType.LEFT_OUTER_JOIN);
	// criteria.createAlias("news.tags", "tag", JoinType.LEFT_OUTER_JOIN);
	//
	// criteria.setProjection(Projections.projectionList().add(
	// Projections.property("news.newsId"), "newsId"));
	//
	// // criteria.addOrder(Order.desc("numberOfComments"));
	// // criteria.addOrder(Order.desc("modificationDate"));
	//
	// if (filter.getAuthor() != null
	// && filter.getAuthor().getAuthorId() != 0) {
	// long authorId = filter.getAuthor().getAuthorId();
	//
	// criteria.add(Restrictions.eq("author.authorId", authorId));
	// }
	//
	// if (filter.getTags() != null && filter.getTags().size() != 0) {
	// Long[] tagsId = new Long[filter.getTags().size()];
	// for (int i = 0; i < filter.getTags().size(); i++) {
	//
	// tagsId[i] = filter.getTags().get(i).getTagId();
	// }
	//
	// criteria.add(Restrictions.in("tag.tagId", tagsId));
	// }
	//
	// criteria.add(Restrictions
	// .sqlRestriction("1=1 group by this_.NEWS_ID, this_.MODIFICATION_DATE order by (select COUNT(c.COMMENT_ID) from COMMENTS c  where c.NEWS_ID=this_.NEWS_ID) desc, this_.modification_date desc"));
	//
	// criteria.setResultTransformer(new AliasToBeanResultTransformer(
	// News.class));
	//
	// list = (List<News>) criteria.list();
	// } catch (HibernateException exc) {
	// throw new DAOException(exc);
	// }
	//
	// System.err.println(list);
	//
	// List<News> listReturn = new LinkedList<News>();
	// for (News news : list) {
	// try {
	// listReturn.add((News) sessionFactory.getCurrentSession().get(
	// News.class, news.getNewsId()));
	// } catch (HibernateException exc) {
	// throw new DAOException(exc);
	// }
	// }
	//
	// // System.err.println(listReturn);
	// return listReturn;
	//
	// }

	@SuppressWarnings("unchecked")
	public List<News> getNews(PagedView pagedView, Filter filter)
			throws DAOException {

		int currentPage = pagedView.getCurrentPage();
		int itemPerPage = pagedView.getItemPerPage();

		Query query = sessionFactory.getCurrentSession().createQuery(
				QueryBuilder.getNewsQL(filter));
		query.setFirstResult(currentPage * itemPerPage);
		query.setMaxResults(itemPerPage);

		if (filter.getTags() != null && filter.getTags().size() != 0) {
			Long[] tagsId = new Long[filter.getTags().size()];
			for (int i = 0; i < filter.getTags().size(); i++) {

				tagsId[i] = filter.getTags().get(i).getTagId();
			}

			query.setParameterList("tagsId", tagsId);

		}

		if (filter.getAuthor() != null && filter.getAuthor().getAuthorId() != 0) {
			query.setLong("authorId", filter.getAuthor().getAuthorId());
		}

		List<News> listReturn = query.list();

		return listReturn;

	}

	@SuppressWarnings("unchecked")
	public Number countNews(Filter filter) throws DAOException {
		Number count = 0;
		try {
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(News.class, "news");
			criteria.setProjection(Projections.countDistinct("news.newsId"));

			if (filter.getAuthor() != null
					&& filter.getAuthor().getAuthorId() != 0) {
				criteria.createAlias("news.author", "author",
						JoinType.LEFT_OUTER_JOIN);
				criteria.add(Restrictions.eq("author.authorId", filter
						.getAuthor().getAuthorId()));
			}
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				Long[] tagsId = new Long[filter.getTags().size()];
				for (int i = 0; i < filter.getTags().size(); i++) {

					tagsId[i] = filter.getTags().get(i).getTagId();
				}

				criteria.createAlias("news.tags", "tag",
						JoinType.LEFT_OUTER_JOIN);
				criteria.add(Restrictions.in("tag.tagId", tagsId));
			}
			List<Number> list = (List<Number>) criteria.list();

			if (list.size() == 1) {
				count = (Number) list.get(0);
			}
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

		// System.out.println("count " + list.get(0));
		return count;
	}

	// @SuppressWarnings("unchecked")
	// public List<Long> getPrevNextNewsId(Filter filter, long newsId)
	// throws DAOException {
	//
	// Set<Long> listId = new LinkedHashSet<Long>();
	//
	// try {
	// Criteria criteria = sessionFactory.getCurrentSession()
	// .createCriteria(News.class, "news");
	//
	// // criteria.createAlias("news.comments", "comment",
	// // JoinType.LEFT_OUTER_JOIN);
	//
	// criteria.setProjection(Projections.projectionList().add(
	// Projections.property("news.newsId"), "newsId"));
	//
	// // criteria.addOrder(Order.desc("numberOfComments"));
	// // criteria.addOrder(Order.desc("modificationDate"));
	//
	// if (filter.getAuthor() != null
	// && filter.getAuthor().getAuthorId() != 0) {
	// long authorId = filter.getAuthor().getAuthorId();
	//
	// criteria.createAlias("news.author", "author",
	// JoinType.LEFT_OUTER_JOIN);
	// criteria.add(Restrictions.eq("author.authorId", authorId));
	// }
	//
	// if (filter.getTags() != null && filter.getTags().size() != 0) {
	// Long[] tagsId = new Long[filter.getTags().size()];
	// for (int i = 0; i < filter.getTags().size(); i++) {
	//
	// tagsId[i] = filter.getTags().get(i).getTagId();
	// }
	//
	// criteria.createAlias("news.tags", "tag",
	// JoinType.LEFT_OUTER_JOIN);
	// criteria.add(Restrictions.in("tag.tagId", tagsId));
	// }
	//
	// criteria.add(Restrictions
	// .sqlRestriction("1=1 group by this_.NEWS_ID, this_.MODIFICATION_DATE "
	// + "order by (select COUNT(c.COMMENT_ID) from COMMENTS c  where "
	// + "c.NEWS_ID=this_.NEWS_ID) desc, this_.modification_date desc"));
	//
	// // System.err.println(criteria.list());
	//
	// listId.addAll(criteria.list());
	// } catch (HibernateException exc) {
	// throw new DAOException(exc);
	// }
	//
	// List<Long> listId2 = new LinkedList<Long>(listId);
	//
	// int index = listId2.indexOf(newsId);
	//
	// List<Long> listPrevNext = new ArrayList<Long>();
	// if (index == 0) {
	// listPrevNext.add(0L); // add previous index
	// if (listId2.size() == 1) {
	// listPrevNext.add(0L); // add next index
	// } else {
	// listPrevNext.add(listId2.get(index + 1)); // add next index
	// }
	// } else if (index == listId2.size() - 1) {
	// listPrevNext.add(listId2.get(index - 1)); // add previous index
	// listPrevNext.add(0L); // add next index
	// } else {
	// listPrevNext.add(listId2.get(index - 1)); // add previous index
	// listPrevNext.add(listId2.get(index + 1)); // add next index
	// }
	//
	// return listPrevNext;
	// }

	@SuppressWarnings("unchecked")
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws DAOException {

		List<Long> listId = new ArrayList<Long>(2);

		Query query = QueryBuilder.getQueryPrevNextNews(sessionFactory, filter,
				newsId);
		List<Object[]> resultList = (List<Object[]>) query.list();

		if (resultList.size() == 0) {
			listId.add(0L);
			listId.add(0L);
		} else if (resultList.size() == 1) {
			Object[] number = resultList.get(0);
			long id = ((Number) number[0]).longValue();
			long rownum = ((Number) number[1]).longValue();
			long curRownum = ((Number) number[3]).longValue();

			if (curRownum < rownum) {
				listId.add(0L);
				listId.add(id);
			} else {
				listId.add(id);
				listId.add(0L);
			}
		} else if (resultList.size() == 2) {
			Object[] number1 = resultList.get(0);
			long id1 = ((Number) number1[0]).longValue();
			long rownum1 = ((Number) number1[1]).longValue();

			Object[] number2 = resultList.get(1);
			long id2 = ((Number) number2[0]).longValue();
			long rownum2 = ((Number) number2[1]).longValue();

			if (rownum1 < rownum2) {
				listId.add(id1);
				listId.add(id2);
			} else {
				listId.add(id2);
				listId.add(id1);
			}
		}

		return listId;

	}

	public long getCurrentVersion(News news) throws DAOException {
		long version = 0L;

		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT version from News n WHERE n.newsId = :newsId");
			query.setLong("newsId", news.getNewsId());

			if (query.list().size() == 1) {

				version = (Long) query.list().get(0);

			}
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return version;
	}

}
