package com.epam.newsmanagement.dao.impl.util;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;

public class QueryBuilder {

	public final static String SELECT_PREV_NEXT_START = "WITH newsm AS(SELECT news.news_id, news.modification_date FROM news LEFT JOIN (SELECT news_id, count(*) AS c FROM comments GROUP BY news_id ORDER BY c DESC) s ON news.news_id=s.news_id ORDER BY COALESCE(s.c,0) DESC, news.modification_date DESC) SELECT * FROM (SELECT newsm.news_id n, ROWNUM rnum1 FROM newsm ";
	public final static String SELECT_PREV_NEXT_MIDDLE = ")q1 join (SELECT newsm.news_id, ROWNUM rnum2 FROM newsm ";
	public final static String SELECT_PREV_NEXT_END = ") q2 on q2.news_id=? where q1.rnum1 in (q2.rnum2-1, q2.rnum2+1) order by q1.rnum1";
	public final static String APPEND_AUTHOR = " JOIN news_author na ON newsm.news_id=na.news_id WHERE na.author_id=?";
	public final static String APPEND_TAG_START = "RIGHT JOIN (SELECT news_id, listagg(tag_id, '; ') WITHIN GROUP (ORDER BY news_tag_id) FROM news_tag WHERE tag_id IN (";
	public final static String APPEND_TAG_END = ") GROUP BY news_id)k ON newsm.news_id=k.news_id";

	public static Query getQueryPrevNextNews(SessionFactory sessionFactory,
			Filter filter, long newsId) {

		Author author = filter.getAuthor();
		List<Tag> tags = filter.getTags();

		boolean appendAuthor = false;
		boolean appendTags = false;

		if (author != null) {
			if (author.getAuthorId() != 0) {
				appendAuthor = true;
			}
		}
		if (tags != null) {
			if (!tags.isEmpty()) {
				appendTags = true;
			}
		}
		//
		// String sql = SELECT_PREV_NEXT_START;
		// String sign = "?";
		// if (appendTags) {
		//
		// // depending on number of tags we need different number of
		// // parameters
		// for (int i = 1; i < tags.size(); i++) {
		// sign = sign + ",?";
		// }
		// sql = sql + APPEND_TAG_START + sign + APPEND_TAG_END;
		// }
		//
		// if (appendAuthor) {
		// sql = sql + " " + APPEND_AUTHOR;
		// }
		//
		// sql = sql + " " + SELECT_PREV_NEXT_MIDDLE;
		//
		// if (appendTags) {
		// sql = sql + APPEND_TAG_START + sign + APPEND_TAG_END;
		// }
		//
		// if (appendAuthor) {
		// sql = sql + " " + APPEND_AUTHOR;
		// }
		//
		// sql = sql + " " + SELECT_PREV_NEXT_END;

		String sql = getSQLString(filter, appendAuthor, appendTags);

		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);

		// define number of positions where to set ID of tags, author
		// number of current page and item per page
		int paramIndex = 0;
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				query.setLong(paramIndex++, tags.get(i - 1).getTagId());
			}
		}
		if (appendAuthor) {
			query.setLong(paramIndex++, author.getAuthorId());
		}
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				query.setLong(paramIndex++, tags.get(i - 1).getTagId());
			}
		}
		if (appendAuthor) {
			query.setLong(paramIndex++, author.getAuthorId());
		}
		query.setLong(paramIndex++, newsId);

		return query;
	}

	public static javax.persistence.Query getQueryPrevNextNews(
			EntityManager entityManager, Filter filter, long newsId) {

		Author author = filter.getAuthor();
		List<Tag> tags = filter.getTags();

		boolean appendAuthor = false;
		boolean appendTags = false;

		if (author != null) {
			if (author.getAuthorId() != 0) {
				appendAuthor = true;
			}
		}
		if (tags != null) {
			if (!tags.isEmpty()) {
				appendTags = true;
			}
		}

		//

		String sql = getSQLString(filter, appendAuthor, appendTags);

		javax.persistence.Query query = entityManager.createNativeQuery(sql);

		// define number of positions where to set ID of tags, author
		// number of current page and item per page
		int paramIndex = 1;
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				query.setParameter(paramIndex++, tags.get(i - 1).getTagId());
			}
		}
		if (appendAuthor) {
			query.setParameter(paramIndex++, author.getAuthorId());
		}
		if (appendTags) {
			for (int i = 1; i <= tags.size(); i++) {
				query.setParameter(paramIndex++, tags.get(i - 1).getTagId());
			}
		}
		if (appendAuthor) {
			query.setParameter(paramIndex++, author.getAuthorId());
		}
		query.setParameter(paramIndex++, newsId);

		return query;
	}

	private static String getSQLString(Filter filter, boolean appendAuthor,
			boolean appendTags) {

		String sql = SELECT_PREV_NEXT_START;
		String sign = "?";
		if (appendTags) {

			// depending on number of tags we need different number of
			// parameters
			for (int i = 1; i < filter.getTags().size(); i++) {
				sign = sign + ",?";
			}
			sql = sql + APPEND_TAG_START + sign + APPEND_TAG_END;
		}

		if (appendAuthor) {
			sql = sql + " " + APPEND_AUTHOR;
		}

		sql = sql + " " + SELECT_PREV_NEXT_MIDDLE;

		if (appendTags) {
			sql = sql + APPEND_TAG_START + sign + APPEND_TAG_END;
		}

		if (appendAuthor) {
			sql = sql + " " + APPEND_AUTHOR;
		}

		sql = sql + " " + SELECT_PREV_NEXT_END;
		return sql;
	}

	public static String getNewsQL(Filter filter) {

		String ql = "select distinct n  from News n LEFT JOIN n.tags t JOIN n.author a WHERE";

		if (filter.getTags() != null && filter.getTags().size() != 0) {
			ql += " (t.tagId IN :tagsId ) ";
		} else {
			ql += " 1=1 ";
		}

		ql += " AND ";

		if (filter.getAuthor() != null && filter.getAuthor().getAuthorId() != 0) {
			ql += "(n.author.authorId = :authorId) ";
		} else {
			ql += " 1=1 ";
		}

		ql += " order by size(n.comments) DESC , n.modificationDate DESC";

		return ql;
	}

}
