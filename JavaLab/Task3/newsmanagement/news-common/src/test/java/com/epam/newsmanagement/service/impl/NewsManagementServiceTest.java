package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {

	@Mock
	private IAuthorService authorService;

	@Mock
	private ICommentService commentService;

	@Mock
	private INewsService newsService;

	@Mock
	private ITagService tagService;

	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;


	@Test
	public void showFileredNewsForPageTest() throws Exception {

		List<News> listOfNews = new ArrayList<News>();
		News news1 = new News();
		news1.setNewsId(1L);
		listOfNews.add(news1);
		News news2 = new News();
		news2.setNewsId(2L);
		listOfNews.add(news2);
	

		Author author = new Author(1L, "one", null);

		Tag tag = new Tag(5L, "t");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag);
		Filter filter = new Filter();
		filter.setAuthor(author);
		filter.setTags(tags);
		
		PagedView pW = new PagedView();
		pW.setCurrentPage(0);
		
		
		Set<Comment> comments = new HashSet<Comment>();
		comments.add(new Comment());
		
		
		when(newsService.getListNews(pW,filter)).thenReturn(listOfNews);
	
		newsManagementService.getListNews(pW,filter);
		
		verify(newsService).getListNews(pW,filter);

		
	}
	

	@Test
	public void editNewsTest() throws Exception {
		News news1 = new News();
		news1.setNewsId(40L);
		Tag tag = new Tag(5L, "t");
		Tag tag2 = new Tag(7L, "ttt");
		List<Long> tagsIdBefore = new ArrayList<Long>();
		tagsIdBefore.add(5L);
		tagsIdBefore.add(7L);
		
		List<Tag> tagsBefore = new ArrayList<Tag>();
		tagsBefore.add(tag);
		tagsBefore.add(tag2);
		
		List<Long> tagsIdAfter = new ArrayList<Long>();
		tagsIdAfter.add(7L);
		tagsIdAfter.add(8L);
		
		newsManagementService.editNews(news1);

		verify(newsService).update(news1);
	}

	@Test
	public void deleteNewsTOTest() throws Exception {

		newsManagementService.deleteNews(1L);
		verify(newsService).delete(1L);
	}

	@Test
	public void showSingleNewsTest() throws Exception {

		News news = new News();
		news.setNewsId(1L);

		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(7L,"q"));
		tags.add(new Tag(8L,"q8"));

		Set<Comment> comments = new HashSet<Comment>();
		comments.add(new Comment());

		when(newsService.getById(1L)).thenReturn(news);
		
		newsManagementService.getNews(1L);
		verify(newsService).getById(1L);

	}

	@Test
	public void addAuthorTest() throws Exception {

		Author a1 = new Author(1L, "qwert", null);
		Author a2 = new Author(2L, "asdfg", null);

		when(authorService.getAuthorId(a1)).thenReturn(1L);
		try {
			newsManagementService.addAuthor(a1);
			fail("Exception is required");
		} catch (ServiceException e) {
			//catch exception
		}
		verify(authorService, never()).create(a1);

		when(authorService.getAuthorId(a1)).thenReturn(0L);
		when(authorService.create(a2)).thenReturn(2L);
		newsManagementService.addAuthor(a2);
		verify(authorService).create(a2);
		assertEquals(2L, newsManagementService.addAuthor(a2));
	}


	@Test
	public void addTagTest() throws Exception {

		Tag t1 = new Tag(1L, "get");
		Tag t2 = new Tag(2L, "post");

		when(tagService.getByName(t1.getTagName())).thenReturn(t1);
		try {
			newsManagementService.addTag(t1);
			fail("Exception is required");
		} catch (ServiceException e) {
			//catch exception
		}
		verify(tagService, never()).create(t1);

		when(tagService.getByName(t2.getTagName())).thenReturn(null);
		when(tagService.create(t2)).thenReturn(2L);
		newsManagementService.addTag(t2);
		verify(tagService).create(t2);
		assertEquals(2L, newsManagementService.addTag(t2));
	}


}
