package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private ITagDAO tagDAO;

	@InjectMocks
	private TagServiceImpl tagService;

	private long id = 1L;
	private long id2 = 2L;
	private Tag tag = new Tag();
	private Tag tag2 = new Tag(2, "Tag2");
	private Tag tagNull = null;

	@Test
	public void createTest() throws Exception {

		when(tagDAO.create(tag)).thenReturn(id);
		tagService.create(tag);
		verify(tagDAO).create(tag);
		assertEquals(id, tagService.create(tag));
	}

	@Test(expected = ServiceException.class)
	public void createTestException() throws Exception {

		tagService.create(tagNull);

	}

	@Test
	public void updateTest() throws Exception {

		tagService.update(tag);
		verify(tagDAO).update(tag);

	}

	@Test(expected = ServiceException.class)
	public void updateTestException() throws Exception {

		tagService.update(tagNull);

	}

	@Test
	public void deleteTest() throws Exception {

		tagService.delete(1L);
		verify(tagDAO).delete(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteTestException() throws Exception {

		tagService.delete(0L);

	}

	@Test
	public void findByIdTest() throws Exception {

		when(tagDAO.getById(id)).thenReturn(tag);
		when(tagDAO.getById(id2)).thenReturn(tag2);

		tagService.getById(id);
		verify(tagDAO).getById(id);
		tagService.getById(id2);
		verify(tagDAO).getById(id2);

		assertFalse(tagService.getById(id).equals(tagService.getById(id2)));
	}

	@Test(expected = ServiceException.class)
	public void findByIdTestException() throws Exception {

		tagService.getById(-1L);

	}

	@Test
	public void findByNameTest() throws Exception {

		when(tagDAO.getByName("Tag2")).thenReturn(tag2);
		tagService.getByName("Tag2");
		verify(tagDAO).getByName("Tag2");
		assertEquals(tag2, tagService.getByName("Tag2"));
	}

	@Test(expected = ServiceException.class)
	public void findByNameTestException() throws Exception {

		tagService.getByName(null);

	}

	@Test
	public void getAllRowsTest() throws Exception {

		tagService.getAll();
		verify(tagDAO).getAll();

	}
}
