package com.epam.newsmanagement.utils;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners(DependencyInjectionTestExecutionListener.class)
public class NavigationTest {

	@Autowired
	private PagedView pagedView;
	
	private int rowCount=20;
	
	
	
	@Test
	public void getIndexListTest() throws Exception {

		int[] index = {1,2,3,4,5,6,7};
		
		pagedView.setRowCount(rowCount);
		int pageCount = pagedView.getPageCount();
		
		assertEquals(index.length,pageCount);
		
		
		int[] indexCount = pagedView.getIndexList();
		
		for(int i =0; i<pageCount; i++){
		assertEquals(index[i],indexCount[i]);
		}
	}
	
	@Test
	public void getPageCountTest() throws Exception {

		int page = 7;
		
		pagedView.setRowCount(rowCount);
		int pageCount = pagedView.getPageCount();
		
		assertEquals(page,pageCount);
	

	}
	
}
