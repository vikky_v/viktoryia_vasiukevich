package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.IUserDAO;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	
	@Mock
	private IUserDAO userDAO;

	@InjectMocks
	private UserServiceImpl userService;
	
	@Test
	public void createTest() throws Exception {

		when(userDAO.getNameByLogin("admin")).thenReturn("Admin Admin");
		userService.getNameByLogin("admin");
		verify(userDAO).getNameByLogin("admin");
		assertEquals("Admin Admin", userService.getNameByLogin("admin"));

	}
	
	
}
