package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock
	private INewsDAO newsDAO;

	@InjectMocks
	private NewsServiceImpl newsService;

	private long id = 1L;
	private long id2 = 2L;
	//private Author author = new Author(1L,"author",null);
	private News news = new News();
	private News news2 = new News(2L, "2", "2", "2", new Timestamp(
			new Date().getTime()), new Timestamp(new Date().getTime()));
	private News newsNull = null;

	@Test
	public void createTest() throws Exception {

		when(newsDAO.create(news)).thenReturn(id);
		newsService.create(news);
		verify(newsDAO).create(news);
		assertEquals(id, newsService.create(news));
	}

	@Test(expected = ServiceException.class)
	public void createTestException() throws Exception {

		newsService.create(newsNull);

	}

	@Test
	public void updateTest() throws Exception {

		newsService.update(news);
		verify(newsDAO).update(news);
	}

	@Test(expected = ServiceException.class)
	public void updateTestException() throws Exception {

		newsService.update(newsNull);

	}

	@Test
	public void deleteTest() throws Exception {

		newsService.delete(1L);
		verify(newsDAO).delete(1L);
	}

	@Test(expected = ServiceException.class)
	public void deleteTestException() throws Exception {

			newsService.delete(-1L);

	}

	@Test
	public void findByIdTest() throws Exception {

		when(newsDAO.getById(id)).thenReturn(news);
		when(newsDAO.getById(id2)).thenReturn(news2);

		newsService.getById(id);
		verify(newsDAO).getById(id);

		newsService.getById(id2);
		verify(newsDAO).getById(id2);

		assertFalse(newsService.getById(id).equals(newsService.getById(id2)));

	}

	@Test(expected = ServiceException.class)
	public void findByIdTestException() throws Exception {

		newsService.getById(0L);

	}

	@Test
	public void getFilteredNewsForPageTest() throws Exception {
		Author a = new Author();
		List<Tag> list = new ArrayList<Tag>();
		Filter filter = new Filter();
		filter.setAuthor(a);
		filter.setTags(list);
		
		PagedView pW = new PagedView();
		pW.setCurrentPage(0);
		
				
		newsService.getListNews(pW,filter);
		verify(newsDAO).getNews(pW,filter);
		
	}


	@Test
	public void countNewsTest() throws Exception {
		Author a = new Author();
		List<Tag> list = new ArrayList<Tag>();
		Filter filter = new Filter();
		filter.setAuthor(a);
		filter.setTags(list);
		
		when(newsDAO.countNews(filter)).thenReturn(0);
		
		newsService.countNews(filter);
		verify(newsDAO).countNews(filter);
		
		
	}
	

//	@Test
//	public void getAuthorByNewsTest() throws Exception {
//
//		when(newsDAO.getAuthorByNews(id)).thenReturn(author);
//
//		newsService.getAuthorByNews(id);
//		verify(newsDAO).getAuthorByNews(id);
//
//		assertEquals(author, newsService.getAuthorByNews(id));
//	}
//
//	@Test(expected = ServiceException.class)
//	public void getAuthorIDByNewsTestException() throws Exception {
//
//		newsService.getAuthorByNews(-1L);
//
//	}
//
//	@Test
//	public void getNewsByAuthorTest() throws Exception {
//
//		newsService.getNewsByAuthor(1L);
//		verify(newsDAO).getNewsByAuthor(1L);
//	}
//
//	@Test(expected = ServiceException.class)
//	public void getNewsIDByAuthorTestException() throws Exception {
//
//			newsService.getNewsByAuthor(0L);
//
//	}
//
//	@Test
//	public void createNewsAuthorTest() throws Exception {
//
//		newsService.createNewsAuthor(1L, 1L);
//		verify(newsDAO).createNewsAuthor(1L, 1L);
//	}
//
//	@Test(expected = ServiceException.class)
//	public void createNewsAuthorTestException() throws Exception {
//
//		newsService.createNewsAuthor(-1L, -1L);
//	
//	}

//	@Test
//	public void updateNewsAuthorTest() throws Exception {
//
//		newsService.updateNewsAuthor(1L, 1L);
//		verify(newsDAO).updateNewsAuthor(1L, 1L);
//	}

//	@Test(expected = ServiceException.class)
//	public void updateNewsAuthorTestException() throws Exception {
//
//		newsService.updateNewsAuthor(0L, 0L);
//
//	}

//	@Test
//	public void getTagsByNewsTest() throws Exception {
//
//		newsService.getTagsByNews(1L);
//		verify(newsDAO).getTagsByNews(1L);
//	}
//
//	@Test(expected = ServiceException.class)
//	public void getTagsByNewsTestException() throws Exception {
//
//			newsService.getTagsByNews(0L);
//
//	}

//	@Test
//	public void createNewsTagTest() throws Exception {
//
//		newsService.createNewsTag(1L, 1L);
//		verify(newsDAO).createNewsTag(1L, 1L);
//	}

//	@Test(expected = ServiceException.class)
//	public void createNewsTagTestException() throws Exception {
//
//		newsService.createNewsTag(1L, 0L);
//		
//	}

//	@Test
//	public void deleteNewsTagTest() throws Exception {
//
//		newsService.deleteNewsTag(1L, 1L);
//		verify(newsDAO).deleteNewsTag(1L, 1L);
//	}

//	@Test(expected = ServiceException.class)
//	public void deleteNewsTagTestException() throws Exception {
//	
//		newsService.deleteNewsTag(-1L, 1L);
//		
//	}

	@Test
	public void getNewsByTagTest() throws Exception {

		newsService.getNewsByTag(1L);
		verify(newsDAO).getNewsByTag(1L);
	}

	@Test(expected = ServiceException.class)
	public void getNewsByTagTestException() throws Exception {

		newsService.getNewsByTag(-1L);
	
	}

//	@Test
//	public void deleteNewsTagByNewsIdTest() throws Exception {
//
//		newsService.deleteNewsTagByNewsID(1L);
//		verify(newsDAO).deleteNewsTagByNewsID(1L);
//	}

//	@Test(expected = ServiceException.class)
//	public void deleteNewsTagByNewsIdTestException() throws Exception {
//
//		newsService.deleteNewsTagByNewsID(0L);
//
//	}

//	@Test
//	public void deleteNewsAuthorByNewsIdTest() throws Exception {
//
//		newsService.deleteNewsAuthorByNewsID(1L);
//		verify(newsDAO).deleteNewsAuthorByNewsID(1L);
//	}
//
//	@Test(expected = ServiceException.class)
//	public void deleteNewsAuthorByNewsIdTestException() throws Exception {
//
//		newsService.deleteNewsAuthorByNewsID(0L);
//	
//	}
}
