package com.epam.newsmanagement.dao.impl.hibernate;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dto.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,TransactionalTestExecutionListener.class  })
@TransactionConfiguration(transactionManager="transactionManager")
@Transactional
public class CommentDAOTest {

	@Autowired
	@Qualifier("commentDAOHibernate")
	private ICommentDAO commentDAO;
	
	@Autowired
	@Qualifier("newsDAOHibernate")
	private INewsDAO newsDAO;
	
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/CommentDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCreate() throws Exception {
		long id = 0;
		Comment c = new Comment();
		c.setCommentText("comment");
		c.setNewsId(2L);
				
		id = commentDAO.create(c);

		assertTrue(id > 0);
	}


	@Test
	@DatabaseSetup("classpath:/resources/setup/CommentDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDelete() throws Exception {

		commentDAO.delete(1L);
		Set<Comment> comments = newsDAO.getById(1L).getComments();
		assertEquals(2, comments.size());
	}

}
