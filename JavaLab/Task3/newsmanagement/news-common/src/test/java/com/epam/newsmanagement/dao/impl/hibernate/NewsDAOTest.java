package com.epam.newsmanagement.dao.impl.hibernate;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@TransactionConfiguration(transactionManager="transactionManager")
@Transactional
public class NewsDAOTest {
	
	@Autowired
	@Qualifier("newsDAOHibernate")
	private INewsDAO newsDAO;

	@Autowired
	private PagedView pagedView;

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE)
	public void testCreate() throws Exception {

		News news = new News();
		news.setTitle("aaa");
		news.setShortText("qwety");
		news.setFullText("qwetyruytuighhfj");
		news.setCreationDate(new Timestamp(new Date().getTime()));
		news.setModificationDate(new Timestamp(new Date().getTime()));
		news.setAuthor(new Author(19L,"qq",null));
		
		
		
		long id = newsDAO.create(news);

		List<News> newsList = newsDAO.getAll();
		assertEquals(7, newsList.size());
		assertTrue(id > 0);
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {
		News news = new News(13, "------", "qwetyruytuighhfj", "131313",
				new Timestamp(new Date().getTime()), new Timestamp(
						new Date().getTime()));

		news.setAuthor(new Author(20L,"Sergey",null));
		
		Tag tag = new Tag(2L,"moovie");
		Tag tag22 = new Tag(567L,"5667");
		Set<Tag> list = new HashSet<Tag>();
		list.add(tag);
		list.add(tag22);
		news.setTags(list);
		
		newsDAO.update(news);

		News news2 = newsDAO.getById(13);
	

		assertEquals(news.getNewsId(), news2.getNewsId());
		assertEquals(news.getShortText(), news2.getShortText());
		assertEquals(news.getFullText(), news2.getFullText());
		assertEquals(news.getTitle(), news2.getTitle());
		
		assertEquals(20L,news.getAuthor().getAuthorId());
		assertTrue(news.getTags().contains(tag));
		assertEquals(2,news.getTags().size());
		

		Calendar now = Calendar.getInstance();
		Long time = news.getModificationDate().getTime();
		now.setTimeInMillis(time);
		now.set(Calendar.AM_PM, 0);
		now.set(Calendar.HOUR, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		
		Calendar now2 = Calendar.getInstance();
		Long time2 = news2.getModificationDate().getTime();
		now2.setTimeInMillis(time2);
		now2.set(Calendar.AM_PM, 0);
		now2.set(Calendar.HOUR, 0);
		now2.set(Calendar.MINUTE, 0);
		now2.set(Calendar.SECOND, 0);
		now2.set(Calendar.MILLISECOND, 0);

		assertEquals(now, now2);
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagauthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDelete() throws Exception {

		newsDAO.delete(6);
		List<News> newsList = newsDAO.getAll();

		assertEquals(5, newsList.size());
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetById() throws Exception {

		News news = new News(3, "1 Short Text here...",
				"1 Short Text here... And then full text...", "1st News",
				new Timestamp(new Date().getTime()), new Timestamp(
						new Date().getTime()));
		News news2 = newsDAO.getById(3);

		assertEquals(news2.getNewsId(), news2.getNewsId());
		assertEquals(news.getShortText(), news2.getShortText());
		assertEquals(news.getFullText(), news2.getFullText());
		assertEquals(news.getTitle(), news2.getTitle());
		
		System.err.println(news2);	
		System.err.println(news2.getFullText()+" \n"+news2.getAuthor()+" \n"+news2.getTags());
		System.err.println(news2.getComments());
	

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetFilteredNews() throws Exception {
		
		
		
		Author a = new Author(18, "Oleg", null);
		Tag t = new Tag(3,"news");
		List<Tag> listTag= new ArrayList<Tag>();
		listTag.add(t);
		Filter filter = new Filter();
		filter.setAuthor(a);
		filter.setTags(listTag);
		
			
		pagedView.setRowCount(newsDAO.countNews(filter).intValue());
		pagedView.setCurrentPage(0);
		
		List<News> newsList = newsDAO.getNews(pagedView,filter);
		
		System.out.println("1)  "+newsList);
		
		assertEquals(2, newsList.size());
		
		
		filter = new Filter();
		filter.setAuthor(new Author());
		filter.setTags(Collections.<Tag>emptyList());
		newsList = newsDAO.getNews(pagedView,filter);
		assertEquals(3, newsList.size());
		
		System.out.println("2)  "+newsList);

	}
	

	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCountNews() throws Exception {
		
		Filter filter = new Filter();
		filter.setAuthor(new Author());
		filter.setTags(Collections.<Tag>emptyList());
		
		Number numberOfNews = newsDAO.countNews(filter);

		assertEquals(6L, numberOfNews.longValue());

	}
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testCountFilteredNews() throws Exception {
		
		Author a = new Author();
		a.setAuthorId(18);
		Filter filter = new Filter();
		filter.setAuthor(a);
		
		
		Number numberOfNews = newsDAO.countNews(filter);
		assertEquals(2, numberOfNews.intValue());
		
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1,""));
		tags.add(new Tag(2,""));
		tags.add(new Tag(3,""));
		filter = new Filter();
		filter.setTags(tags);
		//filter.setAuthor(a);
		
		numberOfNews = newsDAO.countNews(filter);
		assertEquals(4, numberOfNews.intValue());
		//assertEquals(2, numberOfNews.intValue());

	}
	
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAll() throws Exception {
		List<News> newsList = newsDAO.getAll();

		System.out.println(newsList.get(2).getFullText());
		System.out.println(newsList.get(2).getFullText()+" \n"+newsList.get(2).getAuthor()+" \n"+newsList.get(2).getTags());
		System.out.println(newsList.get(2).getComments());
		System.out.println(newsList.get(2).getComments());
			
		assertEquals(6, newsList.size());
		
		

	}
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetPrevNextNewsId() throws Exception {
		
		Filter filter = new Filter();
			
		List<Long> number = newsDAO.getPrevNextNewsId(filter, 13);
		
		assertEquals(2,number.size());
		long prev = number.get(0);
		assertEquals(4L,prev);
		long next = number.get(1);
		assertEquals(5L,next);
			
	}
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetPrevNextNewsIdFilter() throws Exception {
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1, ""));
		tags.add(new Tag(2, ""));
		tags.add(new Tag(3, ""));

		Filter filter = new Filter();
		filter.setTags(tags);

		List<Long> number = newsDAO.getPrevNextNewsId(filter, 4);

		assertEquals(2, number.size());
		long prev = number.get(0);
		assertEquals(0L, prev);
		long next = number.get(1);
		assertEquals(13L, next);

		Author a = new Author();
		filter = new Filter();
		filter.setAuthor(a);

		a.setAuthorId(18L);
		number = newsDAO.getPrevNextNewsId(filter, 4);
		assertEquals(2, number.size());
		prev = number.get(0);
		assertEquals(0L, prev);
		next = number.get(1);
		assertEquals(5L, next);

		filter.setAuthor(a);
		filter.setTags(tags);
		number = newsDAO.getPrevNextNewsId(filter, 4);
	
	}
	
	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDeleteNewsTag() throws Exception {

		News news5 = newsDAO.getById(5L);
		System.out.println(news5.getTags().size());
		news5.setTags(null);
		
		newsDAO.update(news5);
		
		news5 = newsDAO.getById(5L);		

		assertEquals(null, news5.getTags());
	}

	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetNewsByTag() throws Exception {

		List<News> news = newsDAO.getNewsByTag(3);
	
		assertEquals(3, news.size());
		
		assertTrue(news.get(0).getNewsId()==4L || news.get(0).getNewsId()==5L || news.get(0).getNewsId()==13L);
		assertTrue(news.get(1).getNewsId()==4L || news.get(1).getNewsId()==5L || news.get(1).getNewsId()==13L);
		assertTrue(news.get(2).getNewsId()==4L || news.get(2).getNewsId()==5L || news.get(2).getNewsId()==13L);
	}


	
	@Test
	@DatabaseSetup("classpath:/resources/setup/NewsTagAuthorDS.xml")
	@DatabaseTearDown(value = "classpath:/resources/setup/NewsTagAuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetNewsVersion() throws Exception {

		News news = new News();
		news.setNewsId(3L);
		long version = newsDAO.getCurrentVersion(news);

		assertEquals(0, version);			
			
	}

}
