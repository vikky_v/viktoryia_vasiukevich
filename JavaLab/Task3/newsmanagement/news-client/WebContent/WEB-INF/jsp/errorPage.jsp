<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<head>
<title><spring:message code="error.message" /></title>
</head>
<div class="body">

	<h1>
		<b><spring:message code="error.message" /></b>
	</h1>
	<p>
		<c:if test="${not empty errClass}">
			<h2>${errClass}</h2>
		</c:if>
	</p>
	<p>
		<c:if test="${not empty errMsg}">
			<h2>${errMsg}</h2>
		</c:if>
	</p>
	<p>
		<a href="news/list"><spring:message code="error.link" /></a>
	</p>
</div>