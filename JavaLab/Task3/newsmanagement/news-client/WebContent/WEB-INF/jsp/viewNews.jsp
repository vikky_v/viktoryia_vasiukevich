<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="body">
	<a class="left" href="news/list?page=${page}"><spring:message
			code="link.back" /></a>
	<p>
	<div class="news view">
		<div class="title">
			<b><c:out value="${news.title}"></c:out></b> &nbsp; (by <c:out value="${news.author.authorName}"></c:out>)
		</div>
		<div class="date">
			<fmt:setBundle basename="message" var="bundle" />
			<fmt:message bundle="${bundle}" key="date.pattern" var="pattern" />
			<fmt:formatDate pattern="${pattern}" value="${news.modificationDate}" />
		</div>
		<div class="fullText">
			<p><c:out value="${news.fullText}"></c:out></p>
		</div>
	</div>

	<c:forEach items="${news.comments}" var="comments">
		<div class="comment">
			<div class="dateComment">
				<fmt:setBundle basename="message" var="bundle" />
				<fmt:message bundle="${bundle}" key="date.pattern" var="pattern" />
				<fmt:formatDate pattern="${pattern}"
					value="${comments.creationDate}" />
			</div>
			<div class="commentText"><c:out value="${comments.commentText}" ></c:out></div>
		</div>
		<p>
	</c:forEach>
	<div class="comment">
		<form:form commandName="comment" method="POST"
			action="news/comment/add">

			<form:hidden path="newsId" value="${news.newsId}" />
			<form:textarea path="commentText" cols="47" required="required" />
			<div class="buttonComment">
				<input type="submit" class="postCommentButton"
					value="<spring:message code="button.postcomment"/>" />
			</div>
		</form:form>
	</div>
	<br> <br>
	<div class="bottom">
		<div class="previous">
			<c:if test="${previous > 0}">

				<a href="?newsId=${previous}"><spring:message
						code="link.previous" /></a>

			</c:if>
		</div>
		<div class="next">
			<c:if test="${next > 0}">

				<a href="?newsId=${next}"><spring:message code="link.next" /></a>

			</c:if>
		</div>

	</div>
	<br> <br> <br>

</div>