package com.epam.news.service;

import java.util.List;

import com.epam.news.dto.Tag;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface for service layer for {@link ITagDAO.class}
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface ITagService {

	/**
	 * Check if Tag object is null and then invoke ITagDAO create(Tag) method
	 * 
	 * @param ob
	 *            Tag object that should be created
	 * @return ID of created Tag
	 * @throws ServiceException
	 *             when Tag ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public long create(Tag ob) throws ServiceException;

	/**
	 * Check if Tag object is null and then invoke ITagDAO update(Tag) method
	 * 
	 * @param ob
	 *            Tag object that should be created
	 * @throws ServiceException
	 *             when Tag ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void update(Tag ob) throws ServiceException;

	/**
	 * Check if id==0 and then invoke ITagDAO delete(id) method
	 * 
	 * @param id
	 *            ID of Tag that should be deleted
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void delete(long id) throws ServiceException;

	/**
	 * Check if id==0 and then invoke ITagDAO findById(id) method
	 * 
	 * @param id
	 *            ID of Tag that should be found
	 * @return Tag object
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public Tag findById(long id) throws ServiceException;

	/**
	 * Check if name is null and then invoke ITagDAO findByName(name) method
	 * 
	 * @param name
	 *            Name of Tag that should be found
	 * @return Tag object
	 * @throws ServiceException
	 *             when name is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public Tag findByName(String name) throws ServiceException;

	/**
	 * Invoke ITagDAO getAllRows method
	 * 
	 * @return List of Tag objects
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<Tag> getAllRows() throws ServiceException;

}
