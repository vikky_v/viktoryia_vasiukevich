package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dto.Author;
import com.epam.news.dto.Comment;
import com.epam.news.dto.News;
import com.epam.news.dto.NewsVO;
import com.epam.news.dto.Tag;
import com.epam.news.service.INewsManagementService;
import com.epam.news.service.exception.ServiceException;

@Service
public class NewsManagementServiceImpl implements INewsManagementService {

	private Logger log = Logger.getLogger(NewsManagementServiceImpl.class
			.getName());

	@Autowired
	private AuthorServiceImpl authorService;

	@Autowired
	private CommentServiceImpl commentService;

	@Autowired
	private NewsServiceImpl newsService;

	@Autowired
	private TagServiceImpl tagService;

	@Transactional
	public List<NewsVO> showAllNews() throws ServiceException {

		List<NewsVO> listOfNewsTO = new ArrayList<NewsVO>();
		try {

			// get list of News without tags, author, comments

			List<News> listOfNews = newsService.getAllRows();

			Iterator<News> it = listOfNews.iterator();
			while (it.hasNext()) {

				// for each News get Author
				News news = it.next();
				long newsId = news.getNewsId();

				Author author = newsService.getAuthorByNews(newsId);

				// get list of Tags by news id
				List<Tag> listOfTags = newsService.getTagsByNews(newsId);

				// get list of comments by news id
				List<Comment> listOfComments = commentService
						.findByNewsID(newsId);

				// create NewsVO (News with Author, Comments, Tags)
				NewsVO newsTO = new NewsVO(news, author, listOfTags,
						listOfComments);

				listOfNewsTO.add(newsTO);

			}

		} catch (ServiceException e) {
			log.error("Exception - showAllNews", e);
			throw new ServiceException("Can't show all news", e);
		}

		return listOfNewsTO;
	}

	@Transactional
	public long addNews(News news, Author author, List<Tag> tag)
			throws ServiceException {

		long newsId = 0L;
		try {

			if (author != null) {

				// get author id and news id

				long authorId = author.getAuthorId();
				newsId = newsService.create(news);

				// create news_author row
				newsService.createNewsAuthor(newsId, authorId);

				if (tag != null) {

					// for each Tag get tag id and
					// create news_tag row
					Iterator<Tag> it = tag.iterator();
					while (it.hasNext()) {
						Tag current = it.next();
						long tagId = current.getTagId();
						newsService.createNewsTag(newsId, tagId);
					}
				}
			}

		} catch (ServiceException e) {
			log.error("ServiceException - addNews", e);
			throw new ServiceException("News wasn't added", e);
		}
		return newsId;
	}

	@Transactional
	public void editNews(News news, Author author, List<Tag> tag)
			throws ServiceException {

		try {
			newsService.update(news);
			long newsId = news.getNewsId();
			long authorId = author.getAuthorId();
			newsService.updateNewsAuthor(newsId, authorId);

			// delete previous tags
			newsService.deleteNewsTagByNewsID(newsId);

			// insert new tags
			Iterator<Tag> it = tag.iterator();
			while (it.hasNext()) {
				Tag current = it.next();
				long tagId = current.getTagId();
				newsService.createNewsTag(newsId, tagId);
			}

		} catch (ServiceException e) {
			log.error("ServiceException - editNews", e);
			throw new ServiceException("News wasn't updated", e);
		}
	}

	@Transactional
	public void deleteNewsTO(long newsId) throws ServiceException {

		try {
			newsService.deleteNewsTagByNewsID(newsId);
			newsService.deleteNewsAuthorByNewsID(newsId);
			commentService.deleteByNewsID(newsId);
			newsService.delete(newsId);
		} catch (ServiceException e) {
			log.error("Exception - deleteNewsTO", e);
			throw new ServiceException("News wasn't deleted", e);
		}

	}

	@Transactional
	public NewsVO showSingleNewsVO(long newsId) throws ServiceException {
		NewsVO singleNewsVO = null;
		try {
			News singleNews = newsService.findById(newsId);

			Author author = newsService.getAuthorByNews(newsId);

			List<Tag> tags = newsService.getTagsByNews(newsId);

			List<Comment> comments = commentService.findByNewsID(newsId);

			singleNewsVO = new NewsVO(singleNews, author, tags, comments);

		} catch (ServiceException e) {
			log.error("Exception - showSingleNewsTO", e);
			throw new ServiceException("Can't show news", e);
		}
		return singleNewsVO;
	}

	public long addAuthor(Author author) throws ServiceException {
		long authorId = 0L;
		try {
			// if author with equals name doesn't exist
			if (!isAuthorExists(author)) {
				authorId = authorService.create(author);
			} else {
				throw new ServiceException(
						"Can't add Author. Author has already exist");
			}
		} catch (ServiceException e) {
			log.error("ServiceException - addAuthor", e);
			throw new ServiceException("Can't add author", e);
		}
		return authorId;
	}

	@Transactional
	public List<News> searchByAuthor(Author ob) throws ServiceException {
		List<News> authorNews = new ArrayList<News>();
		try {
			// get author id by name
			long authorId = authorService.getAuthorId(ob);

			if (authorId != 0) {
				authorNews = newsService.getNewsByAuthor(authorId);
			}
		} catch (ServiceException e) {
			log.error("Exception -  searchByAuthor ", e);
			throw new ServiceException("Can't search by author", e);
		}
		return authorNews;

	}

	public long addTag(Tag tag) throws ServiceException {
		long tagId = 0L;
		try {
			if (!isTagExists(tag)) {
				tagId = tagService.create(tag);
			} else {
				throw new ServiceException(
						"Can't add Tag. Tag has already exist");
			}
		} catch (ServiceException e) {
			log.error("ServiceException -  addTag ", e);
			throw new ServiceException("Can't add tag", e);
		}
		return tagId;

	}

	public void addTags(List<Tag> tags) throws ServiceException {
		try {
			Iterator<Tag> it = tags.iterator();
			while (it.hasNext()) {
				Tag currentTag = it.next();

				if (!isTagExists(currentTag)) {
					tagService.create(currentTag);
				} else {
					throw new ServiceException(
							"Can't add current tag. Tag has already exist");
				}

			}
		} catch (ServiceException e) {
			log.error("ServiceException -  addTags ", e);
			throw new ServiceException("Can't add tags", e);
		}

	}

	@Transactional
	public List<News> searchByTags(List<Tag> tags) throws ServiceException {
		// list of news which have at least one of such tags
		List<News> tagNews = new ArrayList<News>();
		try {

			// list of news which has at least one tag
			List<News> oneTagNews = new ArrayList<News>();

			// for each tag search it in DB and return its id
			Iterator<Tag> it = tags.iterator();
			while (it.hasNext()) {
				Tag current = it.next();
				long tagId = tagService.findByName(current.getTagName())
						.getTagId();

				// select news id's which have such tag
				oneTagNews = newsService.getNewsByTag(tagId);

				// if there are such news
				if (oneTagNews.size() != 0) {
					Iterator<News> itNews = oneTagNews.iterator();
					while (itNews.hasNext()) {
						
						News currentNews = itNews.next();

						// if list of news doesn't contain such news - add it
						if (!tagNews.contains(currentNews)) {
							tagNews.add(currentNews);
						}
					}
				}
			}
		} catch (ServiceException e) {
			log.error("Exception -  searchByTags ", e);
			throw new ServiceException("Can't search by tags", e);
		}
		return tagNews;

	}

	public void addComments(List<Comment> comments) throws ServiceException {
		try {
			Iterator<Comment> it = comments.iterator();
			while (it.hasNext()) {
				Comment current = it.next();
				commentService.create(current);
			}

		} catch (ServiceException e) {
			log.error("ServiceException -  addComments ", e);
			throw new ServiceException("Can't add comments", e);
		}

	}

	public void deleteComments(List<Long> commentIds) throws ServiceException {
		try {
			Iterator<Long> it = commentIds.iterator();
			while (it.hasNext()) {
				long commentId = it.next();
				commentService.delete(commentId);
			}
		} catch (ServiceException e) {
			log.error("ServiceException -  deleteComments ", e);
			throw new ServiceException("Can't delete comments", e);
		}

	}

	public List<Comment> showAllComments() throws ServiceException {
		List<Comment> allComments = new ArrayList<Comment>();
		try {
			allComments = commentService.getAllRows();
		} catch (ServiceException e) {
			log.error("ServiceException -  showAllComments ", e);
			throw new ServiceException("Can't show all comments", e);
		}
		return allComments;
	}

	/**
	 * Check by name if specified Author exists
	 * 
	 * @param author
	 *            Author that should be checked
	 * @return true - if Author with equals name exists, false - in another case
	 * 
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	private boolean isAuthorExists(Author author) throws ServiceException {
		long dbAuthorId = 0L;
		dbAuthorId = authorService.getAuthorId(author);
		if (dbAuthorId == 0) {
			return false;
		} else
			return true;
	}

	/**
	 * Check by name if specified Tag exists
	 * 
	 * @param tag
	 *            Tag that should be checked
	 * @return true - if Tag with equals name exists, false - in another case
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	private boolean isTagExists(Tag tag) throws ServiceException {
		String tagName = tag.getTagName();
		Tag dbTag = tagService.findByName(tagName);
		if (dbTag == null) {
			return false;
		} else
			return true;

	}

}
