package com.epam.news.dao;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.dto.Author;

/**
 * Extends {@link IDAO<Author>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface IAuthorDAO extends IDAO<Author> {

	/**
	 * Return Author's ID by Author's name
	 * 
	 * @param ob
	 *            Author, which ID should be gotten
	 * @return Author's ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public long getAuthorId(Author ob) throws DAOException;
}
