package com.epam.news.dao;

import java.util.List;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.dto.Comment;

/**
 * Extends {@link IDAO<Comment>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface ICommentDAO extends IDAO<Comment> {

	/**
	 * Delete all Comments found by News' ID
	 * 
	 * @param newsId
	 *            ID of News which comments should be deleted
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteByNewsID(long newsId) throws DAOException;

	/**
	 * Search all Comments for specified News' ID
	 * 
	 * @param newsId
	 *            ID of News which comments should be found
	 * @return List of Comments
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<Comment> findByNewsID(long newsId) throws DAOException;

}
