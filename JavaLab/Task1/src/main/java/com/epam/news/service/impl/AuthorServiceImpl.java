package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.dao.impl.AuthorDAOImpl;
import com.epam.news.dto.Author;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.exception.ServiceException;

@Service
public class AuthorServiceImpl implements IAuthorService {

	private Logger log = Logger.getLogger(AuthorServiceImpl.class.getName());

	@Autowired
	private AuthorDAOImpl authorDAO;

	public long create(Author ob) throws ServiceException {
		long authorId = 0;
		if (ob != null) {
			try {
				authorId = authorDAO.create(ob);
			} catch (DAOException e) {
				log.error("Can't create author", e);
				throw new ServiceException("Can't create author", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return authorId;
	}

	public void update(Author ob) throws ServiceException {

		if (ob != null) {
			try {
				authorDAO.update(ob);
			} catch (DAOException e) {
				log.error("Can't update author", e);
				throw new ServiceException("Can't update author", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
	}

	public void delete(long id) throws ServiceException {

		if (id > 0) {
			try {
				authorDAO.delete(id);
			} catch (DAOException e) {
				log.error("Can't delete author", e);
				throw new ServiceException("Can't delete author", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public Author findById(long id) throws ServiceException {
		Author author = null;
		if (id > 0) {
			try {
				author = authorDAO.findById(id);
			} catch (DAOException e) {
				log.error("Can't find author", e);
				throw new ServiceException("Can't find author", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}

		return author;
	}

	public long getAuthorId(Author ob) throws ServiceException {
		long id = 0L;
		if (ob != null) {
			try {
				id = authorDAO.getAuthorId(ob);
			} catch (DAOException e) {
				log.error("Can't get id", e);
				throw new ServiceException("Can't get id", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return id;
	}

	public List<Author> getAllRows() throws ServiceException {
		List<Author> listAuthor = null;
		try {
			listAuthor = authorDAO.getAllRows();
		} catch (DAOException e) {
			log.error("Can't get list of Authors", e);
			throw new ServiceException("Can't get list of Authors", e);
		}
		return listAuthor;
	}

}
