package com.epam.news.dao;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.dto.Tag;

/**
 * Extends {@link IDAO<Tag>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface ITagDAO extends IDAO<Tag> {

	/**
	 * Returns Tag object by specified name
	 * 
	 * @param name
	 *            Name of Tag that should be found
	 * @return Tag object with specified name
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public Tag findByName(String name) throws DAOException;

}
