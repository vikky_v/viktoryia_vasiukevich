package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.dto.Author;
import com.epam.news.dto.News;
import com.epam.news.dto.Tag;
import com.epam.news.service.INewsService;
import com.epam.news.service.exception.ServiceException;

@Service
public class NewsServiceImpl implements INewsService {

	private Logger log = Logger.getLogger(NewsServiceImpl.class.getName());

	@Autowired
	private NewsDAOImpl newsDAO;

	public long create(News ob) throws ServiceException {
		long newsId = 0L;
		if (ob != null) {
			try {
				newsId = newsDAO.create(ob);
			} catch (DAOException e) {
				log.error("Can't create news", e);
				throw new ServiceException("Can't create news", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return newsId;
	}

	public void update(News ob) throws ServiceException {

		if (ob != null) {
			try {
				newsDAO.update(ob);
			} catch (DAOException e) {
				log.error("Can't update news", e);
				throw new ServiceException("Can't update news", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
	}

	public void delete(long id) throws ServiceException {

		if (id > 0) {
			try {
				newsDAO.delete(id);
			} catch (DAOException e) {
				log.error("Can't delete news", e);
				throw new ServiceException("Can't delete news", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public News findById(long id) throws ServiceException {
		News news = null;
		if (id > 0) {
			try {
				news = newsDAO.findById(id);
			} catch (DAOException e) {
				log.error("Can't find news", e);
				throw new ServiceException("Can't find news", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return news;
	}

	public List<News> getAllRows() throws ServiceException {
		List<News> news = null;
		try {
			news = newsDAO.getAllRows();
		} catch (DAOException e) {
			log.error("Can't get list of news", e);
			throw new ServiceException("Can't get list of news", e);
		}
		return news;
	}

	public Author getAuthorByNews(long newsId) throws ServiceException {
		Author author = null;
		if (newsId > 0) {
			try {
				author = newsDAO.getAuthorByNews(newsId);
			} catch (DAOException e) {
				log.error("Can't get author", e);
				throw new ServiceException("Can't get author", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return author;
	}

	public List<News> getNewsByAuthor(long authorId)
			throws ServiceException {
		List<News> news = null;
		if (authorId > 0) {
			try {
				news = newsDAO.getNewsByAuthor(authorId);
			} catch (DAOException e) {
				log.error("Can't get news", e);
				throw new ServiceException("Can't get news", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return news;
	}

	public void createNewsAuthor(long newsId, long authorId)
			throws ServiceException {
		if (newsId > 0 && authorId > 0) {
			try {
				newsDAO.createNewsAuthor(newsId, authorId);
			} catch (DAOException e) {
				log.error("Can't create news author row", e);
				throw new ServiceException("Can't create news author row",
						e);
			}
		} else {
			log.error("newsId <= 0 or authorId <= 0");
			throw new ServiceException("newsId <= 0 or authorId <= 0");
		}
	}

//	public Map<Long, Long> getAllRowsNewsAuthor() throws ServiceException {
//
//		Map<Long, Long> news_author = null;
//		try {
//			news_author = newsDAO.getAllRowsNewsAuthor();
//		} catch (DAOException e) {
//			log.error("Can't get all rows news author", e);
//			throw new ServiceException("Can't get all rows news author", e);
//		}
//		return news_author;
//	}

	public void updateNewsAuthor(long newsId, long authorId)
			throws ServiceException {
		if (newsId > 0 && authorId > 0) {
			try {
				newsDAO.updateNewsAuthor(newsId, authorId);
			} catch (DAOException e) {
				log.error("Can't get update news author row", e);
				throw new ServiceException(
						"Can't get update news author row", e);
			}
		} else {
			log.error("newsId <= 0 or authorId <= 0");
			throw new ServiceException("newsId <= 0 or authorId <= 0");
		}
	}

	public List<Tag> getTagsByNews(long newsId) throws ServiceException {
		List<Tag> tags = null;
		if (newsId > 0) {
			try {
				tags = newsDAO.getTagsByNews(newsId);
			} catch (DAOException e) {
				log.error("Can't get tag id", e);
				throw new ServiceException("Can't get tag id", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return tags;
	}

//	public Map<Long, ArrayList<Long>> getAllRowsNewsTag()
//			throws ServiceException {
//		Map<Long, ArrayList<Long>> news_tag = null;
//		;
//		try {
//			news_tag = newsDAO.getAllRowsNewsTag();
//		} catch (DAOException e) {
//			log.error("Can't  get all rows from news_tag", e);
//			throw new ServiceException("Can't  get all rows from news_tag",
//					e);
//		}
//		return news_tag;
//	}

	public void createNewsTag(long newsId, long tagId)
			throws ServiceException {
		if (newsId > 0 && tagId > 0) {
			try {
				newsDAO.createNewsTag(newsId, tagId);
			} catch (DAOException e) {
				log.error("Can't get create news tag row", e);
				throw new ServiceException("Can't get create news tag row",
						e);
			}
		} else {
			log.error("newsId <= 0 or tagId <= 0");
			throw new ServiceException("newsId <= 0 or tagId <= 0");
		}
	}

	public void deleteNewsTag(long newsId, long tagId)
			throws ServiceException {
		if (newsId > 0 && tagId > 0) {
			try {
				newsDAO.deleteNewsTag(newsId, tagId);
			} catch (DAOException e) {
				log.error("Can't delete news tag row", e);
				throw new ServiceException("Can't delete news tag row", e);
			}
		} else {
			log.error("newsId <= 0 or tagId <= 0");
			throw new ServiceException("newsId <= 0 or tagId <= 0");
		}
	}

	public List<News> getNewsByTag(long tagId) throws ServiceException {
		List<News> news = null;
		if (tagId > 0) {
			try {
				news = newsDAO.getNewsByTag(tagId);
			} catch (DAOException e) {
				log.error("Can't get news id", e);
				throw new ServiceException("Can't get news id", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return news;
	}

	public void deleteNewsTagByNewsID(long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				newsDAO.deleteNewsTagByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't delete news tag row", e);
				throw new ServiceException("Can't delete news tag row", e);
			}
		} else {
			log.error("newsId <= 0");
			throw new ServiceException("newsId <= 0");
		}

	}

	public void deleteNewsAuthorByNewsID(long newsId)
			throws ServiceException {
		if (newsId > 0) {
			try {
				newsDAO.deleteNewsAuthorByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't delete news author row", e);
				throw new ServiceException("Can't delete news author row",
						e);
			}
		} else {
			log.error("newsId <= 0");
			throw new ServiceException("newsId <= 0");
		}

	}
}
