package com.epam.news.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Entity associated with table 'COMMENTS'
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public class Comment implements Serializable{

	private static final long serialVersionUID = -1630503469313999113L;
	
	private long commentId;
	private String commentText;
	private Date creationDate;
	private long newsId;

	public Comment() {

	}

	public Comment(long comment_id, String comment_text, Date creation_date,
			long news_id) {

		this.commentId = comment_id;
		this.commentText = comment_text;
		this.creationDate = creation_date;
		this.newsId = news_id;
	}

	public long getCommentId() {
		return commentId;
	}

	public void setCommentId(long comment_id) {
		this.commentId = comment_id;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String comment_text) {
		this.commentText = comment_text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creation_date) {
		this.creationDate = creation_date;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(long news_id) {
		this.newsId = news_id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nComment [commentId=");
		builder.append(commentId);
		builder.append(", commentText=");
		builder.append(commentText);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append(", newsId=");
		builder.append(newsId);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + commentId);
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = (int) (prime * result + newsId);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Comment)) {
			return false;
		}
		Comment other = (Comment) obj;
		if (commentId != other.commentId) {
			return false;
		}
		if (commentText == null) {
			if (other.commentText != null) {
				return false;
			}
		} else if (!commentText.equals(other.commentText)) {
			return false;
		}
		if (creationDate == null) {
			if (other.creationDate != null) {
				return false;
			}
		} else if (!creationDate.equals(other.creationDate)) {
			return false;
		}
		if (newsId != other.newsId) {
			return false;
		}
		return true;
	}

}
