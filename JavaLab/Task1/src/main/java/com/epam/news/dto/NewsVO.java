package com.epam.news.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Value Object that aggregates the data from classes {@link News.class}
 * , {@link Author.class}, {@link Tag.class}, {@link Comment.class}
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public class NewsVO implements Serializable{

	
	private static final long serialVersionUID = -6147043338186530383L;
	
	private long newsId;
	private String shortText;
	private String fullText;
	private String title;
	private Date creationDate;
	private Date modificationDate;
	private Author author;
	private List<Tag> tags;
	private List<Comment> comments;

	public NewsVO(News news, Author author, List<Tag> tags,
			List<Comment> comments) {
		this.newsId = news.getNewsId();
		this.shortText = news.getShortText();
		this.fullText = news.getFullText();
		this.title = news.getTitle();
		this.creationDate = news.getCreationDate();
		this.modificationDate = news.getModificationDate();
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(long news_id) {
		this.newsId = news_id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String short_text) {
		this.shortText = short_text;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String full_text) {
		this.fullText = full_text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creation_date) {
		this.creationDate = creation_date;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modification_date) {
		this.modificationDate = modification_date;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public News getNews() {
		return new News(newsId, shortText, fullText, title, creationDate,
				modificationDate);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nNewsTO [newsId=");
		builder.append(newsId);
		builder.append(", shortText=");
		builder.append(shortText);
		builder.append(", fullText=");
		builder.append(fullText);
		builder.append(", title=");
		builder.append(title);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append(", modificationDate=");
		builder.append(modificationDate);
		builder.append(", author=");
		builder.append(author);
		builder.append(", tags=");
		builder.append(tags);
		builder.append(", comments=");
		builder.append(comments);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate
						.hashCode());
		result = (int) (prime * result + newsId);
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId != other.newsId)
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
