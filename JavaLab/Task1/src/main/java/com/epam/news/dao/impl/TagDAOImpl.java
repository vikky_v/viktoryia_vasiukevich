package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.connection.DBUtil;
import com.epam.news.dao.ITagDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.dto.Tag;

public class TagDAOImpl implements ITagDAO {

	@Autowired
	private DBUtil dbUtil;

	public long create(Tag ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long tagId = 0L;
		String generatedColumns[] = { "tag_id" };
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_TAG, generatedColumns);
			ps.setString(1, ob.getTagName());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while (rs.next()) {
				tagId = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return tagId;
	}

	public void update(Tag ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.UPDATE_TAG);
			ps.setString(1, ob.getTagName());
			ps.setLong(2, ob.getTagId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}
	}

	public void delete(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_TAG);
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}
	}

	public Tag findById(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Tag tag = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.FIND_TAG);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				tag = new Tag(rs.getLong(1), rs.getString(2));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return tag;
	}

	public Tag findByName(String name) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Tag tag = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.FIND_TAG_BY_NAME);
			ps.setString(1, name);
			rs = ps.executeQuery();
			while (rs.next()) {
				tag = new Tag(rs.getLong(1), rs.getString(2));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return tag;
	}

	public List<Tag> getAllRows() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		List<Tag> allTags = new ArrayList<Tag>();
		try {
			con = dbUtil.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQLQuery.SELECT_ALL_TAGS);
			while (rs.next()) {
				allTags.add(new Tag(rs.getLong(1), rs.getString(2)));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(st);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return allTags;
	}
}
