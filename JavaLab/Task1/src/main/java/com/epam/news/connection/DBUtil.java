package com.epam.news.connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * DBUtil - class for manage connections
 * 
 * @author Viktoryia Vasiukevich
 * @see java.sql.Connection
 */
public class DBUtil {

	private static Logger log = Logger.getLogger("dbUtil");
	
	@Autowired
	private DataSource dataSource;

	/**
	 * Get Connection from {@link #dataSource}
	 * 
	 * @return {@link Connection}
	 */
	public Connection getConnection() {
		Connection con = null;
			con = DataSourceUtils.getConnection(dataSource);
		return con;
		
	}

	/**
	 * Closes {@link ResultSet}
	 * 
	 * @param rs
	 *            ResultSet that should be closed
	 */
	public void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				log.error("Error while closing ResultSet", e);
			}
		}
	}

	/**
	 * Closes {@link Statement}
	 * 
	 * @param st
	 *            Statement that should be closed
	 */
	public void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				log.error("Error while closing Statement", e);
			}
		}
	}

	/**
	 * Closes {@link PreparedStatement}
	 * 
	 * @param ps
	 *            PreparedStatement that should be closed
	 */
	public void close(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				log.error("Error while closing PreparedStatement", e);
			}
		}
	}

	/**
	 * Closes connection
	 * 
	 * @param con
	 *            Connection that should be closed
	 */
	public void close(Connection con) {
		if (con != null) {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

}
