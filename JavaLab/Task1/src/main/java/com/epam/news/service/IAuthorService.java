package com.epam.news.service;

import java.util.List;

import com.epam.news.dto.Author;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface for service layer for {@link IAuthorDAO.class}
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface IAuthorService {

	/**
	 * Check if Author object is null and then invoke IAuthorDAO create(Author)
	 * method
	 * 
	 * @param ob
	 *            Author object that should be created
	 * @return ID of created Author
	 * @throws ServiceException
	 *             when Author ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public long create(Author ob) throws ServiceException;

	/**
	 * Check if Author object is null and then invoke IAuthorDAO update(Author)
	 * method
	 * 
	 * @param ob
	 *            Author object that should be updated
	 * @throws ServiceException
	 *             when Author ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void update(Author ob) throws ServiceException;

	/**
	 * Check if id==0 and then invoke IAuthorDAO delete(id) method
	 * 
	 * @param id
	 *            ID of Author that should be deleted
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void delete(long id) throws ServiceException;

	/**
	 * Check if id==0 and then invoke IAuthorDAO findById(id) method
	 * 
	 * @param id
	 *            ID of Author that should be found
	 * @return Author object
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public Author findById(long id) throws ServiceException;

	/**
	 * Check if Author object is null and then invoke IAuthorDAO
	 * getAuthorId(Author) method
	 * 
	 * @param ob
	 *            Author object which ID should be found
	 * @return ID of Author
	 * @throws ServiceException
	 *             when Author ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public long getAuthorId(Author ob) throws ServiceException;

	/**
	 * Invoke IAuthorDAO getAllRows method
	 * 
	 * @return List of Author objects
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<Author> getAllRows() throws ServiceException;
}
