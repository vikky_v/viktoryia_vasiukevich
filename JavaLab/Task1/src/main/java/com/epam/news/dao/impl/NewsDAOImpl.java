package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.connection.DBUtil;
import com.epam.news.dao.INewsDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.dto.Author;
import com.epam.news.dto.News;
import com.epam.news.dto.Tag;

public class NewsDAOImpl implements INewsDAO {

	@Autowired
	private DBUtil dbUtil;
	
	@Autowired
	private AuthorDAOImpl authorDAO;
	
	@Autowired
	private TagDAOImpl tagDAO;

	public long create(News ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long newsId = 0L;
		String generatedColumns[] = { "news_id" };
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_NEWS, generatedColumns);
			ps.setString(1, ob.getShortText());
			ps.setString(2, ob.getFullText());
			ps.setString(3, ob.getTitle());
			ps.setTimestamp(4, new Timestamp(new Date().getTime()));
			ps.setTimestamp(5, new Timestamp(new Date().getTime()));
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while (rs.next()) {
				newsId = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return newsId;
	}

	public void update(News ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.UPDATE_NEWS);
			ps.setString(1, ob.getShortText());
			ps.setString(2, ob.getFullText());
			ps.setString(3, ob.getTitle());
			ps.setTimestamp(4, new Timestamp(new Date().getTime()));
			ps.setLong(5, ob.getNewsId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}
	}

	public void delete(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS);
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}

	}

	public News findById(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		News news = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.FIND_NEWS);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				Timestamp tmstmpCreation = rs.getTimestamp(5);
				Date dateCreation = new Date(tmstmpCreation.getTime());
				Timestamp tmstmpModification = rs.getTimestamp(6);
				Date dateModification = new Date(tmstmpModification.getTime());

				news = new News(rs.getLong(1), rs.getString(2),
						rs.getString(3), rs.getString(4), dateCreation,
						dateModification);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return news;
	}

	public List<News> getAllRows() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		List<News> news = new ArrayList<News>();
		try {
			con = dbUtil.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQLQuery.SELECT_ALL_NEWS);
			while (rs.next()) {
				Timestamp tmstmpCreation = rs.getTimestamp(5);
				Date dateCreation = new Date(tmstmpCreation.getTime());
				Timestamp tmstmpModification = rs.getTimestamp(6);
				Date dateModification = new Date(tmstmpModification.getTime());

				news.add(new News(rs.getLong(1), rs.getString(2), rs
						.getString(3), rs.getString(4), dateCreation,
						dateModification));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(st);
			dbUtil.close(rs);
			dbUtil.close(con);
		}

		return news;
	}

	public Author getAuthorByNews(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long authorId = 0L;
		Author author = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_AUTHOR_ID_FROM_NEWSAUTHOR);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				authorId = rs.getLong(1);
			}
			
			if(authorId!=0){
			author = authorDAO.findById(authorId);
			}
			
			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		
		return author;
	}

	public List<News> getNewsByAuthor(long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		
		List<News> newsList = new ArrayList<News>();
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_NEWS_ID_FROM_NEWSAUTHOR);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			long newsId = 0L;
			News news = null;
			while (rs.next()) {
				newsId = rs.getLong(1);
				news = findById(newsId);
				newsList.add(news);
								
			}
									
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		
		return newsList;
	}

	public void createNewsAuthor(long newsId, long authorId)
			throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_NEWS_AUTHOR);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}
	}

	

	public void updateNewsAuthor(long newsId, long authorId)
			throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.UPDATE_NEWS_AUTHOR);
			ps.setLong(2, newsId);
			ps.setLong(1, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}
	}

	public List<Tag> getTagsByNews(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<Tag> tagList = new ArrayList<Tag>();
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_TAG_ID_FROM_NEWSTAG);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			long tagId = 0L;
			Tag tag = null;
			while (rs.next()) {
				tagId = rs.getLong(1);
				tag = tagDAO.findById(tagId);
				tagList.add(tag);
				
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		
		return tagList;
	}

	
	public void createNewsTag(long newsId, long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_NEWS_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}
	}

	public void deleteNewsTag(long newsId, long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}
	}

	public List<News> getNewsByTag(long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<News> newsList = new ArrayList<News>();
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.GET_NEWS_ID_FROM_NEWSTAG);
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			long newsId = 0L;
			News news = null;
			while (rs.next()) {
				newsId = rs.getLong(1);
				news = findById(newsId);
				newsList.add(news);
				
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		
		return newsList;
	}

	public void deleteNewsTagByNewsID(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS_TAG_BY_NEWSID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}

	}

	public void deleteNewsAuthorByNewsID(long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_NEWS_AUTHOR_BY_NEWSID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}

	}

}
