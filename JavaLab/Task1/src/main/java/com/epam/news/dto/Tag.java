package com.epam.news.dto;

import java.io.Serializable;

/**
 * Entity associated with table 'TAG'
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public class Tag implements Serializable{

	
	private static final long serialVersionUID = 951724986404804074L;
	
	private long tagId;
	private String tagName;

	public Tag() {

	}

	public Tag(long tag_id, String tag_name) {
		super();
		this.tagId = tag_id;
		this.tagName = tag_name;
	}

	public long getTagId() {
		return tagId;
	}

	public void setTagId(long tag_id) {
		this.tagId = tag_id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tag_name) {
		this.tagName = tag_name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nTag [tagId=");
		builder.append(tagId);
		builder.append(", tagName=");
		builder.append(tagName);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + tagId);
		result = prime * result
				+ ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Tag)) {
			return false;
		}
		Tag other = (Tag) obj;
		if (tagId != other.tagId) {
			return false;
		}
		if (tagName == null) {
			if (other.tagName != null) {
				return false;
			}
		} else if (!tagName.equals(other.tagName)) {
			return false;
		}
		return true;
	}

}
