package com.epam.news.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Entity associated with table 'NEWS'
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public class News implements Serializable{

	
	private static final long serialVersionUID = 4824471614925247885L;
	
	private long newsId;
	private String shortText;
	private String fullText;
	private String title;
	private Date creationDate;
	private Date modificationDate;

	public News() {

	}

	public News(long news_id, String short_text, String full_text,
			String title, Date creation_date, Date modification_date) {
		super();
		this.newsId = news_id;
		this.shortText = short_text;
		this.fullText = full_text;
		this.title = title;
		this.creationDate = creation_date;
		this.modificationDate = modification_date;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(long news_id) {
		this.newsId = news_id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String short_text) {
		this.shortText = short_text;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String full_text) {
		this.fullText = full_text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creation_date) {
		this.creationDate = creation_date;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modification_date) {
		this.modificationDate = modification_date;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nNews [newsId=");
		builder.append(newsId);
		builder.append(", shortText=");
		builder.append(shortText);
		builder.append(", fullText=");
		builder.append(fullText);
		builder.append(", title=");
		builder.append(title);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append(", modificationDate=");
		builder.append(modificationDate);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate
						.hashCode());
		result = (int) (prime * result + newsId);
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId != other.newsId)
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
