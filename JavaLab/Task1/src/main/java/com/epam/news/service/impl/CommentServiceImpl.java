package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.dto.Comment;
import com.epam.news.service.ICommentService;
import com.epam.news.service.exception.ServiceException;

@Service
public class CommentServiceImpl implements ICommentService {

	private Logger log = Logger.getLogger(CommentServiceImpl.class.getName());

	@Autowired
	private CommentDAOImpl commentDAO;

	public long create(Comment ob) throws ServiceException {
		long commentId = 0L;
		if (ob != null) {
			try {
				commentId = commentDAO.create(ob);
			} catch (DAOException e) {
				log.error("Can't create comment", e);
				throw new ServiceException("Can't create comment", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
		return commentId;
	}

	public void update(Comment ob) throws ServiceException {
		if (ob != null) {
			try {
				commentDAO.update(ob);
			} catch (DAOException e) {
				log.error("Can't update comment", e);
				throw new ServiceException("Can't update comment", e);
			}
		} else {
			log.error("Object is null");
			throw new ServiceException("Object is null");
		}
	}

	public void delete(long id) throws ServiceException {
		if (id > 0) {
			try {
				commentDAO.delete(id);
			} catch (DAOException e) {
				log.error("Can't delete comment", e);
				throw new ServiceException("Can't delete comment", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public void deleteByNewsID(long newsId) throws ServiceException {
		if (newsId > 0) {
			try {
				commentDAO.deleteByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't delete comments", e);
				throw new ServiceException("Can't delete comments", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
	}

	public Comment findById(long id) throws ServiceException {
		Comment comment = null;
		if (id > 0) {
			try {
				comment = commentDAO.findById(id);
			} catch (DAOException e) {
				log.error("Can't find comment", e);
				throw new ServiceException("Can't find comment", e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return comment;
	}

	public List<Comment> getAllRows() throws ServiceException {
		List<Comment> listComment = null;
		try {
			listComment = commentDAO.getAllRows();
		} catch (DAOException e) {
			log.error("Can't get list of comments", e);
			throw new ServiceException("Can't get list of comments", e);
		}
		return listComment;
	}

	public List<Comment> findByNewsID(long newsId)
			throws ServiceException {
		List<Comment> listComment = null;
		if (newsId > 0) {
			try {
				listComment = commentDAO.findByNewsID(newsId);
			} catch (DAOException e) {
				log.error("Can't get list of comments", e);
				throw new ServiceException("Can't get list of comments",
						e);
			}
		} else {
			log.error("ID <= 0");
			throw new ServiceException("ID <= 0");
		}
		return listComment;
	}

}
