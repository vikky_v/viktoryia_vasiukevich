package com.epam.news.service;

import java.util.List;

import com.epam.news.dto.Author;
import com.epam.news.dto.Comment;
import com.epam.news.dto.News;
import com.epam.news.dto.NewsVO;
import com.epam.news.dto.Tag;
import com.epam.news.service.exception.ServiceException;


/**
 * Interface for main service layer
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface INewsManagementService {

	/**
	 * Return list of data transfer objects {@link NewsVO.class}
	 * 
	 * @return List of News with Author, Tags, Coments
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             AuthorServiceException.class} or {@link
	 *             TagServiceException.class} or {@link
	 *             CommentServiceException.class} error occurs
	 */
	public List<NewsVO> showAllNews() throws ServiceException;

	/**
	 * Add News with Author and Tags
	 * 
	 * @param news
	 *            News that should be added
	 * @param author
	 *            Author of this News
	 * @param tag
	 *            list of Tags for this News
	 * @return ID of added News
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} error occurs
	 */
	public long addNews(News news, Author author, List<Tag> tag)
			throws ServiceException;

	/**
	 * Edit News with Author and Tags
	 * 
	 * @param news
	 *            News that should be edited
	 * @param author
	 *            Author of this News
	 * @param tag
	 *            list of Tags for this News
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} error occurs
	 */
	public void editNews(News news, Author author, List<Tag> tag)
			throws ServiceException;

	/**
	 * Delete News with Author, Tags and Comments
	 * 
	 * @param newsId
	 *            ID of News that should be deleted
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             CommentServiceException.class} error occurs
	 */
	public void deleteNewsTO(long newsId) throws ServiceException;

	/**
	 * Return single NewsVO (News with Author and Tags)
	 * 
	 * @param newsId
	 *            ID of News
	 * @return NewsVO (News with Author and Tags)
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             AuthorServiceException.class} or {@link
	 *             TagServiceException.class} or {@link
	 *             CommentServiceException.class} error occurs
	 */
	public NewsVO showSingleNewsVO(long newsId) throws ServiceException;

	/**
	 * Add Author if author with equals name doesn't exist
	 * 
	 * @param author
	 *            Author that should be added
	 * @return ID of added Author
	 * @throws ServiceException
	 *             when author with equals name exist or {@link
	 *             AuthorServiceException.class} error occurs
	 */
	public long addAuthor(Author author) throws ServiceException;

	
	/**
	 * Return list of News associated with specified Author
	 * 
	 * @param ob
	 *            Author which news should be found
	 * @return List of News
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             AuthorServiceException.class} error occurs
	 */
	public List<News> searchByAuthor(Author ob) throws ServiceException;

	/**
	 * Add Tag
	 * 
	 * @param tag
	 *            Tag that should be added
	 * @return ID of added Tag
	 * @throws ServiceException
	 *             when {@link TagServiceException.class} error occurs
	 */
	public long addTag(Tag tag) throws ServiceException;

	/**
	 * Add several Tags
	 * 
	 * @param tags
	 *            List of Tags that should be added
	 * @throws ServiceException
	 *             when {@link TagServiceException.class} error occurs
	 */
	public void addTags(List<Tag> tags) throws ServiceException;

	
	/**
	 * Return list of News associated at least with one of specified Tags
	 * 
	 * @param tags
	 *            list of Tags for which News should be found
	 * @return List of News
	 * @throws ServiceException
	 *             when {@link NewsServiceException.class} or {@link
	 *             TagServiceException.class} error occurs
	 */
	public List<News> searchByTags(List<Tag> tags) throws ServiceException;

    /**
	 * Add several Comments
	 * 
	 * @param comments
	 *            list of Comments that should be added
	 * @throws ServiceException
	 *             when {@link CommentServiceException.class} error occurs
	 */
	public void addComments(List<Comment> comments) throws ServiceException;

		
	/**
	 * Delete several Comments
	 * 
	 * @param commentIds
	 *            List of Comment ID's that should be deleted
	 * @throws ServiceException
	 *             when {@link CommentServiceException.class} error occurs
	 */
	public void deleteComments(List<Long> commentIds) throws ServiceException;

	/**
	 * Return list of all Comments
	 * 
	 * @return list of Comments
	 * @throws ServiceException
	 *             when {@link CommentServiceException.class} error occurs
	 */
	public List<Comment> showAllComments() throws ServiceException;

}
