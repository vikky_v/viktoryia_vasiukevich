package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.connection.DBUtil;
import com.epam.news.dao.IAuthorDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.dto.Author;

public class AuthorDAOImpl implements IAuthorDAO {

	@Autowired
	private DBUtil dbUtil;

	public long create(Author ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long authorId = 0L;
		String generatedColumns[] = { "author_id" };
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.INSERT_AUTHOR, generatedColumns);
			ps.setString(1, ob.getAuthorName());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();

			while (rs.next()) {
				authorId = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return authorId;
	}

	public void update(Author ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.UPDATE_AUTHOR);
			ps.setString(1, ob.getAuthorName());
			ps.setLong(2, ob.getAuthorId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}

	}

	public void delete(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.DELETE_AUTHOR);
			ps.setTimestamp(1, new Timestamp(new Date().getTime()));
			ps.setLong(2, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(con);
		}

	}

	public Author findById(long id) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.FIND_AUTHOR);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				Timestamp tmstmp = rs.getTimestamp(3);
				Date date = null;
				if (tmstmp != null) {
					date = new Date(tmstmp.getTime());
				}
				author = new Author(rs.getLong(1), rs.getString(2), date);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return author;
	}

	public long getAuthorId(Author ob) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		long authorId = 0L;
		try {
			con = dbUtil.getConnection();
			ps = con.prepareStatement(SQLQuery.FIND_AUTHOR_ID_BY_NAME);
			ps.setString(1, ob.getAuthorName());

			rs = ps.executeQuery();
			while (rs.next()) {
				authorId = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(ps);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return authorId;
	}

	public List<Author> getAllRows() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		List<Author> allAuthors = new ArrayList<Author>();
		try {
			con = dbUtil.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQLQuery.SELECT_ALL_AUTHORS);
			while (rs.next()) {
				Timestamp tmstmp = rs.getTimestamp(3);
				Date date = null;
				if (tmstmp != null) {
					date = new Date(tmstmp.getTime());
				}
				allAuthors
						.add(new Author(rs.getLong(1), rs.getString(2), date));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			dbUtil.close(st);
			dbUtil.close(rs);
			dbUtil.close(con);
		}
		return allAuthors;
	}

}
