package com.epam.news.dao;

import java.util.List;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.dto.Author;
import com.epam.news.dto.News;
import com.epam.news.dto.Tag;

/**
 * Extends {@link IDAO<News>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */

public interface INewsDAO extends IDAO<News> {

	/**
	 * Returns Author associated with given News ID
	 * 
	 * @param newsId
	 *            ID of News for which Author ID should be found
	 * @return Author
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public Author getAuthorByNews(long newsId) throws DAOException; 

	/**
	 * Returns list of News ID associated with given Authors ID
	 * 
	 * @param authorId
	 *            Author ID for which list of News ID should be found
	 * @return List of News
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNewsByAuthor(long authorId) throws DAOException; 

	/**
	 * Creates relation between News ID and Author ID
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Author ID
	 * @param authorId
	 *            Author ID that should be connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void createNewsAuthor(long newsId, long authorId)
			throws DAOException;

	/**
	 * Updates relation between News ID and Author ID. Connect with specified
	 * News ID new Author ID
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Author ID
	 * @param authorId
	 *            Author ID that should be connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void updateNewsAuthor(long newsId, long authorId)
			throws DAOException;

	/**
	 * Returns list of Tags associated with given News ID
	 * 
	 * @param newsId
	 *            News ID for which list of Tag ID should be found
	 * @return List of Tag
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<Tag> getTagsByNews(long newsId) throws DAOException; 

	/**
	 * Creates relation between News ID and Tag ID
	 * 
	 * @param newsId
	 *            News ID that should be connected with given Tag ID
	 * @param tagId
	 *            Tag ID that should be connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void createNewsTag(long newsId, long tagId) throws DAOException;

	/**
	 * Deletes relation between News ID and Tag ID
	 * 
	 * @param newsId
	 *            News ID that is connected with given Tag ID
	 * @param tagId
	 *            Tag ID that is connected with given News ID
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteNewsTag(long newsId, long tagId) throws DAOException;

	/**
	 * Returns list of News associated with given Tag ID
	 * 
	 * @param tagId
	 *            Tag ID for which list of News ID should be found
	 * @return List of News
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNewsByTag(long tagId) throws DAOException; 



	/**
	 * Deletes all relations between News ID and Tag ID by News ID
	 * 
	 * @param newsId
	 *            News ID for which should be deleted all relations
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteNewsTagByNewsID(long newsId) throws DAOException;

	/**
	 * Deletes all relations between News ID and Author ID by News ID
	 * 
	 * @param newsId
	 *            News ID for which should be deleted all relations
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public void deleteNewsAuthorByNewsID(long newsId) throws DAOException;
}
