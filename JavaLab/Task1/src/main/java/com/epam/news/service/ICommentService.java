package com.epam.news.service;

import java.util.List;

import com.epam.news.dto.Comment;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface for service layer for {@link ICommentDAO.class}
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface ICommentService {

	/**
	 * Check if Comment object is null and then invoke ICommentDAO
	 * create(Comment) method
	 * 
	 * @param ob
	 *            Comment object that should be created
	 * @return ID of created Comment
	 * @throws ServiceException
	 *             when Comment ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public long create(Comment ob) throws ServiceException;

	/**
	 * Check if Comment object is null and then invoke ICommentDAO
	 * update(Comment) method
	 * 
	 * @param ob
	 *            Comment object that should be updated
	 * @throws ServiceException
	 *             when Comment ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void update(Comment ob) throws ServiceException;

	/**
	 * Check if id==0 and then invoke ICommentDAO delete(id) method
	 * 
	 * @param id
	 *            ID of Comment that should be deleted
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void delete(long id) throws ServiceException;

	/**
	 * Check if newsId==0 and then invoke ICommentDAO deleteByNewsID(id) method
	 * 
	 * @param newsId
	 *            ID of News for which comments should be deleted
	 * @throws ServiceException
	 *             when newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void deleteByNewsID(long newsId) throws ServiceException;

	/**
	 * Check if id==0 and then invoke ICommentDAO findById(id) method
	 * 
	 * @param id
	 *            ID of Comment that should be found
	 * @return Comment object
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public Comment findById(long id) throws ServiceException;

	/**
	 * Invoke ICommentDAO getAllRows method
	 * 
	 * @return List of Comment objects
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<Comment> getAllRows() throws ServiceException;

	/**
	 * Check if newsId==0 and then invoke ICommentDAO findByNewsID(id) method
	 * 
	 * @param newsId
	 *            ID of News for which comments should be found
	 * @return List of Comment objects
	 * @throws ServiceException
	 *             when newsId=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public List<Comment> findByNewsID(long newsId)
			throws ServiceException;

}
