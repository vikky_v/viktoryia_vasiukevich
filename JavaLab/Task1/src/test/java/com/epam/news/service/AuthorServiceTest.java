package com.epam.news.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

import com.epam.news.dao.impl.AuthorDAOImpl;
import com.epam.news.dto.Author;
import com.epam.news.service.exception.ServiceException;
import com.epam.news.service.impl.AuthorServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock
	private AuthorDAOImpl authorDAO;

	@InjectMocks
	private AuthorServiceImpl authorService;

	private long id = 1L;
	private long id2 = 2L;
	private Author author = new Author();
	private Author author2 = new Author(2, "222", null);
	private Author authorNull = null;

	@Test
	public void createTest() throws Exception {

		when(authorDAO.create(author)).thenReturn(id);
		authorService.create(author);
		verify(authorDAO).create(author);
		assertEquals(id, authorService.create(author));

	}

	@Test(expected = ServiceException.class)
	public void createTestException() throws Exception {

		authorService.create(authorNull);

	}

	@Test
	public void updateTest() throws Exception {

		authorService.update(author);
		verify(authorDAO).update(author);

	}

	@Test(expected = ServiceException.class)
	public void updateTestException() throws Exception {

		authorService.update(authorNull);

	}

	@Test
	public void deleteTest() throws Exception {

		authorService.delete(1L);
		verify(authorDAO).delete(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteTestException() throws Exception {

		authorService.delete(-1L);

	}

	@Test
	public void findByIdTest() throws Exception {

		when(authorDAO.findById(id)).thenReturn(author);
		when(authorDAO.findById(id2)).thenReturn(author2);

		authorService.findById(id);
		verify(authorDAO).findById(id);

		authorService.findById(id2);
		verify(authorDAO).findById(id2);

		assertFalse(authorService.findById(id).equals(
				authorService.findById(id2)));

	}

	@Test(expected = ServiceException.class)
	public void findByIdTestException() throws Exception {

		authorService.findById(-1L);
	}

	@Test
	public void getAuthorIdTest() throws Exception {

		when(authorDAO.getAuthorId(author)).thenReturn(id);
		authorService.getAuthorId(author);
		verify(authorDAO).getAuthorId(author);
		assertEquals(id, authorService.getAuthorId(author));
	}

	@Test(expected = ServiceException.class)
	public void getAuthorIdTestException() throws Exception {
		authorService.getAuthorId(authorNull);
	}

	@Test
	public void getAllRowsTest() throws Exception {
		// when(authorDAO.getAllRows().size()).thenReturn(5);
		authorService.getAllRows();
		verify(authorDAO).getAllRows();
		// assertEquals(5,authorService.getAllRows().size());
	}

}
