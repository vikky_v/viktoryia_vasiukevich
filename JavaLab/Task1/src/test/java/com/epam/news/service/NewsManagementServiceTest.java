package com.epam.news.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.dto.Author;
import com.epam.news.dto.Comment;
import com.epam.news.dto.News;
import com.epam.news.dto.Tag;
import com.epam.news.service.exception.ServiceException;
import com.epam.news.service.impl.AuthorServiceImpl;
import com.epam.news.service.impl.CommentServiceImpl;
import com.epam.news.service.impl.NewsManagementServiceImpl;
import com.epam.news.service.impl.NewsServiceImpl;
import com.epam.news.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {

	@Mock
	private AuthorServiceImpl authorService;

	@Mock
	private CommentServiceImpl commentService;

	@Mock
	private NewsServiceImpl newsService;

	@Mock
	private TagServiceImpl tagService;

	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;

	@Test
	public void showAllNewsTest() throws Exception {

		List<News> listOfNews = new ArrayList<News>();
		News news1 = new News();
		news1.setNewsId(1L);
		listOfNews.add(news1);

		Author author = new Author(1L, "one", null);

		Tag tag = new Tag(5L, "t");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag);

		List<Comment> comments = new ArrayList<Comment>();
		comments.add(new Comment());

		when(newsService.getAllRows()).thenReturn(listOfNews);

		when(newsService.getAuthorByNews(1L)).thenReturn(author);
		when(newsService.getTagsByNews(1L)).thenReturn(tags);
		when(commentService.findByNewsID(1L)).thenReturn(comments);

		newsManagementService.showAllNews();

		verify(newsService).getAllRows();
		verify(newsService).getAuthorByNews(listOfNews.get(0).getNewsId());
		verify(newsService).getTagsByNews(1L);
		verify(commentService).findByNewsID(1L);
	}

	@Test
	public void addNewsTest() throws Exception {
		News news1 = new News();
		Author author = new Author(1L, "one", null);
		Tag tag = new Tag(5L, "t");
		Tag tag2 = new Tag(7L, "ttt");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag);
		tags.add(tag2);

		when(newsService.create(news1)).thenReturn(10L);

		newsManagementService.addNews(news1, author, tags);

		verify(newsService).createNewsAuthor(10L, 1L);
		verify(newsService).createNewsTag(10L, 5L);
		verify(newsService).createNewsTag(10L, 7L);
	}

	@Test
	public void editNewsTest() throws Exception {
		News news1 = new News();
		news1.setNewsId(40L);
		Author author = new Author(1L, "one", null);
		Tag tag = new Tag(5L, "t");
		Tag tag2 = new Tag(7L, "ttt");
		Tag tag3 = new Tag(8L, "ttttt");
		List<Tag> tagsBefore = new ArrayList<Tag>();
		tagsBefore.add(tag);
		tagsBefore.add(tag2);
		
		List<Tag> tagsAfter = new ArrayList<Tag>();
		tagsAfter.add(tag2);
		tagsAfter.add(tag3);
		

		when(newsService.getTagsByNews(40L)).thenReturn(tagsBefore);

		newsManagementService.editNews(news1, author, tagsAfter);

		verify(newsService).update(news1);
		verify(newsService).updateNewsAuthor(40L, 1L);
		verify(newsService).deleteNewsTagByNewsID(40L);
		verify(newsService).createNewsTag(40L, 7L);
		verify(newsService).createNewsTag(40L, 8L);
		

	}

	@Test
	public void deleteNewsTOTest() throws Exception {

		newsManagementService.deleteNewsTO(1L);
		verify(newsService).deleteNewsTagByNewsID(1L);
		verify(newsService).deleteNewsAuthorByNewsID(1L);
		verify(commentService).deleteByNewsID(1L);
		verify(newsService).delete(1L);
	}

	@Test
	public void showSingleNewsVOTest() throws Exception {

		News news = new News();
		news.setNewsId(1L);

		Author author = new Author(5L, "a", null);

		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(7L,"q"));
		tags.add(new Tag(8L,"q8"));

		List<Comment> comments = new ArrayList<Comment>();
		comments.add(new Comment());

		when(newsService.findById(1L)).thenReturn(news);
		when(newsService.getAuthorByNews(1L)).thenReturn(author);
		when(newsService.getTagsByNews(1L)).thenReturn(tags);
		
		newsManagementService.showSingleNewsVO(1L);
		verify(newsService).findById(1L);
		verify(newsService).getAuthorByNews(1L);
		verify(newsService).getTagsByNews(1L);
		verify(commentService).findByNewsID(1L);
	}

	@Test
	public void addAuthorTest() throws Exception {

		Author a1 = new Author(1L, "qwert", null);
		Author a2 = new Author(2L, "asdfg", null);

		when(authorService.getAuthorId(a1)).thenReturn(1L);
		try {
			newsManagementService.addAuthor(a1);
			fail("Exception is required");
		} catch (ServiceException e) {
			//catch exception
		}
		verify(authorService, never()).create(a1);

		when(authorService.getAuthorId(a1)).thenReturn(0L);
		when(authorService.create(a2)).thenReturn(2L);
		newsManagementService.addAuthor(a2);
		verify(authorService).create(a2);
		assertEquals(2L, newsManagementService.addAuthor(a2));
	}

	@Test
	public void searchByAuthorTest() throws Exception {
		Author a1 = new Author(1L, "qwert", null);
		News n1 = new News();
		n1.setNewsId(2L);
		News n2 = new News();
		n2.setNewsId(10L);
		List<News> news = new ArrayList<News>();
		news.add(n1);
		news.add(n2);
		
		when(authorService.getAuthorId(a1)).thenReturn(1L);
		when(newsService.getNewsByAuthor(1L)).thenReturn(news);
		
		newsManagementService.searchByAuthor(a1);
		verify(authorService).getAuthorId(a1);
		verify(newsService).getNewsByAuthor(1L);
		
	}

	@Test
	public void addTagTest() throws Exception {

		Tag t1 = new Tag(1L, "get");
		Tag t2 = new Tag(2L, "post");

		when(tagService.findByName(t1.getTagName())).thenReturn(t1);
		try {
			newsManagementService.addTag(t1);
			fail("Exception is required");
		} catch (ServiceException e) {
			//catch exception
		}
		verify(tagService, never()).create(t1);

		when(tagService.findByName(t2.getTagName())).thenReturn(null);
		when(tagService.create(t2)).thenReturn(2L);
		newsManagementService.addTag(t2);
		verify(tagService).create(t2);
		assertEquals(2L, newsManagementService.addTag(t2));
	}

	@Test
	public void addTagsTest() throws Exception {

		Tag t1 = new Tag(1L, "get");
		Tag t2 = new Tag(2L, "post");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(t1);
		tags.add(t2);

		when(tagService.findByName(t1.getTagName())).thenReturn(null);
		when(tagService.create(t1)).thenReturn(1L);
		when(tagService.findByName(t2.getTagName())).thenReturn(t2);

		try {
			newsManagementService.addTags(tags);
			fail("Exception is required");
		} catch (ServiceException e) {
			//catch exception
		}

		verify(tagService).create(t1);
		verify(tagService, never()).create(t2);
	}

	@Test
	public void searchByTagsTest() throws Exception {
		Tag t1 = new Tag(1L, "get");
		Tag t2 = new Tag(2L, "post");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(t1);
		tags.add(t2);

		News n1 = new News();
		n1.setNewsId(9L);
		News n2 = new News();
		n2.setNewsId(10L);
		List<News> news = new ArrayList<News>();
		news.add(n1);
		news.add(n2);

		when(tagService.findByName(t1.getTagName())).thenReturn(t1);
		when(tagService.findByName(t2.getTagName())).thenReturn(t2);
		when(newsService.getNewsByTag(1L)).thenReturn(news);
		when(newsService.getNewsByTag(2L)).thenReturn(new ArrayList<News>());
		
		newsManagementService.searchByTags(tags);
		verify(tagService).findByName(t1.getTagName());
		verify(newsService).getNewsByTag(1L);
		
		verify(tagService).findByName(t2.getTagName());
		verify(newsService).getNewsByTag(2L);

	}

	@Test
	public void addCommentsTest() throws Exception {
		Comment com1 = new Comment();
		com1.setCommentId(1L);
		Comment com2 = new Comment();
		com2.setCommentId(2L);
		List<Comment> coms = new ArrayList<Comment>();
		coms.add(com1);
		coms.add(com2);

		newsManagementService.addComments(coms);
		verify(commentService).create(com1);
		verify(commentService).create(com2);
	}


	@Test
	public void deleteCommentsTest() throws Exception {
		List<Long> comId = new ArrayList<Long>();
		comId.add(11L);
		comId.add(12L);
		newsManagementService.deleteComments(comId);
		verify(commentService).delete(11L);
		verify(commentService).delete(12L);
	}

	@Test
	public void showAllCommentsTest() throws Exception {

		newsManagementService.showAllComments();
		verify(commentService).getAllRows();
	}
}
