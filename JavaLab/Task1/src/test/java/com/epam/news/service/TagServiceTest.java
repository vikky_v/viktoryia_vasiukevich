package com.epam.news.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

import com.epam.news.dao.impl.TagDAOImpl;
import com.epam.news.dto.Tag;
import com.epam.news.service.exception.ServiceException;
import com.epam.news.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private TagDAOImpl tagDAO;

	@InjectMocks
	private TagServiceImpl tagService;

	private long id = 1L;
	private long id2 = 2L;
	private Tag tag = new Tag();
	private Tag tag2 = new Tag(2, "Tag2");
	private Tag tagNull = null;

	@Test
	public void createTest() throws Exception {

		when(tagDAO.create(tag)).thenReturn(id);
		tagService.create(tag);
		verify(tagDAO).create(tag);
		assertEquals(id, tagService.create(tag));
	}

	@Test(expected = ServiceException.class)
	public void createTestException() throws Exception {

		tagService.create(tagNull);

	}

	@Test
	public void updateTest() throws Exception {

		tagService.update(tag);
		verify(tagDAO).update(tag);

	}

	@Test(expected = ServiceException.class)
	public void updateTestException() throws Exception {

		tagService.update(tagNull);

	}

	@Test
	public void deleteTest() throws Exception {

		tagService.delete(1L);
		verify(tagDAO).delete(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteTestException() throws Exception {

		tagService.delete(0L);

	}

	@Test
	public void findByIdTest() throws Exception {

		when(tagDAO.findById(id)).thenReturn(tag);
		when(tagDAO.findById(id2)).thenReturn(tag2);

		tagService.findById(id);
		verify(tagDAO).findById(id);
		tagService.findById(id2);
		verify(tagDAO).findById(id2);

		assertFalse(tagService.findById(id).equals(tagService.findById(id2)));
	}

	@Test(expected = ServiceException.class)
	public void findByIdTestException() throws Exception {

		tagService.findById(-1L);

	}

	@Test
	public void findByNameTest() throws Exception {

		when(tagDAO.findByName("Tag2")).thenReturn(tag2);
		tagService.findByName("Tag2");
		verify(tagDAO).findByName("Tag2");
		assertEquals(tag2, tagService.findByName("Tag2"));
	}

	@Test(expected = ServiceException.class)
	public void findByNameTestException() throws Exception {

		tagService.findByName(null);

	}

	@Test
	public void getAllRowsTest() throws Exception {

		tagService.getAllRows();
		verify(tagDAO).getAllRows();

	}
}
