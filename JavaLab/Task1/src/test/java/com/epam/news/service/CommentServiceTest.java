package com.epam.news.service;

import java.sql.Timestamp;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.dto.Comment;
import com.epam.news.service.exception.ServiceException;
import com.epam.news.service.impl.CommentServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock
	private CommentDAOImpl commentDAO;

	@InjectMocks
	private CommentServiceImpl commentService;

	private long id = 1L;
	private long id2 = 2L;
	private Comment comment = new Comment();
	private Comment comment2 = new Comment(2L, "2", new Timestamp(
			new Date().getTime()), 1L);
	private Comment commentNull = null;

	@Test
	public void createTest() throws Exception {

		when(commentDAO.create(comment)).thenReturn(id);

		commentService.create(comment);
		verify(commentDAO).create(comment);
		assertEquals(id, commentService.create(comment));

	}

	@Test(expected = ServiceException.class)
	public void createTestException() throws Exception {

		commentService.create(commentNull);

	}

	@Test
	public void updateTest() throws Exception {

		commentService.update(comment);
		verify(commentDAO).update(comment);

	}

	@Test(expected = ServiceException.class)
	public void updateTestException() throws Exception {

		commentService.update(commentNull);

	}

	@Test
	public void deleteTest() throws Exception {

		commentService.delete(1L);
		verify(commentDAO).delete(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteTestException() throws Exception {

		commentService.delete(0L);

	}

	@Test
	public void deleteByNewsIDTest() throws Exception {

		commentService.deleteByNewsID(1L);
		verify(commentDAO).deleteByNewsID(1L);

	}

	@Test(expected = ServiceException.class)
	public void deleteByNewsIDTestException() throws Exception {

		commentService.deleteByNewsID(-1L);
	
	}

	@Test
	public void findByIDTest() throws Exception {

		when(commentDAO.findById(id)).thenReturn(comment);
		when(commentDAO.findById(id2)).thenReturn(comment2);

		commentService.findById(id);
		verify(commentDAO).findById(id);

		commentService.findById(id2);
		verify(commentDAO).findById(id2);

		assertFalse(commentService.findById(id).equals(
				commentService.findById(id2)));
	}

	@Test(expected = ServiceException.class)
	public void findByIDTestException() throws Exception {

		commentService.findById(0L);

	}

	@Test
	public void getAllRowsTest() throws Exception {

		commentService.getAllRows();
		verify(commentDAO).getAllRows();

	}

	@Test
	public void findByNewsIdTest() throws Exception {

		commentService.findByNewsID(1L);
		verify(commentDAO).findByNewsID(1L);

	}

	@Test(expected = ServiceException.class)
	public void findByNewsIdTestException() throws Exception {

		commentService.findByNewsID(-1L);

	}

}
