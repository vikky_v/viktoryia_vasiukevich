package com.epam.news.dao.impl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.epam.news.dto.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/resources/DAOTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class TagDAOTest {

	@Autowired
	private TagDAOImpl tagDAO;

	@Test
	@DatabaseSetup("classpath:/resources/setup/TagDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/TagDS.xml", type = DatabaseOperation.DELETE)
	public void testCreate() throws Exception {

		Tag t = new Tag();
		t.setTagName("qwerty");
		long id = tagDAO.create(t);

		List<Tag> tagList = tagDAO.getAllRows();

		assertEquals(4, tagList.size());
		// assertTrue(tagList.contains((new Tag(4, "qwerty"))));
		assertTrue(id > 0);
	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/TagDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/TagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {

		Tag tag = new Tag(1, "one1");

		tagDAO.update(tag);

		Tag tag2 = tagDAO.findById(1);

		assertEquals(tag, tag2);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/TagDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/TagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testDelete() throws Exception {

		tagDAO.delete(1);
		List<Tag> tagList = tagDAO.getAllRows();

		assertEquals(2, tagList.size());

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/TagDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/TagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testFindById() throws Exception {

		Tag tag = tagDAO.findById(2);
		Tag tag2 = new Tag(2, "movie");
		assertEquals(tag, tag2);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/TagDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/TagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testFindByName() throws Exception {

		Tag tag = tagDAO.findByName("movie");
		Tag tag2 = new Tag(2, "movie");
		assertEquals(tag, tag2);

	}

	@Test
	@DatabaseSetup("classpath:/resources/setup/TagDS.xml")
	@DatabaseTearDown(value ="classpath:/resources/setup/TagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAllRows() throws Exception {

		List<Tag> tagList = tagDAO.getAllRows();

		assertEquals(3, tagList.size());
		assertTrue(tagList.contains(new Tag(1, "tag")));
		assertTrue(tagList.contains(new Tag(2, "movie")));
		assertTrue(tagList.contains(new Tag(3, "news")));
	}

}
