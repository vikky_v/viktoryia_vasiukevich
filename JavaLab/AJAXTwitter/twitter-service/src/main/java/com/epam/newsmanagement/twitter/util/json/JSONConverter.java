package com.epam.newsmanagement.twitter.util.json;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JSONConverter {

	@SuppressWarnings("unchecked")
	public static String convertTweetsToNews(String tweetsJSON)
			throws JsonProcessingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode nodes = mapper.readTree(tweetsJSON);
		
		
		JsonNodeFactory factory = JsonNodeFactory.instance;
		ArrayNode list = factory.arrayNode();
		
		

		//JSONArray list = new JSONArray();
		for (JsonNode node : nodes) {

			ObjectNode news = factory.objectNode();
			//JSONObject news = new JSONObject();
			news.set("newsId", node.get("id"));
			news.put("title", node.get("text").asText().split(" ")[0]);
			String shortText = node.get("text").asText();
			if (shortText.length() > 100) {
				news.put("shortText", shortText.substring(0, 99));
			} else {
				news.put("shortText", shortText);
			}
			news.set("fullText", node.get("text"));

			SimpleDateFormat sdf = new SimpleDateFormat(
					"EEE MMM d HH:mm:ss Z yyyy");
			SimpleDateFormat sdfNews = new SimpleDateFormat("MM/dd/yyyy");
			Date date;
			try {
				date = sdf.parse(node.get("created_at").asText());
				news.put("creationDate", sdfNews.format(date));
				news.put("modificationDate", sdfNews.format(date));
			} catch (ParseException e) {
				date = new Date();
			}

			// news.put("creationDate",new Date().toString());
			// news.put("modificationDate",node.get("created_at"));

			//JSONObject author = new JSONObject();
			ObjectNode author = factory.objectNode(); 
			
			JsonNode user = node.get("user");

			author.set("authorId", user.get("id"));
			author.set("authorName", user.get("name"));

			news.set("author", author);

			//JSONArray tags = new JSONArray();
			ArrayNode tags = factory.arrayNode();
			JsonNode hashtags = node.get("entities").get("hashtags");
			for (JsonNode hashtag : hashtags) {
				//JSONObject tag = new JSONObject();
				ObjectNode tag = factory.objectNode(); 
				tag.put("tagName", hashtag.get("text").asText());
				tags.add(tag);
			}

			news.set("tags", tags);
			list.add(news);

		}

		return list.toString();

	}

}
