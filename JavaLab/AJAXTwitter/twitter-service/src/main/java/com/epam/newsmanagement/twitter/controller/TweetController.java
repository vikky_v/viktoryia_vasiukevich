package com.epam.newsmanagement.twitter.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.epam.newsmanagement.twitter.util.Configuration;
import com.epam.newsmanagement.twitter.util.json.JSONConverter;

@RestController
public class TweetController {

	@Autowired
	Configuration configuration;

	@RequestMapping(value = "/", method = RequestMethod.GET, headers = "Accept=application/json")
	public String index() {

		return new String("Hello!");
	}

	@RequestMapping(value = "/home_timeline", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public String getHomeTimeline() throws InvalidKeyException,
			NoSuchAlgorithmException, MalformedURLException, IOException {
		final String GET_HOME_TIMELINE = "https://api.twitter.com/1.1/statuses/home_timeline.json";
		final String GET_HOME_TIMELINE_COUNT = "https://api.twitter.com/1.1/statuses/home_timeline.json?count=4";
		final String METHOD = "GET";

		
		LinkedHashMap<String, String> data = configuration.getData();
		TreeMap<String, String> dataWithParams = new TreeMap<>(data);
		dataWithParams.put("count", "4");

		dataWithParams = (TreeMap<String, String>) configuration.setSignature(dataWithParams,
				GET_HOME_TIMELINE, METHOD);

		String header = configuration.createOAuthHeader(dataWithParams);

		HttpsURLConnection connection = configuration.createConnection(
				GET_HOME_TIMELINE_COUNT, METHOD, header, false);

		
		String newsJSON = JSONConverter.convertTweetsToNews(readResponse(connection));
		
		System.out.println("newsJSON "+newsJSON);
		return newsJSON;

	}
	
	@RequestMapping(value = "/home_timeline/{sinceId}", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public String getHomeTimelineNext(@PathVariable Long sinceId) throws InvalidKeyException,
			NoSuchAlgorithmException, MalformedURLException, IOException {
		final String GET_HOME_TIMELINE = "https://api.twitter.com/1.1/statuses/home_timeline.json";
		final String GET_HOME_TIMELINE_SINCE_ID = "https://api.twitter.com/1.1/statuses/home_timeline.json?since_id="+sinceId;
		final String METHOD = "GET";

		
		LinkedHashMap<String, String> data = configuration.getData();
		TreeMap<String, String> dataWithParams = new TreeMap<>(data);
		dataWithParams.put("since_id", String.valueOf(sinceId));

		dataWithParams = (TreeMap<String, String>) configuration.setSignature(dataWithParams,
				GET_HOME_TIMELINE, METHOD);

		String header = configuration.createOAuthHeader(dataWithParams);

		HttpsURLConnection connection = configuration.createConnection(
				GET_HOME_TIMELINE_SINCE_ID, METHOD, header, false);

		
		String newsJSON = JSONConverter.convertTweetsToNews(readResponse(connection));
		
		System.out.println("newsJSON "+newsJSON);
		return newsJSON;

	}
	
	@RequestMapping(value = "/home_timeline/since/{sinceId}/count/{count}", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public String getRestrictedSinceHomeTimeline(@PathVariable String sinceId, @PathVariable String count) throws InvalidKeyException,
			NoSuchAlgorithmException, MalformedURLException, IOException {
		final String GET_HOME_TIMELINE = "https://api.twitter.com/1.1/statuses/home_timeline.json";
		final String GET_HOME_TIMELINE_SINCE_ID_COUNT = "https://api.twitter.com/1.1/statuses/home_timeline.json?since_id="+sinceId+"&count="+count;
		final String METHOD = "GET";

		
		LinkedHashMap<String, String> data = configuration.getData();
		TreeMap<String, String> dataWithParams = new TreeMap<>(data);
		dataWithParams.put("since_id", sinceId);
		dataWithParams.put("count", count);
		

		dataWithParams = (TreeMap<String, String>) configuration.setSignature(dataWithParams,
				GET_HOME_TIMELINE, METHOD);

		String header = configuration.createOAuthHeader(dataWithParams);

		HttpsURLConnection connection = configuration.createConnection(
				GET_HOME_TIMELINE_SINCE_ID_COUNT, METHOD, header, false);

		
		String newsJSON = JSONConverter.convertTweetsToNews(readResponse(connection));
		
		System.out.println("newsJSON "+newsJSON);
		return newsJSON;

	}
	
	@RequestMapping(value = "/home_timeline/max/{maxId}/count/{count}", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public String getRestrictedMaxHomeTimeline(@PathVariable String maxId, @PathVariable String count) throws InvalidKeyException,
			NoSuchAlgorithmException, MalformedURLException, IOException {
		final String GET_HOME_TIMELINE = "https://api.twitter.com/1.1/statuses/home_timeline.json";
		final String GET_HOME_TIMELINE_SINCE_ID_COUNT = "https://api.twitter.com/1.1/statuses/home_timeline.json?max_id="+maxId+"&count="+count;
		final String METHOD = "GET";

		
		LinkedHashMap<String, String> data = configuration.getData();
		TreeMap<String, String> dataWithParams = new TreeMap<>(data);
		dataWithParams.put("max_id", maxId);
		dataWithParams.put("count", count);
		

		dataWithParams = (TreeMap<String, String>) configuration.setSignature(dataWithParams,
				GET_HOME_TIMELINE, METHOD);

		String header = configuration.createOAuthHeader(dataWithParams);

		HttpsURLConnection connection = configuration.createConnection(
				GET_HOME_TIMELINE_SINCE_ID_COUNT, METHOD, header, false);

		
		String newsJSON = JSONConverter.convertTweetsToNews(readResponse(connection));
		
		System.out.println("newsJSON "+newsJSON);
		return newsJSON;

	}
	
	@RequestMapping(value = "/user_timeline", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public String getUserTimeline() throws InvalidKeyException,
			NoSuchAlgorithmException, MalformedURLException, IOException {
		final String GET_USER_TIMELINE = "https://api.twitter.com/1.1/statuses/user_timeline.json";
		final String GET_USER_TIMELINE_SCREEN_NAME = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=vikky_v";
		final String METHOD = "GET";

		
		LinkedHashMap<String, String> data = configuration.getData();
		TreeMap<String, String> dataWithParams = new TreeMap<>(data);
		dataWithParams.put("screen_name", "vikky_v");

		dataWithParams = (TreeMap<String, String>) configuration.setSignature(dataWithParams,
				GET_USER_TIMELINE, METHOD);

		String header = configuration.createOAuthHeader(dataWithParams);

		HttpsURLConnection connection = configuration.createConnection(
				GET_USER_TIMELINE_SCREEN_NAME, METHOD, header, false);

		
		String newsJSON = JSONConverter.convertTweetsToNews(readResponse(connection));
		
		System.out.println("newsJSON "+newsJSON);
		return newsJSON;

	}

	private boolean writeRequest(HttpsURLConnection connection, String textBody) {
		try {
			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(
					connection.getOutputStream()));
			if (textBody != null) {
				wr.write(textBody);
			}
			wr.flush();
			wr.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	private String readResponse(HttpsURLConnection connection)
			throws IOException {

		StringBuffer str = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		String line = "";
		while ((line = br.readLine()) != null) {
			str.append(line);
		}
		return str.toString();

	}

}
