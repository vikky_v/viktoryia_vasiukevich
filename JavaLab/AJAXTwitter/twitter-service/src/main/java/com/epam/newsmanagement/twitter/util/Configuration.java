package com.epam.newsmanagement.twitter.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Component;

import com.sun.jersey.core.util.Base64;

@Component
public class Configuration {

	private final String APP_CONSUMER_KEY = "nnJqFAVs4xvwWocKYTLt341gK";
	private final String APP_CONSUMER_SECRET = "0YdUTioOZwajoTD0sCbquqmh5BPUigGzBr0bAVgSsAdxkbZOBj";
	private final String APP_TOKEN = "109923131-Aiku1Vd0DGx8jbgnPmthxAxuqFC9hAdBs3j5qvvt";
	private final String APP_TOKEN_SECRET = "rLchxVwkt0pwOVE4kBxPz4a4Ee6gaDx26OCm2bLLxwtpD";

	private final String CONSUMER_KEY = "oauth_consumer_key";
	private final String NONCE = "oauth_nonce";
	private final String SIGNATURE = "oauth_signature";
	private final String SIGNATURE_METHOD = "oauth_signature_method";
	private final String TIMESTAMP = "oauth_timestamp";
	private final String TOKEN = "oauth_token";
	private final String VERSION = "oauth_version";

	private final String CHARSET = "UTF-8";
	private final String ALGORYTHM = "HmacSHA1";

	private  void configure(LinkedHashMap<String, String> data) {

		data.put(CONSUMER_KEY, APP_CONSUMER_KEY);
		data.put(NONCE, String.valueOf((int) (Math.random() * 1000000000)));
		data.put(SIGNATURE, "");
		data.put(SIGNATURE_METHOD, "HMAC-SHA1");
		data.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000));
		data.put(TOKEN, APP_TOKEN);
		data.put(VERSION, "1.0");

	}

	public  LinkedHashMap<String, String> getData() {

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		configure(data);

		return data;
	}

	private String getSignature(Map<String, String> data, String url,
			String method) throws UnsupportedEncodingException,
			NoSuchAlgorithmException, InvalidKeyException {

		String signature = method.toUpperCase() + "&"
				+ URLEncoder.encode(url, CHARSET) + "&";

		for (Entry<String, String> entry : data.entrySet()) {
			if (!entry.getKey().equals(SIGNATURE)) {
				signature += (URLEncoder.encode(entry.getKey(), CHARSET)
						+ "%3D" + URLEncoder.encode(entry.getValue(), CHARSET) + "%26");
			}
		}
		
//		if(parameters!=null){
//		String[] paramKeyValue = parameters.split("=");
//		signature+=(URLEncoder.encode(paramKeyValue[0], CHARSET)
//				+ "%3D" + URLEncoder.encode(paramKeyValue[1], CHARSET) + "%26");
//		}
		
		signature = signature.substring(0, signature.length() - 3);

		Mac m = Mac.getInstance(ALGORYTHM);

		m.init(new SecretKeySpec((APP_CONSUMER_SECRET + "&" + APP_TOKEN_SECRET)
				.getBytes(), ALGORYTHM));

		byte[] macResult = m.doFinal(signature.getBytes());
		String result = new String(Base64.encode(macResult));

		return URLEncoder.encode(result, CHARSET);
	}

	public Map<String, String> setSignature(Map<String, String> data,
			String url, String method)
			throws InvalidKeyException, UnsupportedEncodingException,
			NoSuchAlgorithmException {

		String signature = getSignature(data, url, method);

		data.put(SIGNATURE, signature);
		return data;

	}

	public String createOAuthHeader(Map<String, String> data) {

		String header = "OAuth ";

		for (Entry<String, String> entry : data.entrySet()) {
			header += (entry.getKey() + "=\"" + entry.getValue() + "\", ");
		}

		// cut off last appended comma
		header = header.substring(0, header.length() - 2);

		return header;
	}

	public HttpsURLConnection createConnection(String url, String method,
			String header, boolean addContentType)
			throws MalformedURLException, IOException {

		HttpsURLConnection connection = (HttpsURLConnection) new URL(url)
				.openConnection();
		connection.setDoInput(Boolean.valueOf(true));
		if (method.equals("POST")) {
			connection.setDoOutput(Boolean.valueOf(true));
		}
		connection.setRequestMethod(method);
		if (addContentType) {
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
		}
		connection.setRequestProperty("Authorization", header);

		return connection;
	}

}
