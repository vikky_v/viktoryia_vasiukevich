<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<head>
<title><spring:message code="menu.list" /></title>


<spring:url value="/resources/js/listNews.js" var="listNews" />
<script type="text/javascript" src="${listNews}"></script>
<spring:url value="/resources/js/handlebars-v4.0.2.js" var="handlebars" />
<script type="text/javascript" src="${handlebars}"></script>

</head>

<div class="body">

	<div id="container"></div>
	<script id="templ" type="text/x-handlebars-template">

<input type="hidden" id="lastNews" value="{{news.0.newsId}}"/>
<input type="hidden" id="firstNews" value="{{news.3.newsId}}"/>

    {{#each news}}
						<div class="news">
						<div class="title">
							<b><a href="news?newsId={{newsId}}"><c:out value="{{title}}"></c:out></a>&nbsp;(by
								<c:out value="{{author.authorName}}"></c:out>)</b>
						</div>

						<div class="date">
								<c:out value="{{modificationDate}}"></c:out>

						</div>
						<div class="text"><c:out value="{{shortText}}"></c:out></div>
						<div class="bottomPanel">
							<span class="tags"> 

								{{#each tags}}
								<c:out value="{{tagName}},"></c:out>
						 		{{/each}}


							</span> 

						</div>
					</div>
       
    {{/each}}
	</script>	

	<div class="navigation">
	<button id="next">NEXT</button>
	<button id="prev">PREV</button>	
	</div>
	
</div>