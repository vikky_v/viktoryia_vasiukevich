<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<head>
<title><spring:message code="menu.addnews" /></title>

<%-- <spring:url value="/resources/js/jquery-1.11.3.js" var="jquery" /> --%>
<%-- <script type="text/javascript" src="${jquery}"></script> --%>

<spring:url value="/resources/js/addUpdateNews.js" var="add" />
<script type="text/javascript" src="${add}"></script>


</head>


<div class="body">

	<form:form commandName="News" method="POST" action="save">
		<form:hidden path="newsId" value="" />
		<div>
			<form:hidden path="version" value="" />
			<form:errors path="version" cssClass="error" />
			<div class="field">

				<span class="newsForm"><spring:message code="message.title" /></span>
				<form:input path="title" size="65" maxlength="30"
					required="required" />

				<form:errors path="title" cssClass="error" />
			</div>

			<div class="field">
				<span class="newsForm"><spring:message code="message.date" /></span>

				<fmt:setBundle basename="message" var="bundle" />
				<fmt:message bundle="${bundle}" key="date.pattern" var="pattern" />
				<jsp:useBean id="now" class="java.util.Date" />
				<fmt:formatDate pattern="${pattern}" value="${now}" var="modDate" />
				<input type="hidden" name="pattern" value="${pattern}"> <input
					type="text" name="modificDate" value="${modDate}"
					required="required"
					pattern="((0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/(19|20)\d\d)|((0[1-9]|1[012])/(0[1-9]|[12][0-9]|3[01])/(19|20)\d\d)"
					placeholder="${pattern}" />

				<form:errors path="modificationDate" cssClass="error" />

				<fmt:formatDate pattern="${pattern}" value="${News.creationDate}"
					var="crDate" />
				<c:if test="${empty crDate}">
					<input type="hidden" name="creationDate" value="01/01/1970">
				</c:if>
				<c:if test="${not empty crDate}">
					<input type="hidden" name="creationDate" value="${crDate}">
				</c:if>

			</div>

			<div class="field">

				<span class="newsForm"><spring:message code="message.brief" /></span>
				<form:textarea path="shortText" rows="3" cols="50" maxlength="100"
					required="required" />

				<form:errors path="shortText" cssClass="error" />
			</div>

			<div class="field">

				<span class="newsForm"><spring:message code="message.content" /></span>
				<form:textarea path="fullText" rows="6" cols="50" maxlength="2000"
					required="required" />
				<br>
				<form:errors path="fullText" cssClass="error" />
			</div>



			<div class="filterNews">

				<form:select path="author.authorId" id="author" required="required">
					<form:option value="0" selected="selected" disabled="true">
						<spring:message code="select.author" />
					</form:option>
					<form:options items="${authors}" itemValue="authorId"
						itemLabel="authorName" htmlEscape="true" />

				</form:select>

				<div class="multiselect" id="multiselect">
					<div class="selectBox">

						<select name="tags">
							<option id="selectedTags" selected disabled><spring:message
									code="select.tags" /></option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes">
						<label><input type="checkbox" name="tagsId" value="0"
							id="default" disabled="disabled" /><span><spring:message
									code="select.tags" /></span></label>
						<c:forEach items="${tags}" var="tag">
							<c:if test="${News.tags.contains(tag)}">
								<label><input type="checkbox" name="tagsId"
									value="${tag.tagId}" checked="checked" /><span><c:out
											value="${tag.tagName}"></c:out></span></label>
							</c:if>
							<c:if test="${!News.tags.contains(tag)}">
								<label><input type="checkbox" name="tagsId"
									value="${tag.tagId}" /><span><c:out
											value="${tag.tagName}"></c:out></span></label>
							</c:if>
						</c:forEach>
					</div>
				</div>
			</div>
			<br>
			<form:errors path="author.authorId" cssClass="error" />


			<div class="saveButton">
				<input type="submit" class="saveButton"
					value="<spring:message code="button.savenews"/>" />
			</div>
		</div>
	</form:form>

	<form method="POST" action="add/from/twitter">
		<div class="saveButton">
			<input type="submit" class="saveButton"
				value="<spring:message code="button.savenews"/>" />
		</div>
	</form>
</div>