<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="header">
	<spring:message code="header.title" />
		
	<div class="admin">

		<form method="POST">
			<spring:message code="message.hello" />
			<c:set var="name" value="${userName}" scope="session" />
			<c:out value="${name}" />
			&nbsp; &nbsp; <input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<button formaction="<c:url value="/j_spring_security_logout"/>">
				<spring:message code="button.logout" />
			</button>
		</form>
	</div>
	<div class="localeButton">

		<a
			href="
		
		<c:url value="${requestScope['javax.servlet.forward.servlet_path']}">
		<c:set var="params" value="${requestScope['javax.servlet.forward.query_string']}"/>
		
		<c:set var="string" value="${fn:replace(params, 
                                '&', '=')}" />
        <c:set var="arrayofparams" value="${fn:split(string,'=')}"/>
			
			<c:if test="${arrayofparams[0]!='language'}">
			<c:param name="${arrayofparams[0]}" value="${arrayofparams[1]}"/>
			</c:if>
		
        	<c:param name="language" value="en"/>
		</c:url>
		
		">EN</a>

		<a
			href="
		<c:url value="${requestScope['javax.servlet.forward.servlet_path']}">
		<c:set var="params" value="${requestScope['javax.servlet.forward.query_string']}"/>
		<c:set var="string" value="${fn:replace(params, 
                                '&', '=')}" />
        <c:set var="arrayofparams" value="${fn:split(string,'=')}"/>
			
			<c:if test="${arrayofparams[0]!='language'}">
			<c:param name="${arrayofparams[0]}" value="${arrayofparams[1]}"/>
			</c:if>
			
        	<c:param name="language" value="ru"/>
		</c:url>
		">RU</a>

	</div>
</div>