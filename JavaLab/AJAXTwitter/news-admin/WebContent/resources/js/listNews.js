$(document)
		.ready(
				function() {

					var checkboxes = $("#deleteForm input[type='checkbox']"), submitButt = $(".deleteButton");

					checkboxes
							.click(function() {
								submitButt.attr("disabled", !checkboxes
										.is(":checked"));
							});

					$(document).on('submit', '#deleteForm', function() {

						return confirm($('#message').val());

					});

					var expanded = false;

					$(".overSelect")
							.click(
									function(event) {

										if (expanded) {
											$("#checkboxes").css({
												"display" : "none"
											});
											expanded = false;

											var selectedTags = $(
													'#checkboxes input:checkbox:checked')
													.map(
															function() {
																return $(this)
																		.next(
																				"span")
																		.text()
																		.split(
																				' ')
																		.join(
																				'%20');
															}).get();

											if (selectedTags.length != 0) {
												$('#selectedTags').text(
														selectedTags);
											} else {
												$('#selectedTags').text(
														$('#default').next(
																"span").text());

											}

										} else {
											$("#checkboxes").css({
												"display" : "block"
											});
											expanded = true;
										}
										event.stopPropagation();
									});

					$("html")
							.click(
									function() {

										if (expanded) {
											$("#checkboxes").css({
												"display" : "none"
											});
											expanded = false;

											var selectedTags = $(
													'#checkboxes input:checkbox:checked')
													.map(
															function() {
																return $(this)
																		.next(
																				"span")
																		.text()
																		.split(
																				' ')
																		.join(
																				'%20');
															}).get();

											if (selectedTags.length != 0) {
												$('#selectedTags').text(
														selectedTags);
											} else {
												$('#selectedTags').text(
														$('#default').next(
																"span").text());
											}

										}

									});

					$("#checkboxes").click(function(event) {

						$("#checkboxes").css({
							"display" : "block"
						});
						expanded = true;
						event.stopPropagation();
					});

					var selectedTags = $('#checkboxes input:checkbox:checked')
							.map(
									function() {
										return $(this).next("span").text()
												.split(' ').join('%20');
									}).get();

					if (selectedTags.length != 0) {
						$('#selectedTags').text(selectedTags);
					} else {
						$('#selectedTags').text(
								$('#default').next("span").text());
					}

					$.ajax({
						url : 'http://localhost:8081/twitter-service/home_timeline/',
						dataType: 'json',							
						success : function(data) {
							
							console.log(data);
							createList(data);
							

						},

						error : function(xhr, ajaxOptions, thrownError) {
							console.log('Error: ' + xhr.status +' '+ajaxOptions+' '+thrownError );
						}
					});
					
					
			$("#next").click(
					function() {
					
						$.ajax({
							url : 'http://localhost:8081/twitter-service/home_timeline/since/'
									+ ($('#lastNews').val()+1 )+'/count/4',
							dataType : 'json',
							success : function(data) {
								 if ( data.length == 0 ) {
								        alert("news are up to date!");
								    }
								 else{
								createList(data);
								 }
			
							},
			
							error : function(xhr, ajaxOptions, thrownError) {
								console.log('Error: ' + xhr.status + ' ' + ajaxOptions
										+ ' ' + thrownError);
							}
						});
			
					});
			
			$("#prev").click(
					function() {
					
						$.ajax({
							url : 'http://localhost:8081/twitter-service/home_timeline/max/'
									+ ($('#firstNews').val()-1) +'/count/4',
							dataType : 'json',
							success : function(data) {
								createList(data);
			
							},
			
							error : function(xhr, ajaxOptions, thrownError) {
								console.log('Error: ' + xhr.status + ' ' + ajaxOptions
										+ ' ' + thrownError);
							}
						});
			
					});

				});

		



function createList ( data ) {
	var templateSource 	= $('#templ').html(),
		template 		= Handlebars.compile( templateSource );
		$('#container').empty();
		$('#container').append(
				template({ news: data })
			);
}