$(document).ready(
		function() {

			var tags = [];
			
			$('.edit').on('click', function(e) {

				var target = $(this).attr('id');

				tags[target] = $("#tag"+target).val();
				
				$("#edit" + target).hide();
				$("#menu" + target).css('display', 'inline-block');
				$("#tag" + target).attr("disabled", false);

				e.preventDefault();
			});

			$('.cancel').on('click', function(e) {

				e.preventDefault();
				
				var target = $(this).attr('id');
				
				$("#tag"+target).val(tags[target]);
				$("#tag"+target).removeClass("highlight");
				
				$("#edit" + target).show();
				$("#menu" + target).hide();
				$("#tag" + target).attr("disabled", true);
			});

			$('.delete').on(
					'click',
					function(e) {

						var target = $(this).attr('id');
						e.preventDefault(); // prevent the link's default
						// behaviour
						
						
						var answer = confirm($('#message').val());

					    if (answer) {
//						var input = $("<input>").attr("type", "hidden").attr(
//								"name", "name").val("delete");
//						$('#form' + target).append($(input));
					    $('#form' + target).attr('action', 'tags-delete');	
						$("#form" + target).submit(); // trigget the submit
						// handler
					    }
					});

			$('.update').on(
					'click',
					function(e) {

						e.preventDefault(); // prevent the link's default behaviour
						var target = $(this).attr('id');
						
						if ($.trim($("#tag"+target).val()).length == 0) {
							$("#tag"+target).addClass("highlight");

						} else {
							$("#tag"+target).removeClass("highlight");
						
//						var input = $("<input>").attr("type", "hidden").attr(
//								"name", "name").val("update");
//						$('#form' + target).append($(input));
						$('#form' + target).attr('action', 'tags-update');	
						$("#form" + target).submit(); // trigget the submit handler
					
						}
					});

			$('.save').on('click', function(e) {

				e.preventDefault(); // prevent the link's default behaviour

				if ($.trim($("#tagName").val()).length == 0) {
					$("#tagName").addClass("highlight");

				} else {
					$("#tagName").removeClass("highlight");
					$("#addTag").submit();
				}

			});

		});