package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ListNewsController {
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(){
		
		System.out.println("Welcome!");
		
		return "listNews";
	}

}
