<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<head>
<title><spring:message code="menu.list" /></title>

<%-- <spring:url value="/resources/js/jquery-1.11.3.js" var="jquery" /> --%>
<%-- <script type="text/javascript" src="${jquery}"></script> --%>

<spring:url value="/resources/js/listNews.js" var="listNews" />
<script type="text/javascript" src="${listNews}"></script>

</head>

<div class="body">

	<div class="filter">
		<form:form commandName="filter" method="POST" action="news">

			<form:select path="author.authorId" class="author">
				<form:option value="0" selected="selected" disabled="disabled">
					<spring:message code="select.author" />
				</form:option>
				<form:options items="${authors}" itemValue="authorId"
					itemLabel="authorName" htmlEscape="true"/>

			</form:select>
			<div class="multiselect">
				<div class="selectBox">

					<select name="tags">
						<option id="selectedTags" selected disabled><spring:message
								code="select.tags" /></option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes">
					<label><input type="checkbox" name="tagsId" value="0"
						id="default" disabled="disabled" /><span><spring:message
								code="select.tags" /></span></label>
					<c:forEach items="${tags}" var="tag">
						<c:if test="${filter.tags.contains(tag)}">
							<label><input type="checkbox" name="tagsId"
								value="${tag.tagId}" checked="checked" /><span><c:out value="${tag.tagName}"></c:out></span></label>
						</c:if>
						<c:if test="${!filter.tags.contains(tag)}">
							<label><input type="checkbox" name="tagsId"
								value="${tag.tagId}" /><span><c:out value="${tag.tagName}"></c:out></span></label>
						</c:if>
					</c:forEach>
				</div>
			</div>
			&nbsp; <input id="submit" type="submit" name="submit"
				value="<spring:message code="button.filter"/>" /> 
			&nbsp; <input type="submit" id="reset" name="reset"
				value="<spring:message code="button.reset"/>" />

		</form:form>
	</div>

	<c:choose>
		<c:when test="${empty news}">
			<p>
				<spring:message code="message.nonews" />
			</p>
		</c:when>
		<c:otherwise>
			<form method="POST" action="delete" id="deleteForm">

<c:set var="lastNews" value="${news[0].newsId}"></c:set>
<%-- <input type="hidden" id="lastNews" value="${lastNews}" scope="session"/> --%>

				<c:forEach items="${news}" var="news">
				

					<div class="news">
						<div class="title">
							<b><a href="news?newsId=${news.newsId}"><c:out value="${news.title}"></c:out></a>&nbsp;(by
								<c:out value="${news.author.authorName}"></c:out>)</b>
						</div>

						<div class="date">
							<fmt:setBundle basename="message" var="bundle" />
							<fmt:message bundle="${bundle}" key="date.pattern" var="pattern" />
							<fmt:formatDate pattern="${pattern}"
								value="${news.modificationDate}" />

						</div>
						<div class="text"><c:out value="${news.shortText}"></c:out></div>
						<div class="bottomPanel">
							<span class="tags"> <c:forEach items="${news.tags}"
									var="tags">
								<c:out value="${tags.tagName},"></c:out>
							</c:forEach>
							</span> <span class="comments"> <spring:message
									code="message.comments" />(${news.comments.size()})
							</span> <a href="addnews?newsId=${news.newsId}"><spring:message
									code="message.edit" /></a> <input type="checkbox" name="newsId"
								value="${news.newsId}" />
						</div>
					</div>
				</c:forEach>
				<div class="deleteNews">
					<input type="hidden" name="page" value="${view.currentPage}" /> <input
						type="hidden" name="message" id="message"
						value="<spring:message
										code="message.delete.news" />" />
					<input type="submit" class="deleteButton" disabled="disabled"
						value="<spring:message code="button.delete"/>">
				</div>


				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		</c:otherwise>
	</c:choose>

	<br> <br>

	<div class="navigation">
<!-- 	<input type="button" id="ajaxNext" value="NEXT"/> -->
		<c:forEach var="i" items="${view.indexList}">
			<c:choose>
				<c:when test="${i-1 != view.currentPage}">
					<a class="link" href="?page=${i-1}"><input type="button"
						value="${i}" /></a>&nbsp;
				             </c:when>
				<c:otherwise>
					<input type="button" value="${i}" disabled />&nbsp;
                        </c:otherwise>
			</c:choose>
		</c:forEach>
		
		

	</div>
	<c:set var="page" value="${view.currentPage}" scope="session" />
</div>