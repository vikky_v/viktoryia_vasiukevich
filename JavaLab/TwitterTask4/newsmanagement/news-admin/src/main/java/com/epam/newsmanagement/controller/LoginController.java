package com.epam.newsmanagement.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.LinkedHashMap;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import twitter4j.TwitterException;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.Configuration;

@Controller
@SessionAttributes({ "filter", "userName", "token", "secretToken"})
public class LoginController {

	private static final String REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token";
	private static final String ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token";
	private static final String METHOD_POST = "POST";
	
	@Autowired
	private Configuration configuration;

	@RequestMapping(value = "/")
	public String toLoginPage() {

		return "redirect:login-twitter";
	}
	
	/*
	 * user authorization javax.net.ssl.HttpsURLConnection
	 */

	@RequestMapping(value = "/login-twitter", method = RequestMethod.GET)
	public String login(Model model) throws TwitterException,
			MalformedURLException, IOException, NoSuchAlgorithmException,
			InvalidKeyException {

		LinkedHashMap<String, String> data = configuration.getData();

		data.put("oauth_signature",
				configuration.getSignature(data, REQUEST_TOKEN_URL,METHOD_POST, null, null));

		String header = configuration.createOAuthHeader(data);
		HttpsURLConnection connection = configuration.createConnection(
				REQUEST_TOKEN_URL, METHOD_POST, header, true);

		writeRequest(connection, header);

		String response = readResponse(connection);

		String[] tokens = response.split("&");
		model.addAttribute("token", tokens[0].split("=")[1]);

		return "login";

	}

	@RequestMapping(value = "/callback")
	public String welcome(
			@RequestParam(value = "oauth_token", required = true) String token,
			@RequestParam(value = "oauth_verifier", required = true) String verifier,
			Model model) throws TwitterException, NoSuchAlgorithmException,
			InvalidKeyException, MalformedURLException, IOException {

		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) configuration
				.getData();
		data.put("oauth_token", token);

		data.put("oauth_signature",
				configuration.getSignature(data, ACCESS_TOKEN_URL, METHOD_POST, null, null));

		String header = configuration.createOAuthHeader(data);

		String postData = "oauth_verifier=" + verifier;

		HttpsURLConnection connection = configuration.createConnection(
				ACCESS_TOKEN_URL, METHOD_POST, header, true);

		writeRequest(connection, postData);

		String response = readResponse(connection);

		
		
		String[] tokens = response.split("&");
			
		model.addAttribute("token", tokens[0].split("=")[1]);
		model.addAttribute("secretToken", tokens[1].split("=")[1]);
		model.addAttribute("userName", tokens[3].split("=")[1]);

		Filter filter = new Filter();
		filter.setAuthor(new Author());
		filter.setTags(Collections.<Tag> emptyList());
		model.addAttribute("filter", filter);

		return "redirect:news?page=0";

	}	
	
	
	private boolean writeRequest(HttpsURLConnection connection, String textBody) {
		try {

			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(
					connection.getOutputStream()));
			wr.write(textBody);
			wr.flush();
			wr.close();
			return true;

		}

		catch (IOException e) {
			return false;
		}

	}

	private String readResponse(HttpsURLConnection connection) {

		try {
			StringBuffer buffer = new StringBuffer();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line = "";
			while ((line = br.readLine()) != null) {
				buffer.append(line + System.getProperty("line.separator"));
			}

			System.out.println("str readResponse " + buffer);

			return buffer.toString();
		}

		catch (IOException e) {
			return new String();
		}

	}
	/*
	 * application only authorization
	 */

	// @RequestMapping(value = "/login-twitter", method = RequestMethod.GET)
	// public String login(HttpSession session) throws MalformedURLException,
	// IOException {
	//
	// final String key = "nnJqFAVs4xvwWocKYTLt341gK";
	// final String secret =
	// "0YdUTioOZwajoTD0sCbquqmh5BPUigGzBr0bAVgSsAdxkbZOBj";
	// final String requestTokenURL = "https://api.twitter.com/oauth2/token";
	// final String fetchUserTimeline =
	// "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=vikky_v&count=4";
	//
	// String bearerToken = TwitterClient.requestBearerToken(requestTokenURL,
	// key, secret);
	//
	// session.setAttribute("Bearer", bearerToken);
	//
	// String timeline = TwitterClient.fetchTimelineTweet(fetchUserTimeline,
	// bearerToken);
	//
	// System.out.println(timeline);
	//
	// session.setAttribute("userName", "OnlyApp");
	// Filter filter = new Filter();
	// filter.setAuthor(new Author());
	// filter.setTags(Collections.<Tag> emptyList());
	// session.setAttribute("filter", filter);
	//
	// return "redirect:news?page=0";
	// }

	/*
	 * user authorization twitter4j
	 */

	// @RequestMapping(value = "/login-twitter", method = RequestMethod.GET)
	// public String login(HttpServletResponse response,HttpServletRequest
	// request, Model model) throws TwitterException {
	//
	// ConfigurationBuilder cb = new ConfigurationBuilder();
	// cb.setDebugEnabled(true)
	// .setOAuthConsumerKey("nnJqFAVs4xvwWocKYTLt341gK")
	// .setOAuthConsumerSecret(
	// "0YdUTioOZwajoTD0sCbquqmh5BPUigGzBr0bAVgSsAdxkbZOBj");
	// TwitterFactory tf = new TwitterFactory(cb.build());
	// Twitter twitter = tf.getInstance();
	//
	// mainTwitter = twitter;
	// model.addAttribute("mainTwitter", twitter);
	//
	// String callbackURL = "http://127.0.0.1:8081/news-admin/callback";
	// RequestToken requestToken = twitter.getOAuthRequestToken(callbackURL);
	//
	// mainToken = requestToken;
	// model.addAttribute("mainToken", requestToken);
	//
	//
	// String token = requestToken.getToken();
	// String tokenSecret = requestToken.getTokenSecret();
	//
	// //model.addAttribute("tokenSecret", tokenSecret);
	// //model.addAttribute("token", token);
	// request.getSession().setAttribute("token", token);
	// request.getSession().setAttribute("tokenSecret", tokenSecret);
	//
	// System.err.println(request.getSession().getAttribute("token"));
	//
	// String authUrl = requestToken.getAuthorizationURL();
	// request.setAttribute("authUrl", authUrl);
	//
	// return "login";
	//
	// }
	//
	// @RequestMapping(value = "/callback")
	// public String printWelcome(HttpServletResponse response,
	// HttpServletRequest request, HttpSession session, @RequestParam(value =
	// "oauth_token", required =
	// true) String token, @RequestParam(value = "oauth_verifier", required =
	// true) String verifier, Model model,
	// @ModelAttribute("mainToken")RequestToken requestToken,
	// @ModelAttribute("mainTwitter")Twitter twitter) throws TwitterException
	// {
	//
	// System.err.println("token in callback "+ token);
	// System.err.println("oauth_verifier in callback "+ verifier);
	//
	// // ConfigurationBuilder cb = new ConfigurationBuilder();
	// // cb.setDebugEnabled(true)
	// // .setOAuthConsumerKey("1SIuhrAPVBZqoR2IP7YTTJ7fZ")
	// // .setOAuthConsumerSecret(
	// // "hihIUpXmNnoCD8lCOFVX7mo8UecVyeBhWSOXsVpQhiXVpz8I8B");
	// // TwitterFactory tf = new TwitterFactory(cb.build());
	// // Twitter twitter = tf.getInstance();
	//
	// // Twitter twitter = mainTwitter;
	//
	// //String verifier = request.getParameter("oauth_verifier");
	// // RequestToken requestToken = new
	// //
	// RequestToken(request.getSession().getAttribute("token").toString(),request.getSession().getAttribute("tokenSecret").toString());
	//
	// //RequestToken requestToken = mainToken;
	//
	// AccessToken accessToken = twitter.getOAuthAccessToken(requestToken,
	// verifier);
	// twitter.setOAuthAccessToken(accessToken);
	// User user = twitter.verifyCredentials();
	//
	// // session = request.getSession();
	// Filter filter = new Filter();
	// filter.setAuthor(new Author());
	// filter.setTags(Collections.<Tag>emptyList());
	// session.setAttribute("filter", filter);
	//
	// session.setAttribute("userName", user.getName());
	//
	//
	//
	// return "redirect:news?page=0";
	//
	// }

	

	/*
	 * user authorization org.scribe
	 */

	// @RequestMapping(value = "/login-twitter", method = RequestMethod.GET)
	// public String login(Model model) {
	//
	// OAuthService service = getService();
	//
	// Token requestToken = service.getRequestToken();
	//
	// model.addAttribute("token", requestToken.getToken());
	//
	// model.addAttribute("requestToken", requestToken);
	//
	// return "login";
	// }
	//
	// @RequestMapping(value = "/callback")
	// public String printWelcome(@ModelAttribute("requestToken") Token
	// requestToken,
	// @RequestParam(value = "oauth_verifier", required = true) String
	// oauthVerifier,
	// Model model) throws JsonProcessingException, IOException {
	//
	// OAuthService service = getService();
	//
	//
	// System.out.println(requestToken);
	//
	// Verifier verifier = new Verifier(oauthVerifier);
	//
	// Token accessToken = service.getAccessToken(requestToken, verifier);
	//
	// OAuthRequest request = new OAuthRequest(Verb.GET,
	// PROTECTED_RESOURCE_URL);
	// service.signRequest(accessToken, request);
	// Response response = request.send();
	//
	// ObjectMapper mapper = new ObjectMapper();
	// JsonNode credentials = mapper.readTree(response.getBody());
	// JsonNode name = credentials.get("name");
	//
	// model.addAttribute("userName", name);
	//
	// return "redirect:news?page=0";
	//
	// }
	//
	// private OAuthService getService() {
	//
	// OAuthService service = new ServiceBuilder()
	// .provider(TwitterApi.class)
	// .apiKey("nnJqFAVs4xvwWocKYTLt341gK")
	// .apiSecret("0YdUTioOZwajoTD0sCbquqmh5BPUigGzBr0bAVgSsAdxkbZOBj")
	// .callback("http://127.0.0.1:8081/news-admin/callback").build();
	//
	// return service;
	// }


}
