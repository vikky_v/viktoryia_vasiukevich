package com.epam.newsmanagement.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

@Controller
@SessionAttributes({"filter", "token"})
public class ListNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@Autowired
	private PagedView pagedView;
	
	
	@RequestMapping(value = "/news", method = RequestMethod.POST, params = "submit")
	public String news(
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException{
		
		List<Tag> filterTags = getListOfTags(tagsId);
				
		filter.setTags(filterTags);
		
		System.err.println(filter);
		 
		return "redirect:news?page=0";
	}


	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String newsPage(@RequestParam(value = "page", required = false, defaultValue = "0") String page,
			@RequestParam(value = "since", required = false) String sinceId,
			@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "secretToken", required = false) String secretToken,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException, InvalidKeyException, MalformedURLException, NoSuchAlgorithmException, IOException {
		
		
//		FOR SPRING TASK
		
//		HttpURLConnection connection;
//		if(sinceId!=null){
//			connection = (HttpURLConnection) new URL("http://127.0.0.1:8081/twitter-service/home_timeline/"+sinceId)
//			.openConnection();
//		}
//		else{
//		connection = (HttpURLConnection) new URL("http://127.0.0.1:8081/twitter-service/home_timeline")
//		.openConnection();
//		}
//		String response = TwitterClient.readResponse(connection);
//		response = new String(response.getBytes(), "UTF-8");
//		System.out.println(response);
//		
//		ObjectMapper mapper = new ObjectMapper();	
//		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//		mapper.setDateFormat(sdf);
//		
//	
//		List<News> newsOut = mapper.readValue(response, new TypeReference<List<News>>(){});
//		
//		for(News news : newsOut){
//			newsManagementService.saveNews(news, null);
//		}
		
	

//		SimpleModule mod = new SimpleModule("News");
//		mod.addDeserializer(List.class, new NewsTwitterDeserializer());
//		mapper.registerModule(mod);
//
//		List<News> newsOut = null;
//		try {
//			newsOut = mapper.readValue(response, List.class);
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
			

		int numberOfNews = newsManagementService.countNews(filter).intValue();
		
		pagedView.setRowCount(numberOfNews);
		
		pagedView.setCurrentPage(Integer.parseInt(page));
		
	

		List<News> news = newsManagementService.getListNews(pagedView, filter);
		
		model.addAttribute("view", pagedView);
		model.addAttribute("news", news);
		
		
		
		setFilterContent(model);
		
		return "listNews";

	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(
			@RequestParam(value = "newsId", required = false) List<Long> newsId,
			@RequestParam(value = "page", required = true) String page) throws ServiceException {

		
		for (Long id : newsId) {
			newsManagementService.deleteNews(id);
		}

		return "redirect:news?page=" + page;
	}
	
	
	@RequestMapping(value = "/news", method = RequestMethod.POST, params = "reset")
	public String reset(
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("filter") Filter filter, Model model)
			throws ServiceException{

		filter.setAuthor(new Author());
		filter.setTags(Collections.<Tag>emptyList());
		 
		return "redirect:news?page=0";
	}
	
	
	private List<Tag> getListOfTags(List<Long> tagsId) throws ServiceException{
		
		List<Tag> filterTags = new ArrayList<Tag>();
		if (tagsId != null) {

			for (Long tagId : tagsId) {
				filterTags.add(newsManagementService.getTagById(tagId));
				
			}

		}
		
		return filterTags;
		
	}
	
	private void setFilterContent(Model model) throws ServiceException{
		
		List<Author> authors = newsManagementService.getAllAuthors();
		model.addAttribute("authors", authors);

		List<Tag> tags = newsManagementService.getAllTags();
		model.addAttribute("tags", tags);
	}

}
