package com.epam.newsmanagement.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.ControllerUtils;
import com.epam.newsmanagement.validator.NewsValidator;

@Controller
public class AddUpdateNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@Autowired
	private NewsValidator newsValidator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(newsValidator);
	}

	@RequestMapping(value = "/addnews", method = RequestMethod.GET)
	public String addNews(
			@RequestParam(value = "newsId", required = false, defaultValue = "0") String newsId,
			Model model) throws ServiceException {

		long newsIdLong = Long.parseLong(newsId);
		News news = null;

		if (newsIdLong != 0) {
			news = newsManagementService.getNews(newsIdLong);

			System.err.println(news);

		} else {
			news = new News();

		}

		setFilterContent(model);

		model.addAttribute("News", news);

		return "addUpdateNews";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(
			@RequestParam(value = "pattern", required = true) String pattern,
			@RequestParam(value = "modificDate", required = true) String modificationDate,
			@RequestParam(value = "creationDate", required = true) String creationDate,
			@RequestParam(value = "tagsId", required = false) List<Long> tagsId,
			@ModelAttribute("News") News news, BindingResult result, Model model)
			throws ServiceException, ParseException {

//		System.err.println(creationDate);

		Date date = null;
		
		if (ControllerUtils.isDateValid(creationDate, pattern)) {
			date = ControllerUtils.parseDate(creationDate, pattern);
			news.setCreationDate(date);
		}
		
		
		if (ControllerUtils.isDateValid(modificationDate, pattern)) {
			date = ControllerUtils.parseDate(modificationDate, pattern);
			news.setModificationDate(date);
		}

	

		newsValidator.validate(news, result);

		if (result.hasErrors()) {
//			System.err.println("creation " + creationDate);
//			System.err.println(result.getAllErrors());

			setFilterContent(model);

			model.addAttribute("News", news);

//			System.err.println(news);

			return "addUpdateNews";
		} else {

			System.err.println("success!");
			System.out.println(news);

			Author author = newsManagementService.getAuthorById(news
					.getAuthor().getAuthorId());
			System.err.println(author);

			news.setAuthor(author);
			// pass list of ID (?)
			Set<Tag> setOfTags = new LinkedHashSet<Tag>();
//			System.err.println(tagsId);

			if (tagsId != null && tagsId.size() != 0) {
				for (Long tagId : tagsId) {
					setOfTags.add(newsManagementService.getTagById(tagId));
				}
				news.setTags(setOfTags);
			}

//			System.err.println(news);
			long createdNewsId = newsManagementService.saveNews(news, date);
			model.addAttribute("menu", "news");
			return "redirect:news?newsId=" + createdNewsId;
		}

	}	
	

	private void setFilterContent(Model model) throws ServiceException {

		List<Author> authors = newsManagementService.getAllAuthors();
		model.addAttribute("authors", authors);

		List<Tag> tags = newsManagementService.getAllTags();
		model.addAttribute("tags", tags);
	}

}
