package com.epam.newsmanagement.utils;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ScheduledTask {

	@Autowired
	private INewsManagementService newsManagementService;

	private long lastNewsId;

	public long getLastNewsId() {
		return lastNewsId;
	}

	public void setLastNewsId(long lastNewsId) {
		this.lastNewsId = lastNewsId;
	}

	@Scheduled(fixedDelay = 1800000)
	public void getLastTweets() throws MalformedURLException, IOException,
			ServiceException, ConfigurationException {

		System.err.println("start updating id=" + lastNewsId
				+ " ------------------");

		HttpURLConnection connection;
		if (lastNewsId != 0) {
			connection = (HttpURLConnection) new URL(
					"http://127.0.0.1:8081/twitter-service/home_timeline/"
							+ lastNewsId).openConnection();
		} else {
			connection = (HttpURLConnection) new URL(
					"http://127.0.0.1:8081/twitter-service/home_timeline")
					.openConnection();
		}
		String response = readResponse(connection);
		response = new String(response.getBytes(), "UTF-8");
		System.out.println(response);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		mapper.setDateFormat(sdf);

		List<News> newsOut = mapper.readValue(response,
				new TypeReference<List<News>>() {
				});

		if (newsOut.size() > 0) {
			InputStream in = ScheduledTask.class.getClassLoader()
					.getResourceAsStream("Task.properties");
			Properties props = new Properties();
			props.load(in);
			props.setProperty("lastNewsId",
					String.valueOf(newsOut.get(0).getNewsId()));
			// FileOutputStream fos = new
			// FileOutputStream("src/main/resources/Task.properties");
			// FileOutputStream fos = new FileOutputStream("Task.properties");

			URL r = this.getClass().getResource("/");
			String decoded = URLDecoder.decode(r.getFile(), "UTF-8");
			if (decoded.startsWith("/")) {
				decoded = decoded.replaceFirst("/", "");
			}

			FileOutputStream fos = new FileOutputStream(decoded
					+ "Task.properties");

			props.store(fos, null);

			for (News news : newsOut) {
				Set<Tag> tags = news.getTags();
				if (tags != null && tags.size() > 0) {
					for (Tag tag : tags) {

						Tag tagFromDB = newsManagementService.getTagByName(tag
								.getTagName());
						if (tagFromDB != null) {
							tags.remove(tag);
							tags.add(tagFromDB);
						}
					}
					news.setTags(tags);
				}
				newsManagementService.saveNews(news, null);
			}

		}

	}

	
//	@Scheduled(fixedDelay = 1800000)
//	public void getUserTweets() throws MalformedURLException, IOException,
//			ServiceException, ConfigurationException {
//
//	
//
//		HttpURLConnection connection;
//	
//			connection = (HttpURLConnection) new URL(
//					"http://127.0.0.1:8081/twitter-service/user_timeline")
//					.openConnection();
//		
//		String response = readResponse(connection);
//		response = new String(response.getBytes(), "UTF-8");
//		System.out.println(response);
//
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//		mapper.setDateFormat(sdf);
//
//		List<News> newsOut = mapper.readValue(response,
//				new TypeReference<List<News>>() {
//				});
//
//		if (newsOut.size() > 0) {
//			
//			for (News news : newsOut) {
//				Set<Tag> tags = news.getTags();
//				if (tags != null && tags.size() > 0) {
//					for (Tag tag : tags) {
//
//						Tag tagFromDB = newsManagementService.getTagByName(tag
//								.getTagName());
//						if (tagFromDB != null) {
//							tags.remove(tag);
//							tags.add(tagFromDB);
//						}
//					}
//					news.setTags(tags);
//				}
//				newsManagementService.saveNews(news, null);
//			}
//
//		}
//
//	}
	
	private String readResponse(HttpURLConnection connection) {

		try {
			StringBuffer buffer = new StringBuffer();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line = "";
			while ((line = br.readLine()) != null) {
				buffer.append(line + System.getProperty("line.separator"));
			}

			System.out.println("str readResponse " + buffer);

			return buffer.toString();
		}

		catch (IOException e) {
			return new String();
		}

	}

}
