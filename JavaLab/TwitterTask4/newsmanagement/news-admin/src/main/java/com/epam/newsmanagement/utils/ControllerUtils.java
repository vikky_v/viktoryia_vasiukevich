package com.epam.newsmanagement.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class ControllerUtils {

	private ControllerUtils(){
		
	}
		
	public static Date parseDate(String dateStr, String pattern) throws ParseException{
		
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		formatter.setLenient(false);
		Date date = formatter.parse(dateStr);
		return date;
	}
	
	public static boolean isDateValid(String dateStr, String pattern){
		if(dateStr==null){
			return false;
		}
		 try{
			 parseDate(dateStr,pattern);
		 }
		 catch(ParseException e){
			 return false;
		 }
		 return true;
	}
		
}
