package com.epam.newsmanagement.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler
	public ModelAndView error(Exception ex) {

		ModelAndView model = new ModelAndView("errorPage");
		model.addObject("errClass", ex.getClass().getName());
		model.addObject("errMsg", ex.getLocalizedMessage());

		return model;
	}

	
}
