package com.epam.newsmanagement.dao.impl.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.dao.exception.DAOException;

@Transactional
public class UserDAOJPAImpl implements IUserDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public String getNameByLogin(String login) throws DAOException {

		Query query = entityManager
				.createNativeQuery("SELECT TO_CHAR(u.user_name) FROM user_table u WHERE u.login = ?1");
		query.setParameter(1, login);

		return (String) query.getSingleResult();
	}

}
