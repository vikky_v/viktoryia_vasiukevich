package com.epam.newsmanagement.dto;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;

/**
 * Entity associated with table 'AUTHOR'
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
@Entity
@Table(name="AUTHOR")
@BatchSize(size=100)
public class Author implements Serializable {

	
	private static final long serialVersionUID = 3882660528416323563L;
	
	@Id
	@Column(name="AUTHOR_ID")
	//@GeneratedValue(generator="seqA")
	//@SequenceGenerator(name="seqA",sequenceName="SEQUENCE_AUTHOR", allocationSize=1) 
	private long authorId;
	
	@Column(name="AUTHOR_NAME")
	private String authorName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="EXPIRED")
	private Date expired;

	public Author() {

	}

	public Author(long id, String name, Date expired) {
		this.authorId = id;
		this.authorName = name;
		this.expired = expired;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long author_id) {
		this.authorId = author_id;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String name) {
		this.authorName = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nAuthor [authorId=");
		builder.append(authorId);
		builder.append(", authorName=");
		builder.append(authorName);
		builder.append(", expired=");
		builder.append(expired);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + authorId);
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId != other.authorId)
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		return true;
	}

}
