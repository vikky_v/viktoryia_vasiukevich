package com.epam.newsmanagement.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.eclipse.persistence.annotations.BatchFetch;
import org.eclipse.persistence.annotations.BatchFetchType;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


/**
 * Entity associated with table 'NEWS'
 * 
 * @author Viktoryia Vasiukevich
 * 
 */

@Entity
@Cacheable(false)
@Table(name = "NEWS")
public class News implements Serializable{

	
	private static final long serialVersionUID = 4824471614925247885L;
	
	@Id
	@Column(name="NEWS_ID")
	//while inserting from json 
	//@GeneratedValue(generator="seqN")
	//@SequenceGenerator(name="seqN", sequenceName="SEQUENCE_NEWS",allocationSize=1)
	private long newsId;
	
	@Column(name="SHORT_TEXT")
	private String shortText;
	
	@Column(name="FULL_TEXT")
	private String fullText;
	
	@Column(name="TITLE")
	private String title;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM/dd/yyyy")
	private Date creationDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFICATION_DATE")
	//@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM/dd/yyyy")
	private Date modificationDate;

	@OneToOne(fetch = FetchType.EAGER, cascade = {javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.PERSIST}) //PERSIST for save from json
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = @JoinColumn(name = "NEWS_ID"), inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID"))
	@Cascade({CascadeType.SAVE_UPDATE}) //hibernate
	@BatchFetch(value = BatchFetchType.IN, size=100) //eclipselink
	private Author author;

	@OneToMany(fetch = FetchType.LAZY, cascade = {javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.PERSIST}) //PERSIST for save from json
	@JoinTable(name = "NEWS_TAG", joinColumns = @JoinColumn(name = "NEWS_ID"),inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
	@Cascade({CascadeType.SAVE_UPDATE}) //hibernate
	@Fetch(FetchMode.JOIN) //hibernate
	@BatchSize(size=100) //hibernate
	@BatchFetch(value = BatchFetchType.IN, size=100) //eclipselink
	private Set<Tag> tags;

	@OneToMany(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.REMOVE)
	@JoinColumn(name = "NEWS_ID", insertable = false, updatable=false)
	@Cascade({CascadeType.DELETE}) //hibernate	
	//@Fetch(FetchMode.JOIN) //hibernate
	@BatchSize(size=100) //hibernate
	@LazyCollection(LazyCollectionOption.EXTRA) //hibernate
	@BatchFetch(value = BatchFetchType.IN, size=100) //eclipselink	
	@OrderBy(clause = "COMMENT_ID ASC") //hibernate
	private Set<Comment> comments;
	

//	@Formula(value = "(select COUNT(c.COMMENT_ID) from COMMENTS c where c.NEWS_ID=NEWS_ID)")
//	private long numberOfComments;
	
	@Version
	@Column(name="VERSION")
	private long version;
	
	public News() {

	}

	public News(long newsId, String shortText, String fullText, String title,
			Date creationDate, Date modificationDate, Author author,
			Set<Tag> tags, Set<Comment> comments) {
		super();
		this.newsId = newsId;
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}
	
	
//delete constructor
	public News(long newsId, String shortText, String fullText, String title,
			Date creationDate, Date modificationDate) {
		super();
		this.newsId = newsId;
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId != other.newsId)
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n News [newsId=");
		builder.append(newsId);
		builder.append(", shortText=");
		builder.append(shortText);
		builder.append(", fullText=");
		builder.append(fullText);
		builder.append(", title=");
		builder.append(title);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append(", modificationDate=");
		builder.append(modificationDate);
		builder.append(", author=");
		builder.append(author);
		builder.append(", tags=");
		builder.append(tags);
		builder.append(", comments=");
		builder.append(comments);
		builder.append(", version=");
		builder.append(version);
		builder.append("] \n");
		return builder.toString();
	}



	
}
