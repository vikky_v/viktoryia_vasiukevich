package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

/**
 * Interface for service layer for {@link INewsDAO.class}
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface INewsService {

	/**
	 * Check if News object is null and then invoke INewsDAO create(News) method
	 * 
	 * @param ob
	 *            News object that should be created
	 * @return ID of created News
	 * @throws ServiceException
	 *             when News ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public long create(News ob) throws ServiceException;

	/**
	 * Check if News object is null and then invoke INewsDAO update(News) method
	 * 
	 * @param ob
	 *            News object that should be updated
	 * @throws ServiceException
	 *             when News ob is null or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void update(News ob) throws ServiceException;

	/**
	 * Check if id==0 and then invoke INewsDAO delete(id) method
	 * 
	 * @param id
	 *            ID of News that should be deleted
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public void delete(long id) throws ServiceException;

	/**
	 * Check if id==0 and then invoke INewsDAO findById(id) method
	 * 
	 * @param id
	 *            ID of News that should be found
	 * @return News object
	 * @throws ServiceException
	 *             when id=0 or when
	 *             {@link com.epam.news.dao.exception.DAOException} error occurs
	 */
	public News getById(long id) throws ServiceException;

	/**
	 * Method that invokes INewsDAO getPrevNextNewsId method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @param newsId
	 *            ID of current news
	 * @return List with previous and next newsId
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws ServiceException;

	/**
	 * Method that invokes INewsDAO countFilteredNews method
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return number of found news
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public Long countNews(Filter filter) throws ServiceException;

	/**
	 * Method that invokes INewsDAO getFilteredNews method
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return list of news that will be shown
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public List<News> getListNews(PagedView pagedView, Filter filter)
			throws ServiceException;

	/**
	 * Method that invokes INewsDAO getCurrentVersion method
	 * 
	 * @param news
	 *            News which version should be found
	 * @return current version of news
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public long getCurrentVersion(News news) throws ServiceException;

}
