package com.epam.newsmanagement.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dto.Author;
import com.epam.newsmanagement.dto.Comment;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.dto.Tag;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

public class NewsManagementServiceImpl implements INewsManagementService {

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private ICommentService commentService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private ITagService tagService;

	/**
	 * Check by name if specified Author exists
	 * 
	 * @param author
	 *            Author that should be checked
	 * @return true - if Author with equals name exists, false - in another case
	 * 
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public boolean isAuthorExists(Author author) throws ServiceException {
		long dbAuthorId = 0L;
		dbAuthorId = authorService.getAuthorId(author);

		return (dbAuthorId != 0);
	}

	/**
	 * Check by name if specified Tag exists
	 * 
	 * @param tag
	 *            Tag that should be checked
	 * @return true - if Tag with equals name exists, false - in another case
	 * @throws ServiceException
	 *             when {@link com.epam.news.dao.exception.DAOException} error
	 *             occurs
	 */
	public boolean isTagExists(Tag tag) throws ServiceException {
		String tagName = tag.getTagName();
		Tag dbTag = getTagByName(tagName);
		if (dbTag == null) {
			return false;
		} else
			return true;

	}

	public List<Tag> getAllTags() throws ServiceException {

		List<Tag> listTag = tagService.getAll();

		return listTag;
	}

	public Tag getTagById(long id) throws ServiceException {

		Tag tag = tagService.getById(id);

		return tag;
	}

	public List<Author> getAllAuthors() throws ServiceException {

		List<Author> listAuthor = authorService.getAll();

		return listAuthor;
	}

	public void updateAuthor(Author ob) throws ServiceException {

		authorService.update(ob);

	}

	public void deleteAuthor(long id) throws ServiceException {

		authorService.delete(id);

	}

	public Long countNews(Filter filter) throws ServiceException {

		long numberNews = newsService.countNews(filter);

		return numberNews;
	}

	public void updateTag(Tag ob) throws ServiceException {

		tagService.update(ob);
	}

	public void deleteTag(long id) throws ServiceException {

		tagService.delete(id);
	}

	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws ServiceException {

		List<Long> listId = newsService.getPrevNextNewsId(filter, newsId);

		return listId;
	}

	public long addComment(Comment comment) throws ServiceException {

		return commentService.create(comment);

	}

	public void deleteComment(Long id) throws ServiceException {

		commentService.delete(id);

	}

	public long saveNews(News news, Date date) throws ServiceException {
		long newsId = news.getNewsId();
		if (news.getNewsId() == 0L) {
			news.setCreationDate(date);
			newsId = addNews(news);
		} else {
//			Set<Comment> setOfComments = getCommentsByNewsID(news.getNewsId());
//			news.setComments(setOfComments);
			editNews(news);
		}

		return newsId;
	}

	public long addNews(News news) throws ServiceException {

		return newsService.create(news);
	}

	public void editNews(News news) throws ServiceException {

		newsService.update(news);
	}

	public void deleteNews(long newsId) throws ServiceException {

		newsService.delete(newsId);

	}

	public News getNews(long newsId) throws ServiceException {

		return newsService.getById(newsId);
	}

	public long addAuthor(Author author) throws ServiceException {

		long id = 0;
		if (!isAuthorExists(author)) {
			id = authorService.create(author);
		}
		else{
			throw new ServiceException("Author is already exists!");
		}
		return id;
	}


	public long addTag(Tag tag) throws ServiceException {

		long id = 0;
		if (!isTagExists(tag)) {
			id = tagService.create(tag);
		}
		else{
			throw new ServiceException("Tag is already exists!");
		}
		return id;
	}


	public List<News> getListNews(PagedView pagedView, Filter filter)
			throws ServiceException {

		return newsService.getListNews(pagedView, filter);
	}

	public Author getAuthorById(long authorId) throws ServiceException {

		return authorService.findById(authorId);
	}


	public long getCurrentVersion(News news) throws ServiceException {

		return newsService.getCurrentVersion(news);
	}

	public Tag getTagByName(String name) throws ServiceException {
		
		return tagService.getByName(name);
	}

}
