package com.epam.newsmanagement.dao.impl.hibernate;

import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Comment;

@Transactional
public class CommentDAOHibernateImpl implements ICommentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public long create(Comment comment) throws DAOException {
		long commentId = 0L;
		comment.setCreationDate(new Timestamp(new Date().getTime()));
		try {
			commentId = (Long) sessionFactory.getCurrentSession().save(comment);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return commentId;
	}

	public void delete(long id) throws DAOException {

		try {
			Comment comment = (Comment) sessionFactory.getCurrentSession().get(
					Comment.class, id);
			sessionFactory.getCurrentSession().delete(comment);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

	}

}
