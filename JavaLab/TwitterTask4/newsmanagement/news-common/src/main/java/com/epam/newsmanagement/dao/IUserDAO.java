package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;

public interface IUserDAO {

	/**
	 * Return name of user logged in system
	 * 
	 * @param login
	 *            login of user
	 * @return name of user
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public String getNameByLogin(String login) throws DAOException;

}
