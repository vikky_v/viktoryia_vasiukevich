package com.epam.newsmanagement.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;

/**
 * Entity associated with table 'COMMENTS'
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
@Entity
@Table(name = "COMMENTS")
@BatchSize(size=100)
public class Comment implements Serializable {

	private static final long serialVersionUID = -1630503469313999113L;

	@Id
	@Column(name = "COMMENT_ID")
	@GeneratedValue(generator = "seqC")
	@SequenceGenerator(name = "seqC", sequenceName = "SEQUENCE_COMMENTS", allocationSize = 1)
	private long commentId;

	@Column(name = "COMMENT_TEXT")
	private String commentText;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	// @ManyToOne
	// @JoinColumn(name = "NEWS_ID")
	// @Transient
	// private News news;

	@Column(name = "NEWS_ID", updatable=false)
	private Long newsId;

	public Comment() {

	}

	public Comment(long commentId, String commentText, Date creationDate) {

		this.commentId = commentId;
		this.commentText = commentText;
		this.creationDate = creationDate;

	}

	public Comment(long commentId, String commentText, Date creationDate,
	// News news) {
			long newsId) {
		super();
		this.commentId = commentId;
		this.commentText = commentText;
		this.creationDate = creationDate;
		// this.news = news;
		this.newsId = newsId;
	}

	public long getCommentId() {
		return commentId;
	}

	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (commentId ^ (commentId >>> 32));
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId != other.commentId)
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Comment [commentId=");
		builder.append(commentId);
		builder.append(", commentText=");
		builder.append(commentText);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append(", newsId=");
		builder.append(newsId);
		builder.append("]");
		return builder.toString();
	}

	// public News getNews() {
	// return news;
	// }
	//
	// public void setNews(News news) {
	// this.news = news;
	// }

	// @Override
	// public int hashCode() {
	// final int prime = 31;
	// int result = 1;
	// result = prime * result + (int) (commentId ^ (commentId >>> 32));
	// result = prime * result
	// + ((commentText == null) ? 0 : commentText.hashCode());
	// result = prime * result
	// + ((creationDate == null) ? 0 : creationDate.hashCode());
	// result = prime * result + ((news == null) ? 0 : news.hashCode());
	// return result;
	// }
	//
	// @Override
	// public boolean equals(Object obj) {
	// if (this == obj)
	// return true;
	// if (obj == null)
	// return false;
	// if (getClass() != obj.getClass())
	// return false;
	// Comment other = (Comment) obj;
	// if (commentId != other.commentId)
	// return false;
	// if (commentText == null) {
	// if (other.commentText != null)
	// return false;
	// } else if (!commentText.equals(other.commentText))
	// return false;
	// if (creationDate == null) {
	// if (other.creationDate != null)
	// return false;
	// } else if (!creationDate.equals(other.creationDate))
	// return false;
	// if (news == null) {
	// if (other.news != null)
	// return false;
	// } else if (!news.equals(other.news))
	// return false;
	// return true;
	// }
	//
	// @Override
	// public String toString() {
	// StringBuilder builder = new StringBuilder();
	// builder.append("Comment [commentId=");
	// builder.append(commentId);
	// builder.append(", commentText=");
	// builder.append(commentText);
	// builder.append(", creationDate=");
	// builder.append(creationDate);
	// builder.append(", news=");
	// builder.append(news);
	// builder.append("]");
	// return builder.toString();
	// }

}
