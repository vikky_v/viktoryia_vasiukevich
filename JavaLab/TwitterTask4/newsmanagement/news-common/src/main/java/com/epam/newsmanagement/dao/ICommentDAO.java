package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Comment;

/**
 * Extends {@link IDAO<Comment>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */
public interface ICommentDAO  {

	/**
	 * @param ob
	 *            Comment that should be created
	 * @return ID of created entity
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	long create(Comment ob) throws DAOException;


	/**
	 * @param id
	 *            ID of Comment that should be deleted
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	void delete(long id) throws DAOException;
	


}
