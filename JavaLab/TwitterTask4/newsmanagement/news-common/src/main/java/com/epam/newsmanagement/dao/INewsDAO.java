package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.News;
import com.epam.newsmanagement.utils.Filter;
import com.epam.newsmanagement.utils.PagedView;

/**
 * Extends {@link IDAO<News>} interface
 * 
 * @author Viktoryia Vasiukevich
 * 
 */

public interface INewsDAO extends IDAO<News> {

	/**
	 * Returns list of News associated with given Tag ID
	 * 
	 * @param tagId
	 *            Tag ID for which list of News ID should be found
	 * @return List of News
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNewsByTag(long tagId) throws DAOException;

	/**
	 * Method that get news applying filter by author and tags
	 * 
	 * @param currentPage
	 *            number of current page
	 * @param itemPerPage
	 *            number of news per page
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return list of news that will be shown
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<News> getNews(PagedView pagedView, Filter filter)
			throws DAOException;

	/**
	 * Method that returns number of found news
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @return number of found news
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public Number countNews(Filter filter) throws DAOException;

	/**
	 * Method that returns List with previous newsId on 0 position and next
	 * newsId on 1st position
	 * 
	 * @param filter
	 *            object that store Author and list of Tags for filtering news
	 * @param newsId
	 *            ID of current news
	 * @return List with previous and next newsId
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	public List<Long> getPrevNextNewsId(Filter filter, long newsId)
			throws DAOException;

	/**
	 * Method that returns current version of news
	 * 
	 * @param news
	 *            News which version should be found
	 * @return current version of news
	 * @throws DAOException
	 */
	public long getCurrentVersion(News news) throws DAOException;

	/**
	 * @param id
	 *            ID of entity that should be found
	 * @return entity
	 * @throws DAOException
	 *             when {@link java.sql.SQLException} error occurs
	 */
	News getById(long id) throws DAOException;
}
