package com.epam.newsmanagement.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.IUserService;
import com.epam.newsmanagement.service.exception.ServiceException;

public class UserServiceImpl implements IUserService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private IUserDAO userDAO;

	public String getNameByLogin(String login) throws ServiceException {

		String userName = null;
		if (login != null) {
			try {
				userName = userDAO.getNameByLogin(login);
			} catch (DAOException e) {
				log.error("Can't get user name", e);
				throw new ServiceException("Can't get user name", e);
			}
		} else {
			log.error("Login is null");
			throw new ServiceException("Login is null");
		}

		return userName;
	}

}
