package com.epam.newsmanagement.dao.impl.hibernate;

import java.util.Collections;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.dto.Tag;

@Transactional
public class TagDAOHibernateImpl implements ITagDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public long create(Tag tag) throws DAOException {

		long tagId = 0L;
		try {
			tagId = (Long) sessionFactory.getCurrentSession().save(tag);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return tagId;

	}

	public void update(Tag tag) throws DAOException {

		try {
			sessionFactory.getCurrentSession().update(tag);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
	}

	public void delete(long id) throws DAOException {

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery("delete from news_tag where news_tag.tag_id=:tagId");
			query.setLong("tagId", id);
			query.executeUpdate();
			Tag tag = getById(id);
			sessionFactory.getCurrentSession().delete(tag);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}

	}

	public Tag getById(long id) throws DAOException {
		
		Tag tag = null;
		try {
			tag = (Tag) sessionFactory.getCurrentSession().get(Tag.class, id);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return tag;

	}

	@SuppressWarnings("unchecked")
	public List<Tag> getAll() throws DAOException {

		List<Tag> listTag = Collections.<Tag>emptyList();
		try {
			listTag = (List<Tag>) sessionFactory.getCurrentSession()
					.createQuery("from Tag t order by t.tagId").list();
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		return listTag;
	}

	public Tag getByName(String name) throws DAOException {

		Query query;
		try {
			query = sessionFactory.getCurrentSession().createQuery(
					"select t from Tag t where tagName=:name");
			query.setString("name", name);
		} catch (HibernateException exc) {
			throw new DAOException(exc);
		}
		if (query.list().size() == 1) {
			return (Tag) query.list().get(0);
		} else {
			return null;
		}
	}

}
